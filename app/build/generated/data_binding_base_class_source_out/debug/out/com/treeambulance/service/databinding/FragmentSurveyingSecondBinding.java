// Generated by data binding compiler. Do not edit!
package com.treeambulance.service.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.button.MaterialButton;
import com.treeambulance.service.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentSurveyingSecondBinding extends ViewDataBinding {
  @NonNull
  public final MaterialButton btSave;

  @NonNull
  public final EditText etNotes;

  @NonNull
  public final ImageView ivBackSurveying2;

  @NonNull
  public final ImageView ivSelfiePhoto;

  @NonNull
  public final ImageView ivTree1st;

  @NonNull
  public final ImageView ivTree2nd;

  @NonNull
  public final LinearLayout llSelfiePhoto;

  @NonNull
  public final LinearLayout llTree1;

  @NonNull
  public final LinearLayout llTree2;

  @NonNull
  public final RelativeLayout rlToolBar;

  @NonNull
  public final TextView tvNotes;

  @NonNull
  public final TextView tvTitle;

  @Bindable
  protected View.OnClickListener mClickListener;

  protected FragmentSurveyingSecondBinding(Object _bindingComponent, View _root,
      int _localFieldCount, MaterialButton btSave, EditText etNotes, ImageView ivBackSurveying2,
      ImageView ivSelfiePhoto, ImageView ivTree1st, ImageView ivTree2nd, LinearLayout llSelfiePhoto,
      LinearLayout llTree1, LinearLayout llTree2, RelativeLayout rlToolBar, TextView tvNotes,
      TextView tvTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btSave = btSave;
    this.etNotes = etNotes;
    this.ivBackSurveying2 = ivBackSurveying2;
    this.ivSelfiePhoto = ivSelfiePhoto;
    this.ivTree1st = ivTree1st;
    this.ivTree2nd = ivTree2nd;
    this.llSelfiePhoto = llSelfiePhoto;
    this.llTree1 = llTree1;
    this.llTree2 = llTree2;
    this.rlToolBar = rlToolBar;
    this.tvNotes = tvNotes;
    this.tvTitle = tvTitle;
  }

  public abstract void setClickListener(@Nullable View.OnClickListener clickListener);

  @Nullable
  public View.OnClickListener getClickListener() {
    return mClickListener;
  }

  @NonNull
  public static FragmentSurveyingSecondBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_surveying_second, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentSurveyingSecondBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentSurveyingSecondBinding>inflateInternal(inflater, R.layout.fragment_surveying_second, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentSurveyingSecondBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_surveying_second, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentSurveyingSecondBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentSurveyingSecondBinding>inflateInternal(inflater, R.layout.fragment_surveying_second, null, false, component);
  }

  public static FragmentSurveyingSecondBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentSurveyingSecondBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentSurveyingSecondBinding)bind(component, view, R.layout.fragment_surveying_second);
  }
}
