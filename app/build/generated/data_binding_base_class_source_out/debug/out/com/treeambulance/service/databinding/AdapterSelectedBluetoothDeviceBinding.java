// Generated by data binding compiler. Do not edit!
package com.treeambulance.service.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.treeambulance.service.R;
import com.treeambulance.service.model.BluetoothDeviceListModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class AdapterSelectedBluetoothDeviceBinding extends ViewDataBinding {
  @NonNull
  public final TextView tvItemDescription;

  @NonNull
  public final TextView tvItemName;

  @Bindable
  protected BluetoothDeviceListModel mModel;

  protected AdapterSelectedBluetoothDeviceBinding(Object _bindingComponent, View _root,
      int _localFieldCount, TextView tvItemDescription, TextView tvItemName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.tvItemDescription = tvItemDescription;
    this.tvItemName = tvItemName;
  }

  public abstract void setModel(@Nullable BluetoothDeviceListModel model);

  @Nullable
  public BluetoothDeviceListModel getModel() {
    return mModel;
  }

  @NonNull
  public static AdapterSelectedBluetoothDeviceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.adapter_selected_bluetooth_device, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static AdapterSelectedBluetoothDeviceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<AdapterSelectedBluetoothDeviceBinding>inflateInternal(inflater, R.layout.adapter_selected_bluetooth_device, root, attachToRoot, component);
  }

  @NonNull
  public static AdapterSelectedBluetoothDeviceBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.adapter_selected_bluetooth_device, null, false, component)
   */
  @NonNull
  @Deprecated
  public static AdapterSelectedBluetoothDeviceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<AdapterSelectedBluetoothDeviceBinding>inflateInternal(inflater, R.layout.adapter_selected_bluetooth_device, null, false, component);
  }

  public static AdapterSelectedBluetoothDeviceBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static AdapterSelectedBluetoothDeviceBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (AdapterSelectedBluetoothDeviceBinding)bind(component, view, R.layout.adapter_selected_bluetooth_device);
  }
}
