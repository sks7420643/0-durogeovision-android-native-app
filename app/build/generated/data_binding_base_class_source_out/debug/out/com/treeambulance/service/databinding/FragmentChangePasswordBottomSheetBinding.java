// Generated by data binding compiler. Do not edit!
package com.treeambulance.service.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.button.MaterialButton;
import com.treeambulance.service.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentChangePasswordBottomSheetBinding extends ViewDataBinding {
  @NonNull
  public final MaterialButton btSubmit;

  @NonNull
  public final EditText edtConfirmPassword;

  @NonNull
  public final EditText edtNewPassword;

  @NonNull
  public final EditText edtPassword;

  @NonNull
  public final TextView tvTitle;

  @Bindable
  protected View.OnClickListener mClickListener;

  protected FragmentChangePasswordBottomSheetBinding(Object _bindingComponent, View _root,
      int _localFieldCount, MaterialButton btSubmit, EditText edtConfirmPassword,
      EditText edtNewPassword, EditText edtPassword, TextView tvTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btSubmit = btSubmit;
    this.edtConfirmPassword = edtConfirmPassword;
    this.edtNewPassword = edtNewPassword;
    this.edtPassword = edtPassword;
    this.tvTitle = tvTitle;
  }

  public abstract void setClickListener(@Nullable View.OnClickListener clickListener);

  @Nullable
  public View.OnClickListener getClickListener() {
    return mClickListener;
  }

  @NonNull
  public static FragmentChangePasswordBottomSheetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_change_password_bottom_sheet, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentChangePasswordBottomSheetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentChangePasswordBottomSheetBinding>inflateInternal(inflater, R.layout.fragment_change_password_bottom_sheet, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentChangePasswordBottomSheetBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_change_password_bottom_sheet, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentChangePasswordBottomSheetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentChangePasswordBottomSheetBinding>inflateInternal(inflater, R.layout.fragment_change_password_bottom_sheet, null, false, component);
  }

  public static FragmentChangePasswordBottomSheetBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentChangePasswordBottomSheetBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentChangePasswordBottomSheetBinding)bind(component, view, R.layout.fragment_change_password_bottom_sheet);
  }
}
