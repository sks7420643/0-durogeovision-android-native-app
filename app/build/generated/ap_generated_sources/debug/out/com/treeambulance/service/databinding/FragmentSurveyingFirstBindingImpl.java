package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSurveyingFirstBindingImpl extends FragmentSurveyingFirstBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 13);
        sViewsWithIds.put(R.id.tvTitle, 14);
        sViewsWithIds.put(R.id.llProjectName, 15);
        sViewsWithIds.put(R.id.llCountryName, 16);
        sViewsWithIds.put(R.id.llStateName, 17);
        sViewsWithIds.put(R.id.llDistrictName, 18);
        sViewsWithIds.put(R.id.llVillageName, 19);
        sViewsWithIds.put(R.id.etVillage, 20);
        sViewsWithIds.put(R.id.llSurveyDate, 21);
        sViewsWithIds.put(R.id.llLocation, 22);
        sViewsWithIds.put(R.id.etLocationText, 23);
        sViewsWithIds.put(R.id.llTreeNo, 24);
        sViewsWithIds.put(R.id.etTreeNo, 25);
        sViewsWithIds.put(R.id.llNewTreeNo, 26);
        sViewsWithIds.put(R.id.etNewTreeNo, 27);
        sViewsWithIds.put(R.id.llLandSurveyNo, 28);
        sViewsWithIds.put(R.id.etLandSurveyNo, 29);
        sViewsWithIds.put(R.id.llNameLand, 30);
        sViewsWithIds.put(R.id.etNameLand, 31);
        sViewsWithIds.put(R.id.llLocalTreeName, 32);
        sViewsWithIds.put(R.id.llBotanicalTreeName, 33);
        sViewsWithIds.put(R.id.llHealthStatus, 34);
        sViewsWithIds.put(R.id.etHealthStatus, 35);
        sViewsWithIds.put(R.id.llSizeOfGirth, 36);
        sViewsWithIds.put(R.id.etSizeOfGirth, 37);
        sViewsWithIds.put(R.id.llHeightOfTree, 38);
        sViewsWithIds.put(R.id.etHeightOfTree, 39);
        sViewsWithIds.put(R.id.llValueOfTree, 40);
        sViewsWithIds.put(R.id.etValueOfTree, 41);
        sViewsWithIds.put(R.id.llAValueOfTree, 42);
        sViewsWithIds.put(R.id.llAgeOfTheTree, 43);
        sViewsWithIds.put(R.id.etAgeOfTheTree, 44);
        sViewsWithIds.put(R.id.llContractorName, 45);
        sViewsWithIds.put(R.id.llSurveyorName, 46);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public FragmentSurveyingFirstBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 47, sIncludes, sViewsWithIds));
    }
    private FragmentSurveyingFirstBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[12]
            , (android.widget.EditText) bindings[9]
            , (android.widget.EditText) bindings[44]
            , (android.widget.EditText) bindings[8]
            , (android.widget.EditText) bindings[10]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[35]
            , (android.widget.EditText) bindings[39]
            , (android.widget.EditText) bindings[29]
            , (android.widget.EditText) bindings[7]
            , (android.widget.EditText) bindings[23]
            , (android.widget.EditText) bindings[31]
            , (android.widget.EditText) bindings[27]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[37]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[11]
            , (android.widget.EditText) bindings[25]
            , (android.widget.EditText) bindings[41]
            , (android.widget.EditText) bindings[20]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.LinearLayout) bindings[42]
            , (android.widget.LinearLayout) bindings[43]
            , (android.widget.LinearLayout) bindings[33]
            , (android.widget.LinearLayout) bindings[45]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[18]
            , (android.widget.LinearLayout) bindings[34]
            , (android.widget.LinearLayout) bindings[38]
            , (android.widget.LinearLayout) bindings[28]
            , (android.widget.LinearLayout) bindings[32]
            , (android.widget.LinearLayout) bindings[22]
            , (android.widget.LinearLayout) bindings[30]
            , (android.widget.LinearLayout) bindings[26]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[36]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.LinearLayout) bindings[21]
            , (android.widget.LinearLayout) bindings[46]
            , (android.widget.LinearLayout) bindings[24]
            , (android.widget.LinearLayout) bindings[40]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.RelativeLayout) bindings[13]
            , (android.widget.TextView) bindings[14]
            );
        this.btNextPage.setTag(null);
        this.etAValueOfTree.setTag(null);
        this.etBotanicalTreeName.setTag(null);
        this.etContractorName.setTag(null);
        this.etCountryName.setTag(null);
        this.etDistrict.setTag(null);
        this.etLocalTreeName.setTag(null);
        this.etProjectName.setTag(null);
        this.etState.setTag(null);
        this.etSurveyDate.setTag(null);
        this.etSurveyorName.setTag(null);
        this.ivBackSurveying1.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;

        if ((dirtyFlags & 0x3L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btNextPage.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etAValueOfTree.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etBotanicalTreeName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etContractorName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etCountryName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etDistrict.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etLocalTreeName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etProjectName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etState.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etSurveyDate.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etSurveyorName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackSurveying1.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}