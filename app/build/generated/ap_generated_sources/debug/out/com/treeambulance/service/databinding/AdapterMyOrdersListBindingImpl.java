package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class AdapterMyOrdersListBindingImpl extends AdapterMyOrdersListBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tvOrderNoTitle, 1);
        sViewsWithIds.put(R.id.tvOrderNoValue, 2);
        sViewsWithIds.put(R.id.tvDateValue, 3);
        sViewsWithIds.put(R.id.tvTimeValue, 4);
        sViewsWithIds.put(R.id.tvCustNameTitle, 5);
        sViewsWithIds.put(R.id.tvCustNameValue, 6);
        sViewsWithIds.put(R.id.tvPickUpAddressTitle, 7);
        sViewsWithIds.put(R.id.tvPickUpAddressValue, 8);
        sViewsWithIds.put(R.id.tvDesignationAddressTitle, 9);
        sViewsWithIds.put(R.id.tvDesignationAddressValue, 10);
        sViewsWithIds.put(R.id.tvMobileNoTitle, 11);
        sViewsWithIds.put(R.id.tvMobileNoValue, 12);
        sViewsWithIds.put(R.id.tvReportingToTitle, 13);
        sViewsWithIds.put(R.id.tvReportingToValue, 14);
        sViewsWithIds.put(R.id.llAcceptDecline, 15);
        sViewsWithIds.put(R.id.btAccept, 16);
        sViewsWithIds.put(R.id.btDecline, 17);
        sViewsWithIds.put(R.id.llStartCancel, 18);
        sViewsWithIds.put(R.id.btStart, 19);
        sViewsWithIds.put(R.id.btCancel, 20);
        sViewsWithIds.put(R.id.llCompleteCancel, 21);
        sViewsWithIds.put(R.id.btComplete, 22);
        sViewsWithIds.put(R.id.btComCancel, 23);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public AdapterMyOrdersListBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 24, sIncludes, sViewsWithIds));
    }
    private AdapterMyOrdersListBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[16]
            , (com.google.android.material.button.MaterialButton) bindings[20]
            , (com.google.android.material.button.MaterialButton) bindings[23]
            , (com.google.android.material.button.MaterialButton) bindings[22]
            , (com.google.android.material.button.MaterialButton) bindings[17]
            , (com.google.android.material.button.MaterialButton) bindings[19]
            , (com.google.android.material.card.MaterialCardView) bindings[0]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[21]
            , (android.widget.LinearLayout) bindings[18]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[4]
            );
        this.cv.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}