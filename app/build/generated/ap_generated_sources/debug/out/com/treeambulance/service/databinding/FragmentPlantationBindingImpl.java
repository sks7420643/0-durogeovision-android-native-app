package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentPlantationBindingImpl extends FragmentPlantationBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 11);
        sViewsWithIds.put(R.id.tvTitle, 12);
        sViewsWithIds.put(R.id.llProjectName, 13);
        sViewsWithIds.put(R.id.llPlantingDate, 14);
        sViewsWithIds.put(R.id.llNewTreeNo, 15);
        sViewsWithIds.put(R.id.etNewTreeNo, 16);
        sViewsWithIds.put(R.id.llSurveyNo, 17);
        sViewsWithIds.put(R.id.etSurveyNo, 18);
        sViewsWithIds.put(R.id.PlantedBy, 19);
        sViewsWithIds.put(R.id.etPlantedBy, 20);
        sViewsWithIds.put(R.id.AdoptedBy, 21);
        sViewsWithIds.put(R.id.etAdoptedBy, 22);
        sViewsWithIds.put(R.id.llHeightOfTree, 23);
        sViewsWithIds.put(R.id.etHeightOfTree, 24);
        sViewsWithIds.put(R.id.llLocalTreeName, 25);
        sViewsWithIds.put(R.id.llBotanicalTreeName, 26);
        sViewsWithIds.put(R.id.etBotanicalTreeName, 27);
        sViewsWithIds.put(R.id.llCountryName, 28);
        sViewsWithIds.put(R.id.llStateName, 29);
        sViewsWithIds.put(R.id.llDistrictName, 30);
        sViewsWithIds.put(R.id.llVillageName, 31);
        sViewsWithIds.put(R.id.etVillage, 32);
        sViewsWithIds.put(R.id.llLocation, 33);
        sViewsWithIds.put(R.id.etLocationText, 34);
        sViewsWithIds.put(R.id.llSuprivisorName, 35);
        sViewsWithIds.put(R.id.llAValueOfTree, 36);
        sViewsWithIds.put(R.id.llAgeOfTheTree, 37);
        sViewsWithIds.put(R.id.etAgeOfTheTree, 38);
        sViewsWithIds.put(R.id.llContractorName, 39);
        sViewsWithIds.put(R.id.etContractorName, 40);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public FragmentPlantationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 41, sIncludes, sViewsWithIds));
    }
    private FragmentPlantationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[21]
            , (android.widget.LinearLayout) bindings[19]
            , (com.google.android.material.button.MaterialButton) bindings[10]
            , (android.widget.EditText) bindings[9]
            , (android.widget.EditText) bindings[22]
            , (android.widget.EditText) bindings[38]
            , (android.widget.EditText) bindings[27]
            , (android.widget.EditText) bindings[40]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[7]
            , (android.widget.EditText) bindings[24]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[34]
            , (android.widget.EditText) bindings[16]
            , (android.widget.EditText) bindings[8]
            , (android.widget.EditText) bindings[20]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[18]
            , (android.widget.EditText) bindings[32]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.LinearLayout) bindings[36]
            , (android.widget.LinearLayout) bindings[37]
            , (android.widget.LinearLayout) bindings[26]
            , (android.widget.LinearLayout) bindings[39]
            , (android.widget.LinearLayout) bindings[28]
            , (android.widget.LinearLayout) bindings[30]
            , (android.widget.LinearLayout) bindings[23]
            , (android.widget.LinearLayout) bindings[25]
            , (android.widget.LinearLayout) bindings[33]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[14]
            , (android.widget.LinearLayout) bindings[13]
            , (android.widget.LinearLayout) bindings[29]
            , (android.widget.LinearLayout) bindings[35]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.LinearLayout) bindings[31]
            , (android.widget.RelativeLayout) bindings[11]
            , (android.widget.TextView) bindings[12]
            );
        this.btNextPage.setTag(null);
        this.etAValueOfTree.setTag(null);
        this.etCountryName.setTag(null);
        this.etDistrict.setTag(null);
        this.etLocalTreeName.setTag(null);
        this.etOperatorName.setTag(null);
        this.etPlantingDate.setTag(null);
        this.etProjectName.setTag(null);
        this.etState.setTag(null);
        this.ivBackSurveying1.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;

        if ((dirtyFlags & 0x3L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btNextPage.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etAValueOfTree.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etCountryName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etDistrict.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etLocalTreeName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etOperatorName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etPlantingDate.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etProjectName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etState.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackSurveying1.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}