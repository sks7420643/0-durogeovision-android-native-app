package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityPlantationRetrievalBindingImpl extends ActivityPlantationRetrievalBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 7);
        sViewsWithIds.put(R.id.tvTitle, 8);
        sViewsWithIds.put(R.id.llProjectName, 9);
        sViewsWithIds.put(R.id.llRange, 10);
        sViewsWithIds.put(R.id.llCount, 11);
        sViewsWithIds.put(R.id.llTransplantationTreeCount, 12);
        sViewsWithIds.put(R.id.tvDistance, 13);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.FrameLayout mboundView5;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public ActivityPlantationRetrievalBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 14, sIncludes, sViewsWithIds));
    }
    private ActivityPlantationRetrievalBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[4]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[12]
            , (android.widget.RelativeLayout) bindings[7]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[8]
            );
        this.etProjectName.setTag(null);
        this.etRange.setTag(null);
        this.etTransplantationTreeCount.setTag(null);
        this.ivBackDataRetrieval.setTag(null);
        this.ivTree1st.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView5 = (android.widget.FrameLayout) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;

        if ((dirtyFlags & 0x3L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.etProjectName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etRange.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etTransplantationTreeCount.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackDataRetrieval.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivTree1st.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.mboundView5.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}