package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentReportGenerationBindingImpl extends FragmentReportGenerationBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 12);
        sViewsWithIds.put(R.id.tvTitle, 13);
        sViewsWithIds.put(R.id.llReportType, 14);
        sViewsWithIds.put(R.id.llFromDate, 15);
        sViewsWithIds.put(R.id.llToDate, 16);
        sViewsWithIds.put(R.id.llOperatorName, 17);
        sViewsWithIds.put(R.id.llStatus, 18);
        sViewsWithIds.put(R.id.llCountryName, 19);
        sViewsWithIds.put(R.id.llStateName, 20);
        sViewsWithIds.put(R.id.llDistrictName, 21);
        sViewsWithIds.put(R.id.llVillageName, 22);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public FragmentReportGenerationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 23, sIncludes, sViewsWithIds));
    }
    private FragmentReportGenerationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[11]
            , (android.widget.EditText) bindings[7]
            , (android.widget.EditText) bindings[9]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[8]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[10]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.LinearLayout) bindings[21]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.LinearLayout) bindings[14]
            , (android.widget.LinearLayout) bindings[20]
            , (android.widget.LinearLayout) bindings[18]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[22]
            , (android.widget.RelativeLayout) bindings[12]
            , (android.widget.TextView) bindings[13]
            );
        this.btSubmit.setTag(null);
        this.etCountryName.setTag(null);
        this.etDistrict.setTag(null);
        this.etFromDate.setTag(null);
        this.etOperatorName.setTag(null);
        this.etReportType.setTag(null);
        this.etState.setTag(null);
        this.etStatusName.setTag(null);
        this.etToDate.setTag(null);
        this.etVillage.setTag(null);
        this.ivBackReportGeneration.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;

        if ((dirtyFlags & 0x3L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btSubmit.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etCountryName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etDistrict.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etFromDate.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etOperatorName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etReportType.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etState.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etStatusName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etToDate.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etVillage.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackReportGeneration.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}