package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCreateCompanyBindingImpl extends FragmentCreateCompanyBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 8);
        sViewsWithIds.put(R.id.tvTitle, 9);
        sViewsWithIds.put(R.id.llCompanyName, 10);
        sViewsWithIds.put(R.id.llLogo, 11);
        sViewsWithIds.put(R.id.llRFIDName, 12);
        sViewsWithIds.put(R.id.llRFIDList, 13);
        sViewsWithIds.put(R.id.tvRFIDDevicesTitle, 14);
        sViewsWithIds.put(R.id.tvNoOfRFIDDevice, 15);
        sViewsWithIds.put(R.id.rvRFIDDevices, 16);
        sViewsWithIds.put(R.id.tvNoRFID, 17);
        sViewsWithIds.put(R.id.llGPSName, 18);
        sViewsWithIds.put(R.id.llGPSDevicesTitle, 19);
        sViewsWithIds.put(R.id.tvGPSDevicesTitle, 20);
        sViewsWithIds.put(R.id.tvNoOfGPSDevice, 21);
        sViewsWithIds.put(R.id.llGPSList, 22);
        sViewsWithIds.put(R.id.rvGPSDevices, 23);
        sViewsWithIds.put(R.id.tvNoGPS, 24);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.FrameLayout mboundView3;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public FragmentCreateCompanyBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private FragmentCreateCompanyBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (android.widget.EditText) bindings[6]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[2]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.LinearLayout) bindings[22]
            , (android.widget.LinearLayout) bindings[18]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[13]
            , (android.widget.LinearLayout) bindings[12]
            , (android.widget.RelativeLayout) bindings[8]
            , (androidx.recyclerview.widget.RecyclerView) bindings[23]
            , (androidx.recyclerview.widget.RecyclerView) bindings[16]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[9]
            );
        this.btSave.setTag(null);
        this.etAddGPSDevice.setTag(null);
        this.etAddRFIDDevice.setTag(null);
        this.etCompanyName.setTag(null);
        this.ivBackCreateCompany.setTag(null);
        this.ivLogoName.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (android.widget.FrameLayout) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;

        if ((dirtyFlags & 0x3L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btSave.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etAddGPSDevice.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etAddRFIDDevice.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etCompanyName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackCreateCompany.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivLogoName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.mboundView3.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}