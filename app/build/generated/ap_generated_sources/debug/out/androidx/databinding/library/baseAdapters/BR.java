package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int clickListener = 1;

  public static final int currencyNameModel = 2;

  public static final int model = 3;

  public static final int reportTypeModel = 4;

  public static final int stateCityModel = 5;

  public static final int statusRangeModel = 6;

  public static final int strCompanyName = 7;
}
