package com.treeambulance.service.Interface;

public interface OnItemClickListener {
    void onItemClick(int position);
}