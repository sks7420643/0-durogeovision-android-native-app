package com.treeambulance.service.Interface;

import android.view.View;

public interface RecyclerViewListener {
    void onRecyclerViewItem(int pos, View view);
}
