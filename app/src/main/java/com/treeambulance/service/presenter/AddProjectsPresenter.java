package com.treeambulance.service.presenter;

import com.treeambulance.service.model.AddProjectsModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class AddProjectsPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public AddProjectsPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getAddEditProjects(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getAddEditProjects(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessAddProjects, this::onErrorAddProjects));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    public void getCountryList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCountryList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCountryList, this::onErrorCountryList));
    }



    private void onSuccessAddProjects(AddProjectsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessAddProjects(model);
        } else {
            contactInterface.onErrorAddProjects(model.getMsg());
        }
    }



    private void onErrorAddProjects(Throwable throwable) {
        contactInterface.onErrorAddProjects(Utiles.getBodyError(throwable));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessCountryList(CountryListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCountryList(model);
        } else {
            contactInterface.onErrorCountryList(model.getMsg());
        }
    }

    private void onErrorCountryList(Throwable throwable) {
        contactInterface.onErrorCountryList(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessAddProjects(AddProjectsModel addProjectsModel);

        void onErrorAddProjects(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessCountryList(CountryListModel countryListModel);

        void onErrorCountryList(String error);
    }
}