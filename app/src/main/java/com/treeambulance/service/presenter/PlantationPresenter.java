package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.ListPlantationModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.TreeDetailsModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class PlantationPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public PlantationPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getListPlantations(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getListPlantation(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessListProject, this::onErrorListProjects));
    }

    public void getTreeManagementList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getTreeManagementList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeManagementList, this::onErrorTreeManagementList));
    }

    public void getCreatePlantationMaintenance(HashMap<String, RequestBody> hashMap,
                                     MultipartBody.Part tree1Photo, MultipartBody.Part tree2Photo,
                                     MultipartBody.Part selfiePhoto) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreatePlantationMaintenance(hashMap, tree1Photo, tree2Photo, selfiePhoto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateMaintenance, this::onErrorCreateMaintenance));
    }

    public void getCountryList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCountryList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCountryList, this::onErrorCountryList));
    }

    public void getOperatorList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getOperatorList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOperatorList, this::onErrorOperatorList));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    public void getCheckPlantation(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCheckSurveyNo(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCheckSurveyNo, this::onErrorCheckSurveyNo));
    }

    public void getTreeDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getTreeDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeDetails, this::onErrorTreeDetails));
    }

    private void onSuccessTreeDetails(TreeDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeDetails(model);
        } else {
            contactInterface.onErrorTreeDetails(model.getMsg());
        }
    }

    private void onErrorTreeDetails(Throwable throwable) {
        contactInterface.onErrorTreeDetails(Utiles.getBodyError(throwable));
    }

    public void getCreatePlantation(HashMap<String, RequestBody> createSurveyingHashMap,
                                   MultipartBody.Part tree1Photo, MultipartBody.Part tree2Photo,
                                   MultipartBody.Part selfiePhoto) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreatePlantation(createSurveyingHashMap, tree1Photo, tree2Photo, selfiePhoto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateSurveying, this::onErrorCreateSurveying));
    }

    private void onSuccessListProject(ListPlantationModel listPlantationModel) {
        if (listPlantationModel.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessListProject(listPlantationModel);
        } else {
            contactInterface.onErrorListProjects(listPlantationModel.getMsg());
        }
    }

    private void onErrorListProjects(Throwable throwable) {
        contactInterface.onErrorListProjects(Utiles.getBodyError(throwable));
    }

    private void onSuccessOperatorList(OperatorListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessOperatorList(model);
        } else {
            contactInterface.onErrorOperatorList(model.getMsg());
        }
    }

    private void onErrorOperatorList(Throwable throwable) {
        contactInterface.onErrorOperatorList(Utiles.getBodyError(throwable));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessCheckSurveyNo(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCheckSurveyNo(model);
        } else {
            contactInterface.onErrorCheckSurveyNo(model.getMsg());
        }
    }

    private void onErrorCheckSurveyNo(Throwable throwable) {
        contactInterface.onErrorCheckSurveyNo(Utiles.getBodyError(throwable));
    }

    private void onSuccessCreateSurveying(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateSurveying(model);
        } else {
            contactInterface.onErrorCreateSurveying(model.getMsg());
        }
    }

    private void onErrorCreateSurveying(Throwable throwable) {
        contactInterface.onErrorCreateSurveying(Utiles.getBodyError(throwable));
    }

    private void onSuccessCreateMaintenance(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateMaintenance(model);
        } else {
            contactInterface.onErrorCreateMaintenance(model.getMsg());
        }
    }

    private void onErrorCreateMaintenance(Throwable throwable) {
        contactInterface.onErrorCreateMaintenance(Utiles.getBodyError(throwable));
    }

    private void onSuccessCountryList(CountryListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCountryList(model);
        } else {
            contactInterface.onErrorCountryList(model.getMsg());
        }
    }

    private void onErrorCountryList(Throwable throwable) {
        contactInterface.onErrorCountryList(Utiles.getBodyError(throwable));
    }

    private void onSuccessTreeManagementList(TreeManagementListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeManagementList(model);
        } else {
            contactInterface.onErrorTreeManagementList(model.getMsg());
        }
    }

    private void onErrorTreeManagementList(Throwable throwable) {
        contactInterface.onErrorTreeManagementList(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessListProject(ListPlantationModel listPlantationModel);

        void onErrorListProjects(String error);

        void onSuccessOperatorList(OperatorListModel operatorListModel);

        void onErrorOperatorList(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessCheckSurveyNo(CommonModel model);

        void onErrorCheckSurveyNo(String error);

        void onSuccessCreateSurveying(CommonModel model);

        void onErrorCreateSurveying(String error);

        void onSuccessCountryList(CountryListModel countryListModel);

        void onErrorCountryList(String error);

        void onSuccessTreeManagementList(TreeManagementListModel addTreeManagementModel);

        void onErrorTreeManagementList(String error);

        void onSuccessCreateMaintenance(CommonModel model);

        void onErrorCreateMaintenance(String error);

        void onSuccessTreeDetails(TreeDetailsModel listProjectsModel);

        void onErrorTreeDetails(String error);
    }
}
