package com.treeambulance.service.presenter;

import com.treeambulance.service.model.AddProjectsModel;
import com.treeambulance.service.model.AddTreeManagementModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class AddTreeManagementPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public AddTreeManagementPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getTreeManagementList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getTreeManagementList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeManagementList, this::onErrorTreeManagementList));
    }

    public void getDeleteOperator(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getDeleteOperator(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessDeleteTreeManagement, this::onErrorDeleteTreeManagement));
    }



    public void getAddEditTreeManagement(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getAddEditTreeManagements(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeManagement, this::onErrorTreeManagement));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    private void onSuccessTreeManagement(AddTreeManagementModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeManagement(model);
        } else {
            contactInterface.onErrorTreeManagement(model.getMsg());
        }
    }



    private void onErrorTreeManagement(Throwable throwable) {
        contactInterface.onErrorTreeManagement(Utiles.getBodyError(throwable));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessTreeManagementList(TreeManagementListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeManagementList(model);
        } else {
            contactInterface.onErrorTreeManagementList(model.getMsg());
        }
    }

    private void onErrorTreeManagementList(Throwable throwable) {
        contactInterface.onErrorTreeManagementList(Utiles.getBodyError(throwable));
    }

    private void onSuccessDeleteTreeManagement(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessDeleteTreeManagement(model);
        } else {
            contactInterface.onErrorDeleteTreeManagement(model.getMsg());
        }
    }

    private void onErrorDeleteTreeManagement(Throwable throwable) {
        contactInterface.onErrorDeleteTreeManagement(Utiles.getBodyError(throwable));
    }


    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessTreeManagement(AddTreeManagementModel addTreeManagementModel);

        void onErrorTreeManagement(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessTreeManagementList(TreeManagementListModel addTreeManagementModel);

        void onErrorTreeManagementList(String error);

        void onSuccessDeleteTreeManagement(CommonModel commonModel);

        void onErrorDeleteTreeManagement(String error);

    }
}