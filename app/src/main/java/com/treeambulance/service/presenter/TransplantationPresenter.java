package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.SurveyDetailsModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class TransplantationPresenter {
    private final CompositeDisposable compositeDisposable;
    private TransplantationPresenter.ContactInterface contactInterface;

    public TransplantationPresenter(TransplantationPresenter.ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getListProjects(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getListProjects(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessListProjects, this::onErrorListProjects));
    }

    public void getTreeManagementList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getTreeManagementList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeManagementList, this::onErrorTreeManagementList));
    }

    private void onSuccessTreeManagementList(TreeManagementListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeManagementList(model);
        } else {
            contactInterface.onErrorTreeManagementList(model.getMsg());
        }
    }

    private void onErrorTreeManagementList(Throwable throwable) {
        contactInterface.onErrorTreeManagementList(Utiles.getBodyError(throwable));
    }

    public void getSurveyDetailsProjects(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getSurveyDetailsProjects(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessSurveyDetailsProjects, this::onErrorSurveyDetailsProjects));
    }

    public void getCountryList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCountryList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCountryList, this::onErrorCountryList));
    }

    public void getOperatorList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getOperatorList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOperatorList, this::onErrorOperatorList));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    public void getCheckTreeNo(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCheckTreeNo(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCheckTreeNo, this::onErrorCheckTreeNo));
    }

    public void getCreateTransplantation(HashMap<String, RequestBody> hashMap,
                                         MultipartBody.Part tree1Photo, MultipartBody.Part tree2Photo,
                                         MultipartBody.Part selfiePhoto) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreateTransplantation(hashMap, tree1Photo, tree2Photo, selfiePhoto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateTransplantation, this::onErrorCreateTransplantation));
    }

    private void onSuccessListProjects(ListProjectsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessListProjects(model);
        } else {
            contactInterface.onErrorListProjects(model.getMsg());
        }
    }

    private void onSuccessSurveyDetailsProjects(SurveyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessSurveyDetailsProjects(model);
        } else {
            contactInterface.onErrorSurveyDetailsProjects(model.getMsg());
        }
    }

    private void onSuccessOperatorList(OperatorListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessOperatorList(model);
        } else {
            contactInterface.onErrorOperatorList(model.getMsg());
        }
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessCountryList(CountryListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCountryList(model);
        } else {
            contactInterface.onErrorCountryList(model.getMsg());
        }
    }

    private void onErrorCountryList(Throwable throwable) {
        contactInterface.onErrorCountryList(Utiles.getBodyError(throwable));
    }

    private void onSuccessCheckTreeNo(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCheckTreeNo(model);
        } else {
            contactInterface.onErrorCheckTreeNo(model.getMsg());
        }
    }

    private void onSuccessCreateTransplantation(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateTransplantation(model);
        } else {
            contactInterface.onErrorCreateTransplantation(model.getMsg());
        }
    }

    private void onErrorListProjects(Throwable throwable) {
        contactInterface.onErrorListProjects(Utiles.getBodyError(throwable));
    }

    private void onErrorSurveyDetailsProjects(Throwable throwable) {
        contactInterface.onErrorSurveyDetailsProjects(Utiles.getBodyError(throwable));
    }

    private void onErrorOperatorList(Throwable throwable) {
        contactInterface.onErrorOperatorList(Utiles.getBodyError(throwable));
    }

    private void onErrorCheckTreeNo(Throwable throwable) {
        contactInterface.onErrorCheckTreeNo(Utiles.getBodyError(throwable));
    }

    private void onErrorCreateTransplantation(Throwable throwable) {
        contactInterface.onErrorCreateTransplantation(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessListProjects(ListProjectsModel listProjectsModel);

        void onErrorListProjects(String error);

        void onSuccessSurveyDetailsProjects(SurveyDetailsModel surveyDetailsModel);

        void onErrorSurveyDetailsProjects(String error);

        void onSuccessOperatorList(OperatorListModel operatorListModel);

        void onErrorOperatorList(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessCheckTreeNo(CommonModel model);

        void onErrorCheckTreeNo(String error);

        void onSuccessCreateTransplantation(CommonModel model);

        void onErrorCreateTransplantation(String error);

        void onSuccessCountryList(CountryListModel countryListModel);

        void onErrorCountryList(String error);

        void onSuccessTreeManagementList(TreeManagementListModel addTreeManagementModel);

        void onErrorTreeManagementList(String error);
    }
}
