package com.treeambulance.service.presenter;

import com.treeambulance.service.model.AddPlantationProjectModel;
import com.treeambulance.service.model.AddProjectsModel;
import com.treeambulance.service.model.AddTreeManagementModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.PlantationListModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class AddPlantationPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public AddPlantationPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getAddEditPlantation(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getAddEditPlantation(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessAddPlantationProjects, this::onErrorAddPlantationProjects));
    }

    public void getPlantationProject(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getPlantationProject(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessPlantationProjects, this::onErrorPlantationProjects));
    }

    public void getDeleteOperator(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getDeleteOperator(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessDeleteProjects, this::onErrorDeleteProjects));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    public void getCountryList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCountryList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCountryList, this::onErrorCountryList));
    }



    private void onSuccessAddPlantationProjects(AddPlantationProjectModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessAddPlantationProjects(model);
        } else {
            contactInterface.onErrorAddPlantationProjects(model.getMsg());
        }
    }



    private void onErrorAddPlantationProjects(Throwable throwable) {
        contactInterface.onErrorAddPlantationProjects(Utiles.getBodyError(throwable));
    }

    private void onSuccessPlantationProjects(PlantationListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessPlantationProjects(model);
        } else {
            contactInterface.onErrorPlantationProjects(model.getMsg());
        }
    }



    private void onErrorPlantationProjects(Throwable throwable) {
        contactInterface.onErrorPlantationProjects(Utiles.getBodyError(throwable));
    }


    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessCountryList(CountryListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCountryList(model);
        } else {
            contactInterface.onErrorCountryList(model.getMsg());
        }
    }

    private void onErrorCountryList(Throwable throwable) {
        contactInterface.onErrorCountryList(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    private void onSuccessDeleteProjects(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessDeleteProjects(model);
        } else {
            contactInterface.onErrorDeleteProjects(model.getMsg());
        }
    }

    private void onErrorDeleteProjects(Throwable throwable) {
        contactInterface.onErrorDeleteProjects(Utiles.getBodyError(throwable));
    }

    public interface ContactInterface {
        void onSuccessAddPlantationProjects(AddPlantationProjectModel addProjectsModel);

        void onErrorAddPlantationProjects(String error);

        void onSuccessPlantationProjects(PlantationListModel plantationListModel);

        void onErrorPlantationProjects(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessCountryList(CountryListModel countryListModel);

        void onErrorCountryList(String error);

        void onSuccessDeleteProjects(CommonModel commonModel);

        void onErrorDeleteProjects(String error);

    }
}