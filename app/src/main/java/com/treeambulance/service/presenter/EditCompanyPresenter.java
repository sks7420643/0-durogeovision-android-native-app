package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.EditCompanyModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class EditCompanyPresenter {

    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public EditCompanyPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }


    public void getEditCompany(HashMap<String, RequestBody> createSurveyingHashMap,
                                 MultipartBody.Part tree1Photo) {
        compositeDisposable.add(getRetrofitInstance()
                .getEditCompany(createSurveyingHashMap, tree1Photo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessEditCompany, this::onErrorEditCompany));
    }

    public void getEditCompanyData(HashMap<String, RequestBody> createSurveyingHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getEditCompanyData(createSurveyingHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessEditCompanyData, this::onErrorEditCompanyData));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessEditCompany(EditCompanyModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessEditCompany(model);
        } else {
            contactInterface.onErrorEditCompany(model.getMsg());
        }
    }

    private void onErrorEditCompany(Throwable throwable) {
        contactInterface.onErrorEditCompany(Utiles.getBodyError(throwable));
    }

    private void onSuccessEditCompanyData(EditCompanyModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessEditCompany(model);
        } else {
            contactInterface.onErrorEditCompany(model.getMsg());
        }
    }

    private void onErrorEditCompanyData(Throwable throwable) {
        contactInterface.onErrorEditCompany(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessEditCompany(EditCompanyModel model);

        void onErrorEditCompany(String error);

        void onSuccessEditCompanyData(EditCompanyModel model);

        void onErrorEditCompanyData(String error);

    }
}
