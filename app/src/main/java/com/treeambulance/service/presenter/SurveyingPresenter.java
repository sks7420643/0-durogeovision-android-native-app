package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class SurveyingPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public SurveyingPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getListProjects(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getListProjects(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessListProjects, this::onErrorListProjects));
    }

    public void getTreeManagementList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getTreeManagementList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeManagementList, this::onErrorTreeManagementList));
    }

    public void getCountryList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCountryList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCountryList, this::onErrorCountryList));
    }

    public void getOperatorList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getOperatorList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOperatorList, this::onErrorOperatorList));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    public void getCheckSurveyNo(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCheckSurveyNo(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCheckSurveyNo, this::onErrorCheckSurveyNo));
    }

    public void getCreateSurveying(HashMap<String, RequestBody> createSurveyingHashMap,
                                   MultipartBody.Part tree1Photo, MultipartBody.Part tree2Photo,
                                   MultipartBody.Part selfiePhoto) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreateSurveying(createSurveyingHashMap, tree1Photo, tree2Photo, selfiePhoto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateSurveying, this::onErrorCreateSurveying));
    }

    private void onSuccessListProjects(ListProjectsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessListProjects(model);
        } else {
            contactInterface.onErrorListProjects(model.getMsg());
        }
    }

    private void onErrorListProjects(Throwable throwable) {
        contactInterface.onErrorListProjects(Utiles.getBodyError(throwable));
    }

    private void onSuccessOperatorList(OperatorListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessOperatorList(model);
        } else {
            contactInterface.onErrorOperatorList(model.getMsg());
        }
    }

    private void onErrorOperatorList(Throwable throwable) {
        contactInterface.onErrorOperatorList(Utiles.getBodyError(throwable));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessCheckSurveyNo(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCheckSurveyNo(model);
        } else {
            contactInterface.onErrorCheckSurveyNo(model.getMsg());
        }
    }

    private void onErrorCheckSurveyNo(Throwable throwable) {
        contactInterface.onErrorCheckSurveyNo(Utiles.getBodyError(throwable));
    }

    private void onSuccessCreateSurveying(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateSurveying(model);
        } else {
            contactInterface.onErrorCreateSurveying(model.getMsg());
        }
    }

    private void onErrorCreateSurveying(Throwable throwable) {
        contactInterface.onErrorCreateSurveying(Utiles.getBodyError(throwable));
    }

    private void onSuccessCountryList(CountryListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCountryList(model);
        } else {
            contactInterface.onErrorCountryList(model.getMsg());
        }
    }

    private void onErrorCountryList(Throwable throwable) {
        contactInterface.onErrorCountryList(Utiles.getBodyError(throwable));
    }

    private void onSuccessTreeManagementList(TreeManagementListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeManagementList(model);
        } else {
            contactInterface.onErrorTreeManagementList(model.getMsg());
        }
    }

    private void onErrorTreeManagementList(Throwable throwable) {
        contactInterface.onErrorTreeManagementList(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessListProjects(ListProjectsModel listProjectsModel);

        void onErrorListProjects(String error);

        void onSuccessOperatorList(OperatorListModel operatorListModel);

        void onErrorOperatorList(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessCheckSurveyNo(CommonModel model);

        void onErrorCheckSurveyNo(String error);

        void onSuccessCreateSurveying(CommonModel model);

        void onErrorCreateSurveying(String error);

        void onSuccessCountryList(CountryListModel countryListModel);

        void onErrorCountryList(String error);

        void onSuccessTreeManagementList(TreeManagementListModel addTreeManagementModel);

        void onErrorTreeManagementList(String error);
    }
}
