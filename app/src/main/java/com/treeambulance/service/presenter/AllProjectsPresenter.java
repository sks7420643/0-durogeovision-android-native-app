package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class AllProjectsPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public AllProjectsPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getListProjects(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getListProjects(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessListProjects, this::onErrorListProjects));
    }

    public void getDeleteProjects(HashMap<String, String> deleteHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getDeleteProjects(deleteHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessDeleteProjects, this::onErrorDeleteProjects));
    }

    private void onSuccessListProjects(ListProjectsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessListProjects(model);
        } else {
            contactInterface.onErrorListProjects(model.getMsg());
        }
    }

    private void onErrorListProjects(Throwable throwable) {
        contactInterface.onErrorListProjects(Utiles.getBodyError(throwable));
    }

    private void onSuccessDeleteProjects(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessDeleteProjects(model);
        } else {
            contactInterface.onErrorDeleteProjects(model.getMsg());
        }
    }

    private void onErrorDeleteProjects(Throwable throwable) {
        contactInterface.onErrorDeleteProjects(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessListProjects(ListProjectsModel listProjectsModel);

        void onErrorListProjects(String error);

        void onSuccessDeleteProjects(CommonModel commonModel);

        void onErrorDeleteProjects(String error);
    }
}