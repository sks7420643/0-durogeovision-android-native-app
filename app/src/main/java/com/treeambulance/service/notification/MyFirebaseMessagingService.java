package com.treeambulance.service.notification;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.treeambulance.service.R;
import com.treeambulance.service.other.PrefManager;
import com.treeambulance.service.views.activity.SplashActivity;

import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import timber.log.Timber;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    static String CHANNEL_ID = "my_notification_channel";
    static int NOTIFICATION_ID = 100;
    PrefManager pref;
    String resource, title;
    Bitmap bitmap = null;

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);

        Timber.i("onNewToken: NEW_TOKEN: %s", s);
//        str_token = s;
        pref = new PrefManager(getApplicationContext());
//        pref.setToken(str_token);

        Timber.i("onNewToken: token: %s", pref.getToken());

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

//        Timber.d("Notification Message Body: %s", remoteMessage.getNotification().getBody());
        if (remoteMessage.getData() != null) {
            Timber.d("From: %s", remoteMessage.getFrom());
            Timber.d("Notification Message Body: %s", remoteMessage.getData());

            //Calling method to generate notification
            resource = remoteMessage.getData().get("image");

            System.out.println(" the resource " + resource);
            if (resource != null) {

                bitmap = getBitmapfromUrl(remoteMessage.getData().get("image"));

            }
            System.out.println("Bitmap" + bitmap);

            if (remoteMessage.getData().get("title") != null) {
                title = remoteMessage.getData().get("title");
            } else {
                title = "AutoAssistance";
            }
            //booking_id need to get this for further process
            checknotification(remoteMessage.getData().get("message"));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sendnotification(remoteMessage.getData().get("message"), bitmap, title, remoteMessage.getData().get("click_action"));
            } else {
                sendNotification(remoteMessage.getData().get("message"), bitmap, title, remoteMessage.getData().get("click_action"));
            }

        } else {
            Timber.d("FCM Notification failed");
        }
    }

    private Bitmap getBitmapfromUrl(String picture) {
        try {
            URL url = new URL(picture);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }

    }

    private void sendNotification(String messageBody, Bitmap bitmap, String title, String click_action) {
        Intent intent = null;
        intent = new Intent(this, SplashActivity.class);

        /*if (click_action != null) {
            if (click_action.equalsIgnoreCase("restaurant")) {
                intent = new Intent(this, BottomActivity.class);
            } else if (click_action.equalsIgnoreCase("catering")) {
                intent = new Intent(this, CateringMainActivity.class);
            }
        } else {
            intent = new Intent(this, CateringMainActivity.class);
        }*/

        intent.putExtra("status", "1");
        intent.putExtra("Notification", messageBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder;

        if (Build.VERSION.SDK_INT < 26) {
            notificationBuilder = new NotificationCompat.Builder(this);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this, "");
        }

        notificationBuilder.setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(messageBody))
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentIntent(pendingIntent);

        notificationBuilder.setSmallIcon(getNotificationIcon(notificationBuilder), 2);
        if (bitmap != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap));


            notificationBuilder.setLargeIcon(bitmap);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
            return R.drawable.img_app_icon;
        } else {
            return R.drawable.img_app_icon;
        }
    }

    @SuppressLint("WrongConstant")
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void Sendnotification(String messageBody, Bitmap bitmap, String title, String click_action) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        String id = "id_product";
        // The user-visible name of the channel.
        CharSequence name = "Product";
        // The user-visible description of the channel.
        String description = "Notifications regarding our products";
        int importance = NotificationManager.IMPORTANCE_MAX;
        @SuppressLint("WrongConstant")
        NotificationChannel mChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(id, name, importance);
        }
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.enableLights(true);

        // Sets the notification light color for notifications posted to this
        // channel, if the device supports this feature.
//        mChannel.setLightColor(R.color.colorPrimary);
        notificationManager.createNotificationChannel(mChannel);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = null;
        intent = new Intent(this, SplashActivity.class);
        /*if (click_action != null) {
            if (click_action.equalsIgnoreCase("restaurant")) {
                intent = new Intent(this, BottomActivity.class);
            } else if (click_action.equalsIgnoreCase("catering")) {
                intent = new Intent(this, CateringMainActivity.class);
            }
        } else {
            intent = new Intent(this, CateringMainActivity.class);
        }*/

        intent.putExtra("status", "1");
        intent.putExtra("Notification", messageBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 123, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), "id_product")
                .setChannelId(id)
                .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(messageBody))
                .setContentTitle(title)
                .setAutoCancel(true).setContentIntent(pendingIntent)
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setWhen(System.currentTimeMillis());
        notificationBuilder.setSmallIcon(getNotificationIcon(notificationBuilder), 1);
        if (bitmap != null) {
            notificationBuilder
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(bitmap));
            notificationBuilder.setLargeIcon(bitmap);
        }
        notificationManager.notify(1, notificationBuilder.build());
    }

    public void checknotification(String message) {
        if (message != null) {
            if (message.startsWith("Your order on the restaurant")) {
//                EventBus.getDefault().postSticky(new Filtertype(message));
            } else if (message.startsWith("Order From the restaurant")) {
//                EventBus.getDefault().postSticky(new Filtertype(message));
            }
        }
    }


}
