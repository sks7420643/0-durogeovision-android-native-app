package com.treeambulance.service.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.treeambulance.service.R;
import com.treeambulance.service.other.SharedHelper;

import java.util.Map;

import timber.log.Timber;

public class NotificationMessagingService extends FirebaseMessagingService {
    private String TAG = "NotificationMessagingService";

    private PendingIntent pendingIntent;
    /* {
        Intent intent =new  Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return getBookingIntent();
    }*/

    /*private PendingIntent getBookingIntent() {
        return NavDeepLinkBuilder(this)  //2
                .setGraph(R.navigation.nav_main_graph)  //3
                .setDestination(R.id.navigation_transaction)  //4
                .createPendingIntent();
    }*/

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Timber.i("From: " + remoteMessage.getFrom());
        // Check if `message contains a data payload.
        if (!remoteMessage.getData().isEmpty()) {
            Timber.d("Message data payload: %s", remoteMessage.getData());
            Timber.d("Message data: %s", remoteMessage.getData().get("type"));
            sendNotificationFromData(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //  Timber.d("Message Notification:"+remoteMessage.notification!!.get("<YOUR_KEY>");)
            Timber.d("Message Notification Body: %s", remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotificationFromData(Map<String, String> messageBody) {
//        PendingIntent pendingIntent = getBookingIntent();

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
//                .setSmallIcon(R.drawable.pushnotification_icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody.get("message"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);

        Timber.i("Refreshed token: " + token);

        addToSharedPreference(token);
    }

    private void addToSharedPreference(String token) {
        SharedHelper sharedHelper = new SharedHelper(getApplication());
        sharedHelper.putInCache(SharedHelper.FCM_TOKEN, token);
    }

    private void sendNotification(String messageBody) {
        PendingIntent pendingIntent = this.pendingIntent;

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
//                .setSmallIcon(R.drawable.pushnotification_icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
