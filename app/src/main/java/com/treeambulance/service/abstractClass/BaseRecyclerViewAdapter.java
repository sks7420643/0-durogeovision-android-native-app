package com.treeambulance.service.abstractClass;

import android.view.View;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.RecyclerViewListener;

import java.util.List;

public abstract class BaseRecyclerViewAdapter<T extends BaseRecyclerViewAdapter.BaseHolder, X>
        extends RecyclerView.Adapter<T> {

    private RecyclerViewListener recyclerViewListener;
    private List<X> data;

    public BaseRecyclerViewAdapter(List<X> data) {
        this.data = data;
    }

    public void onItemClick(View view, X obj) {
        if (recyclerViewListener != null) {
            int pos = data.indexOf(obj);
            recyclerViewListener.onRecyclerViewItem(pos, view);
        }
    }

    public void setRecyclerViewListener(RecyclerViewListener recyclerViewListener) {
        this.recyclerViewListener = recyclerViewListener;
    }

    public BaseRecyclerViewAdapter getAdapter() {
        return this;
    }

    public class BaseHolder extends RecyclerView.ViewHolder {
        public BaseHolder(ViewDataBinding itemView) {
            super(itemView.getRoot());
//            itemView.setVariable(BR.baseAdapter, getAdapter());
        }
    }

}