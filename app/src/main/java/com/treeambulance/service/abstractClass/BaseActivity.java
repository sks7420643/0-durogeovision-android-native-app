package com.treeambulance.service.abstractClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.splunk.mint.Mint;
import com.treeambulance.service.BuildConfig;
import com.treeambulance.service.R;
import com.treeambulance.service.other.CommonFunction;
import com.treeambulance.service.other.SharedHelper;

import org.greenrobot.eventbus.EventBus;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {

    public CommonFunction commonFunction;
    public SharedHelper sharedHelper;
    protected Activity activity;
    protected Context context;
    protected FragmentManager fragmentManager;
    protected BottomSheetDialogFragment bottomSheetDialogFragment;
    protected EventBus eventBus;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
            Mint.initAndStartSession(this.getApplication(), BuildConfig.splunkMintKey);
        } catch (Exception e) {
            Timber.e(e);
        }
//        setContentView(getLayoutResourceId());

        activity = this;
        context = this;

//        fragmentManager = new FragmentActivity().getSupportFragmentManager();
        fragmentManager = getSupportFragmentManager();
        eventBus = EventBus.getDefault();
        commonFunction = new CommonFunction(getApplication());
        sharedHelper = new SharedHelper(getApplication());

    }

//    protected abstract int getLayoutResourceId();

    protected void setIntent(Intent intend, int value) {
        startActivity(intend);
        switch (value) {
            case 1: {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
            break;
            case 2: {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                activity.finish();
            }
            break;
            case 3: {
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                activity.finishAffinity();
            }
            break;
        }
    }

    protected void moveToFragment(Fragment fragment, int id, Boolean first) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(id, fragment);
        if (!first) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void hideSoftKeyBoard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
            bottomSheetDialogFragment.dismiss();
        }
        commonFunction.dismissLoader();
    }
}