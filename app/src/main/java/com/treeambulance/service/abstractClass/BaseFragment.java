package com.treeambulance.service.abstractClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treeambulance.service.R;
import com.treeambulance.service.other.CommonFunction;
import com.treeambulance.service.other.SharedHelper;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseFragment extends Fragment {

    public CommonFunction commonFunction;
    public SharedHelper sharedHelper;
    protected Activity activity;
    protected Context context;
    protected FragmentManager fragmentManager;
    protected BottomSheetDialogFragment bottomSheetDialogFragment = null;
    protected EventBus eventBus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();
        context = getContext();

        fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();
        setRetainInstance(true);
        eventBus = EventBus.getDefault();
        commonFunction = new CommonFunction(activity.getApplication());
        sharedHelper = new SharedHelper(activity.getApplication());
    }

    protected void moveToFragment(Fragment fragment, int ids, Boolean first) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(ids, fragment);
        if (!first) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void setIntent(Intent intend, int value) {
        startActivity(intend);
        switch (value) {
            case 1: {
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
            break;
            case 2: {
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                activity.finish();
            }
            break;
            case 3: {
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                activity.finishAffinity();
            }
            break;
        }
    }

    public boolean isNetworkAvailable(Activity activity) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public void hideSoftKeyBoard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
            bottomSheetDialogFragment.dismiss();
        }
        commonFunction.dismissLoader();
    }
}