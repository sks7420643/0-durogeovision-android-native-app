package com.treeambulance.service.restapi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.treeambulance.service.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiClient {

    /*public static final String BASE_DOWNLOAD_URL = weakReference.get().getString(R.string.BASE_DOWNLOAD_URL);
    private static final String BASE_URL = weakReference.get().getString(R.string.BASE_URL);*/

    public static final String BASE_DOWNLOAD_URL = BuildConfig.baseUrl;
    private static final String BASE_URL = BuildConfig.baseUrl;
    private static final String BASE_GOOGLE_URL = "https://maps.googleapis.com/maps/api/";

    private static Retrofit retrofit = null, retrofitapi = null;

    public static Retrofit getClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);
        builder.connectTimeout(3, TimeUnit.MINUTES);
        builder.readTimeout(3, TimeUnit.MINUTES).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(builder.build())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient okHttpClient() {

        /*Interceptor interceptor = chain -> {
            Request newRequest = chain.request().newBuilder().addHeader("Authorization", "1d0311a08594a9ab52740a7403b6330541a8b9bd").build();
            return chain.proceed(newRequest);
        };*/

        HttpLoggingInterceptor interceptors = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG)
            interceptors.level(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient().newBuilder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .retryOnConnectionFailure(true)
                .writeTimeout(5, TimeUnit.MINUTES)
                .callTimeout(5, TimeUnit.MINUTES)
                .addInterceptor(interceptors)
                .build();
//                .addInterceptor(interceptor)
    }

    private static Retrofit getRxJavaRetrofitApi() {
        retrofitapi = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofitapi;
    }

    public static ApiInterface getRetrofitInstance() {
        return getRxJavaRetrofitApi().create(ApiInterface.class);
    }

    private static Retrofit getRxJavaRetrofitGoogleApi() {
        retrofitapi = new Retrofit.Builder()
                .baseUrl(BASE_GOOGLE_URL)
                .client(okHttpClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofitapi;
    }

    public static ApiInterface getGeoRetrofitInstance() {
        return getRxJavaRetrofitGoogleApi().create(ApiInterface.class);
    }
}
