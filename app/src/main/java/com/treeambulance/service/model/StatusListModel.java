package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusListModel {

    @Expose
    @SerializedName("to")
    private String to;
    @Expose
    @SerializedName("from")
    private String from;
    @Expose
    @SerializedName("isChecked")
    private boolean isChecked;

    public StatusListModel(String from, String to, boolean isChecked) {
        this.to = to;
        this.from = from;
        this.isChecked = isChecked;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
