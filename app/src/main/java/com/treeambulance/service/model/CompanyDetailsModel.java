package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CompanyDetailsModel {

    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("result")
    private Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public static class Result {
        @Expose
        @SerializedName("rfid_list")
        private ArrayList<RfidList> rfidList;
        @Expose
        @SerializedName("gps_list")
        private ArrayList<GpsList> gpsList;
        @Expose
        @SerializedName("logo_image")
        private String logoName;
        @Expose
        @SerializedName("company_name")
        private String companyName;
        @Expose
        @SerializedName("id")
        private String id;

        public ArrayList<RfidList> getRfidList() {
            return rfidList;
        }

        public void setRfidList(ArrayList<RfidList> rfidList) {
            this.rfidList = rfidList;
        }

        public ArrayList<GpsList> getGpsList() {
            return gpsList;
        }

        public void setGpsList(ArrayList<GpsList> gpsList) {
            this.gpsList = gpsList;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getLogoName() {
            return logoName;
        }

        public void setLogoName(String logoName) {
            this.logoName = logoName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class RfidList {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("isChecked")
        private String ischecked;
        @Expose
        @SerializedName("bondState")
        private String bondstate;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("company_id")
        private String companyId;
        @Expose
        @SerializedName("id")
        private String id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIschecked() {
            return ischecked;
        }

        public void setIschecked(String ischecked) {
            this.ischecked = ischecked;
        }

        public String getBondstate() {
            return bondstate;
        }

        public void setBondstate(String bondstate) {
            this.bondstate = bondstate;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class GpsList {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("isChecked")
        private String ischecked;
        @Expose
        @SerializedName("bondState")
        private String bondstate;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("company_id")
        private String companyId;
        @Expose
        @SerializedName("id")
        private String id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIschecked() {
            return ischecked;
        }

        public void setIschecked(String ischecked) {
            this.ischecked = ischecked;
        }

        public String getBondstate() {
            return bondstate;
        }

        public void setBondstate(String bondstate) {
            this.bondstate = bondstate;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}
