package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportTypeModel {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id = null;
    private boolean isChecked = false;

    public String getReportType() {
        return name;
    }

    public void setReportType(String state) {
        this.name = state;
    }

    public String getReportId() {
        return id;
    }

    public void setReportId(String id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
