package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrencyNameModel {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id = null;
    private boolean isChecked = false;

    public String getCurrencyName() {
        return name;
    }

    public void setCurrencyName(String state) {
        this.name = state;
    }

    public String getCurrencyId() {
        return id;
    }

    public void setCurrencyId(String id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
