package com.treeambulance.service.model;

import java.io.Serializable;

public class GPSModel implements Serializable {


    private int gpsId;

    private Double lon;

    private Double lat;

    private String udd;

    private int sync;

    private int remoteId;

    public GPSModel() {
    }

    public GPSModel(Double lon, Double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public GPSModel(Double lon, Double lat, int sync) {
        this.lon = lon;
        this.lat = lat;
        this.sync = sync;
    }

    public GPSModel(Double lon, Double lat, String udd) {
        this.lon = lon;
        this.lat = lat;
        this.udd = udd;
    }

    public GPSModel(int id, double lon, double lat) {
        this.gpsId = id;
        this.lon = lon;
        this.lat = lat;
    }

    public String getUdd() {
        return udd;
    }

    public void setUdd(String udd) {
        this.udd = udd;
    }


    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public int getGpsId() {
        return gpsId;
    }

    public void setGpsId(int gpsId) {
        this.gpsId = gpsId;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String toString() {
        return "{lon:+" + this.lon + ",lat:" + this.lat + "}";
    }

    public int getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(int remoteId) {
        this.remoteId = remoteId;
    }
}
