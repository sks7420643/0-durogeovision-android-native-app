package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.RequestBody;

public class SurveyDetailsModel {

    @Expose
    @SerializedName("result")
    private Result result;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class Result {
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("selfie")
        private String selfie;
        @Expose
        @SerializedName("photo2")
        private String photo2;
        @Expose
        @SerializedName("photo1")
        private String photo1;
        @Expose
        @SerializedName("notes")
        private String notes;

        @Expose
        @SerializedName("land_owner_name")
        private String nameLand;
        @Expose
        @SerializedName("old_tree")
        private String oldTree;
        @Expose
        @SerializedName("new_tree")
        private String newTree;
        @Expose
        @SerializedName("land_survey_number")
        private String lanSurveyNumber;
        @Expose
        @SerializedName("height_tree")
        private String heightTree;
        @Expose
        @SerializedName("aprx_value")
        private String appValue;
        @Expose
        @SerializedName("aprx_new_value")
        private String appNewValue;
        @Expose
        @SerializedName("location_name")
        private String locationName;
        @Expose
        @SerializedName("contractor_name")
        private String contractorName;
        @Expose
        @SerializedName("operator_name")
        private String operatorName;
        @Expose
        @SerializedName("tree_age")
        private String treeAge;
        @Expose
        @SerializedName("grith_size")
        private String grithSize;
        @Expose
        @SerializedName("health_status")
        private String healthStatus;
        @Expose
        @SerializedName("local_tree")
        private String localName;
        @Expose
        @SerializedName("botanical_tree")
        private String botanicalName;
        @Expose
        @SerializedName("village")
        private String village;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("district")
        private String district;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("project_id")
        private String projectId;
        @Expose
        @SerializedName("survey_no")
        private String surveyNo;
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("longitude")
        private String longitude;

        public String getNameLand() {
            return nameLand;
        }

        public void setNameLand(String nameLand) {
            this.nameLand = nameLand;
        }

        public void setLanSurveyNumber(String lanSurveyNumber) {
            this.lanSurveyNumber = lanSurveyNumber;
        }

        public String getLanSurveyNumber() {
            return lanSurveyNumber;
        }

        public String getOldTree() {
            return oldTree;
        }

        public void setOldTree(String oldTree) {
            this.oldTree = oldTree;
        }

        public String getNewTree() {
            return newTree;
        }

        public void setNewTree(String newTree) {
            this.newTree = newTree;
        }


        public String getHeightTree() {
            return heightTree;
        }

        public void setHeightTree(String heightTree) {
            this.heightTree = heightTree;
        }

        public String getAppValue() {
            return appValue;
        }
        public void setAppValue(String appValue) {
            this.appValue = appValue;
        }

        public String getAppNewValue() {
            return appNewValue;
        }

        public void setAppNewValue(String appNewValue) {
            this.appNewValue = appNewValue;
        }

        public String getLocationName() {
            return locationName;
        }

        public void setLocationName(String locationName) {
            this.locationName = locationName;
        }

        public String getContractorName() {
            return contractorName;
        }

        public void setContractorName(String contractorName) {
            this.surveyNo = contractorName;
        }


        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getSelfie() {
            return selfie;
        }

        public void setSelfie(String selfie) {
            this.selfie = selfie;
        }

        public String getPhoto2() {
            return photo2;
        }

        public void setPhoto2(String photo2) {
            this.photo2 = photo2;
        }

        public String getPhoto1() {
            return photo1;
        }

        public void setPhoto1(String photo1) {
            this.photo1 = photo1;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getOperatorName() {
            return operatorName;
        }

        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        public String getTreeAge() {
            return treeAge;
        }

        public void setTreeAge(String treeAge) {
            this.treeAge = treeAge;
        }

        public String getGrithSize() {
            return grithSize;
        }

        public void setGrithSize(String grithSize) {
            this.grithSize = grithSize;
        }

        public String getHealthStatus() {
            return healthStatus;
        }

        public void setHealthStatus(String healthStatus) {
            this.healthStatus = healthStatus;
        }

        public String getLocalName() {
            return localName;
        }

        public void setLocalName(String localName) {
            this.localName = localName;
        }

        public String getBotanicalName() {
            return botanicalName;
        }

        public void setBotanicalName(String botanicalName) {
            this.botanicalName = botanicalName;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getProjectId() {
            return projectId;
        }

        public void setProjectId(String projectId) {
            this.projectId = projectId;
        }

        public String getSurveyNo() {
            return surveyNo;
        }

        public void setSurveyNo(String surveyNo) {
            this.surveyNo = surveyNo;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

    }
}
