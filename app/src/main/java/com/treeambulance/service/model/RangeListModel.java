package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RangeListModel {

    @Expose
    @SerializedName("value")
    private double value;
    @Expose
    @SerializedName("unit")
    private String unit;

    public RangeListModel(double value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
