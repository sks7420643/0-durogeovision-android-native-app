package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SurveyListDetailsModel {

    @Expose
    @SerializedName("result")
    private ArrayList<Result> result;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Result {
        @Expose
        @SerializedName("project_name")
        private String projectName;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("rfid_code")
        private String rfidCode;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("selfie")
        private String selfie;
        @Expose
        @SerializedName("photo2")
        private String photo2;
        @Expose
        @SerializedName("photo1")
        private String photo1;
        @Expose
        @SerializedName("notes")
        private String notes;
        @Expose
        @SerializedName("operator_name")
        private String operatorName;
        @Expose
        @SerializedName("tree_age")
        private String treeAge;
        @Expose
        @SerializedName("grith_size")
        private String grithSize;
        @Expose
        @SerializedName("health_status")
        private String healthStatus;
        @Expose
        @SerializedName("local_name")
        private String localName;
        @Expose
        @SerializedName("botanical_name")
        private String botanicalName;
        @Expose
        @SerializedName("survey_location")
        private String surveyLocation;
        @Expose
        @SerializedName("village")
        private String village;
        @Expose
        @SerializedName("district")
        private String district;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("company_id")
        private String companyId;
        @Expose
        @SerializedName("project_id")
        private String projectId;
        @Expose
        @SerializedName("survey_no")
        private String surveyNo;
        @Expose
        @SerializedName("id")
        private String id;

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getRfidCode() {
            return rfidCode;
        }

        public void setRfidCode(String rfidCode) {
            this.rfidCode = rfidCode;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getSelfie() {
            return selfie;
        }

        public void setSelfie(String selfie) {
            this.selfie = selfie;
        }

        public String getPhoto2() {
            return photo2;
        }

        public void setPhoto2(String photo2) {
            this.photo2 = photo2;
        }

        public String getPhoto1() {
            return photo1;
        }

        public void setPhoto1(String photo1) {
            this.photo1 = photo1;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getOperatorName() {
            return operatorName;
        }

        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        public String getTreeAge() {
            return treeAge;
        }

        public void setTreeAge(String treeAge) {
            this.treeAge = treeAge;
        }

        public String getGrithSize() {
            return grithSize;
        }

        public void setGrithSize(String grithSize) {
            this.grithSize = grithSize;
        }

        public String getHealthStatus() {
            return healthStatus;
        }

        public void setHealthStatus(String healthStatus) {
            this.healthStatus = healthStatus;
        }

        public String getLocalName() {
            return localName;
        }

        public void setLocalName(String localName) {
            this.localName = localName;
        }

        public String getBotanicalName() {
            return botanicalName;
        }

        public void setBotanicalName(String botanicalName) {
            this.botanicalName = botanicalName;
        }

        public String getSurveyLocation() {
            return surveyLocation;
        }

        public void setSurveyLocation(String surveyLocation) {
            this.surveyLocation = surveyLocation;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getProjectId() {
            return projectId;
        }

        public void setProjectId(String projectId) {
            this.projectId = projectId;
        }

        public String getSurveyNo() {
            return surveyNo;
        }

        public void setSurveyNo(String surveyNo) {
            this.surveyNo = surveyNo;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}