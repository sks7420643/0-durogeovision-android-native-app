package com.treeambulance.service.model;

public class BluetoothDeviceListModel {
    String address = "";
    int bondState = 0;
    String name = "";
    int type = 0;
    boolean isChecked = false;

    public BluetoothDeviceListModel(String address, int bondState, String name, int type, boolean isChecked) {
        this.address = address;
        this.bondState = bondState;
        this.name = name;
        this.type = type;
        this.isChecked = isChecked;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBondState() {
        return bondState;
    }

    public void setBondState(int bondState) {
        this.bondState = bondState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
