package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.model.ReportTypeModel;
import com.treeambulance.service.model.StateCityModel;

import java.util.ArrayList;

public class ReportTypeAdapter extends RecyclerView.Adapter<ReportTypeAdapter.ViewHolder> {

    ArrayList<ReportTypeModel> reportTypeModel = new ArrayList<>();
    OnItemClickListener onItemClickListener;

    public ReportTypeAdapter(ArrayList<ReportTypeModel> reportTypeModel, OnItemClickListener onItemClickListener) {
        this.reportTypeModel = reportTypeModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_report_type, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvItemName.setText(reportTypeModel.get(position).getReportType());
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return reportTypeModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
        }
    }
}
