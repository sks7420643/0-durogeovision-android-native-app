package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterMultiListBinding;
import com.treeambulance.service.model.StateCityModel;

import java.util.ArrayList;

public class StateMultiListAdapter extends RecyclerView.Adapter<StateMultiListAdapter.ViewHolder> {

    private ArrayList<StateCityModel> stateCityModels;
    private OnStateMultiListSelected onStateMultiListSelected;

    public StateMultiListAdapter(ArrayList<StateCityModel> stateCityModels, OnStateMultiListSelected onStateMultiListSelected) {
        this.stateCityModels = stateCityModels;
        this.onStateMultiListSelected = onStateMultiListSelected;
    }

    @NonNull
    @Override
    public StateMultiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_multi_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StateMultiListAdapter.ViewHolder holder, int position) {
        holder.binding.tvItemName.setText(stateCityModels.get(position).getState());
        holder.binding.cbItem.setChecked(stateCityModels.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (stateCityModels.get(position).isChecked()) {
                onStateMultiListSelected.onStateSelected(position, false);
            } else if (!stateCityModels.get(position).isChecked()) {
                onStateMultiListSelected.onStateSelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stateCityModels.size();
    }

    public interface OnStateMultiListSelected {
        public void onStateSelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterMultiListBinding binding;

        public ViewHolder(@NonNull AdapterMultiListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
