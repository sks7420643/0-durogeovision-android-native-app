package com.treeambulance.service.views.fragment;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.bluetoothReader.BluetoothChatServiceForContinuosData;
import com.treeambulance.service.databinding.FragmentDataRetrievalBinding;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.GPSModel;
import com.treeambulance.service.model.ListPlantationModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.PlantationListModel;
import com.treeambulance.service.model.ProjectListDetailsModel;
import com.treeambulance.service.model.RangeListModel;
import com.treeambulance.service.model.SurveyListDetailsModel;
import com.treeambulance.service.presenter.DataRetrievalPresenter;
import com.treeambulance.service.presenter.PlantationRetrievalPresenter;
import com.treeambulance.service.utility.Constants;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllPlantationBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllProjectsBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.RangeListBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class PlantationRetrievalFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback,
        PlantationRetrievalPresenter.ContactInterface, AllPlantationBottomSheetFragment.OnPlantationClickListener,
        RangeListBottomSheetFragment.OnRangeClickListener, GoogleMap.OnMarkerClickListener {

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
            }
        }
    };
    private List<Marker> markerTransplantationList = new ArrayList<>(), markerSurveyList = new ArrayList<>();
    private FragmentDataRetrievalBinding binding;
    private SupportMapFragment supportMapFragment;
    private GoogleMap googleMap;
    private HashMap<String, String> listProjectsHashMap, projectListDetailsHashMap, surveyListDetailsHashMap, getCompanyDetailsHashMap;
    private PlantationListModel projectListDetailsModel;
    private HashMap<String, String> allProjectsHashMap;
    private SurveyListDetailsModel surveyListDetailsModel;
    private ArrayList<RangeListModel> rangeListModels;
    private ListProjectsModel listProjectsModel;
    private String projectId = "";
    private PlantationRetrievalPresenter plantationPresenter;
    private MarkerOptions markerOptionsCurrentLocation, markerOptionsTransplantation, markerOptionsSurvey;
    private LatLng latLng;
    private Circle circle, destCircle;
    private Location mCurrentLocation;
    private float distance = 0;
    private int counter = 0;
    private boolean secure;
    private Marker markerCurrentLocation, markerTransplantation, markerSurvey, selectedMarker;

    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<String> GPSDeviceList = new ArrayList<>();

    private BluetoothChatServiceForContinuosData mChatService = null;
    private String mConnectedDeviceName = "";
    private ListPlantationModel listPlantationModel;
    private GPSModel gpsModel;
    private int updateMarkerCounter = 0, surveyVisibleCount = 0, transplantationVisibleCount = 0;
    private LatLng currentLocation;
    private boolean firstAlert = true;
    private AlertDialog.Builder alertDialogBuilder = null;
    /**
     * The Handler that gets information back from the BluetoothChatServiceForContinuosData
     */
    private final Handler mHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
//            FragmentActivity activity = getActivity();
            Timber.i("handleMessage: %s", new Gson().toJson(msg));
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
//                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTING:
//                            setStatus(String.valueOf(R.string.title_connecting));
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_LISTEN:
                        case BluetoothChatServiceForContinuosData.STATE_NONE:
//                            setStatus(String.valueOf(R.string.title_not_connected));
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
//                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_LONG).show();
                    GPSModel currentGPS = sendToProcessMessage(readMessage);
                    if (currentGPS != null) {
//                        actionBar.setText(String.format("In Handler lat : %S , lon : %S", currentGPS.getLat(), currentGPS.getLon()));
                        binding.tvDistance.setText(String.format("In Handler lat : %S , lon : %S", currentGPS.getLat(), currentGPS.getLon()));
                        updateMarker(currentGPS);
                    }
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
//                    if (null != activity) {

                    Timber.i("MESSAGE_DEVICE_NAME: %s", new Gson().toJson(msg.getData()));

                    /*Toast.makeText(context, "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();*/
                    commonFunction.showShortToast("Connected to " + mConnectedDeviceName);
//                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    /*commonFunction.showShortToast(msg.getData().getString(Constants.TOAST));

                    if ("Unable to connect device".equalsIgnoreCase(msg.getData().getString(Constants.TOAST)))
                        counter++;
                    if (counter == 11) {
                        showFailedMessage("Bluetooth Connection Failed", "Retry?");
                    }*/
                    if (msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Unable to connect device") || msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Device connection was lost")) {
                        counter++;
                        Timber.i("");
//                        map.getUiSettings().setZoomControlsEnabled(false);
                        if (counter < GPSDeviceList.size()) {
                            attempt();
                        }
                    }
                    if (counter == GPSDeviceList.size()) {

                        commonFunction.showShortToast(msg.getData().getString(Constants.TOAST));
//                        binding.btRefresh.performClick();
//                        showFailedMessage("Bluetooth Connection Failed", "Retry?");
                    }

                    break;
            }
        }
    };
    private Map<Marker, PlantationListModel.Result> markerTransplantationMap;
    private Map<Marker, SurveyListDetailsModel.Result> markerSurveyMap;
    private ArrayList<String> GPSFromBluetooth, GPSSelectedBluetooth;

    public PlantationRetrievalFragment(ArrayList<String> GPSDeviceList) {
        this.GPSDeviceList = GPSDeviceList;
        Timber.i("DataRetrievalFragment: GPSDeviceList %s", new Gson().toJson(this.GPSDeviceList));
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plantation_retrieval, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        supportMapFragment.getMapAsync(this);
        listPlantationModel = new ListPlantationModel();
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        // Initialize the BluetoothChatServiceForContinuosData to perform bluetooth connections
        mChatService = new BluetoothChatServiceForContinuosData(context, mHandler);

        plantationPresenter = new PlantationRetrievalPresenter(this);

        listProjectsHashMap = new HashMap<>();
        allProjectsHashMap = new HashMap<>();
        projectListDetailsHashMap = new HashMap<>();
        surveyListDetailsHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();

        markerTransplantationMap = new HashMap<>();
        markerSurveyMap = new HashMap<>();

        listProjectsModel = new ListProjectsModel();
        projectListDetailsModel = new PlantationListModel();
        surveyListDetailsModel = new SurveyListDetailsModel();

        rangeListModels = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();

        // Attempt to connect to the device
        secure = false;

        attempt();

        hitGetAllProjects();
        addRange();
    }

    private void attempt() {
        if (GPSDeviceList != null && !GPSDeviceList.isEmpty()) {
            Timber.i("attempt: counter " + counter + " - " + GPSDeviceList.get(counter));
            BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(GPSDeviceList.get(counter));
            mChatService.connect(device1, secure);
        }
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(String resId) {
        Context context1 = context;
        if (null == context1) {
            return;
        }
//        binding.tvStatus.setText(binding.tvStatus.getText() + " , " + resId);
        Timber.i("setStatus %s", resId);
    }

    private GPSModel sendToProcessMessage(String wholeString) {

        String lineSeparatedArray[] = wholeString.split("\n");
        if (lineSeparatedArray.length > 1) {
            //Check if we have lan lat in our line
            for (int i = 0; i < lineSeparatedArray.length; i++) {
                String[] splitByGNRMC = lineSeparatedArray[i].split("GNRMC");
                if (splitByGNRMC.length > 1) {
//                    gnrmc.setText(splitByGNRMC[1]);
                    try {
                        Timber.i("sendToProcessMessage: splitByGNRMC: " + splitByGNRMC[1].split(",")[3] + "\t" + splitByGNRMC[1].split(",")[5]);
                        double lat = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[3]));
                        double lon = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[5]));
                        Timber.i("sendToProcessMessage: lon: " + lon + "\t lat: " + lat);
                        gpsModel = new GPSModel(lon, lat);
//                        actionBar.setText(String.format("In Processing Function lat : %S , lon : %S ", currentGPS.getLat(), currentGPS.getLon()));
                        binding.tvDistance.setText(String.format("In Processing Function lat : %S , lon : %S ", gpsModel.getLat(), gpsModel.getLon()));
                        updateMarker(gpsModel);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Timber.e("sendToProcessMessage: splitData: %s", e);
                    }
                }
                Timber.i("sendToProcessMessage: lineSeparatedArray - " + lineSeparatedArray[i] + "\t" + i);
            }
            //If we get lon lat
            return gpsModel;
        }
        return gpsModel;
    }

    private void updateMarker(GPSModel currentGPS) {
        updateMarkerCounter++;
//      actionBar.setText(String.format("%S: In Update Marker lat : %S , lon : %S with distance : %S ", updateMarkerCounter + " ", currentGPS.getLat(), currentGPS.getLon(), distance));
        binding.tvDistance.setText(String.format("%S: In Update Marker lat : %S , lon : %S with distance : %S ", updateMarkerCounter + " ", currentGPS.getLat(), currentGPS.getLon(), distance));
        currentLocation = new LatLng(currentGPS.getLat(), currentGPS.getLon());
        markerCurrentLocation.setPosition(currentLocation);
//      googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

        if (firstAlert && destCircle != null && isCircleContains(destCircle, new LatLng(currentGPS.getLat(), currentGPS.getLon()))) {
            firstAlert = false;
//            mChatService.stop();

            final String CHANNEL_ID = "DurotekChannel";
            commonFunction.showLongToast("Marker Reached: " + selectedMarker.getSnippet());

            // do something when the button is clicked

            if (alertDialogBuilder == null) {
                alertDialogBuilder = new AlertDialog.Builder(context)
                        .setMessage(String.format("Marker %S Reached", selectedMarker.getSnippet()))
                        .setPositiveButton("OK", (arg0, arg1) -> {
                            firstAlert = true;
//                            mChatService.start();
                            alertDialogBuilder = null;
                        /*firstAlert = true;
                        Intent intent1 = new Intent(getApplicationContext(), ServiceOrRemoveActivity.class);
                        intent1.putExtra("Project", project);
                        intent1.putExtra("Area", area);
                        intent1.putExtra("Operator", operator);
                        intent1.putExtra("Marker", markerType);
                        intent1.putExtra("DataLogs", selectedDataLog);
                        startActivity(intent1);*/
                        })
                        .setCancelable(false);
                alertDialogBuilder.show();
            }

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.img_app_logo)
                            .setContentTitle("DUROTEK")
                            .setContentText(String.format("Marker %S Reached", selectedMarker.getSnippet()))
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_CALL);

// notificationId is a unique int for each notification that you must define
            NotificationManagerCompat notificationManager;
            notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(1, builder.build());
        }

    }

    /*
     * Convert a NMEA decimal-decimal degree value into degrees/minutes/seconds
     * First. convert the decimal-decimal value to a decimal:
     * 5144.3855 (ddmm.mmmm) = 51 44.3855 = 51 + 44.3855/60 = 51.7397583 degrees
     *
     * Then convert the decimal to degrees, minutes seconds:
     * 51 degress + .7397583 * 60 = 44.385498 = 44 minutes
     * .385498 = 23.1 seconds
     * Result: 51 44' 23.1"
     *
     * @return String value of the decimal degree, using the proper units
     */
    private double decimalToDMS(double value) {
        Timber.i("decimalToDMS: value %s", value);
        int degrees = (int) (value / 100);
        Timber.i("decimalToDMS: degrees %s", degrees);
        double minutes = (value - degrees * 100) / 60;
        Timber.i("decimalToDMS: minutes %s", minutes);
        return degrees + minutes;
    }

    private void showFailedMessage(String message, String title) {

        // Create the object of
        // AlertDialog Builder class
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(context);

        // Set the message show for the Alert time
        builder.setMessage(message);

        // Set Alert Title
        builder.setTitle(title);

        // Set Cancelable false
        // for when the user clicks on the outside
        // the Dialog Box then it will remain show
        builder.setCancelable(false);

        // Set the positive button with yes name
        // OnClickListener method is use of
        // DialogInterface interface.

        builder.setPositiveButton(
                "Ok",
                new DialogInterface
                        .OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
//                        When the user click yes button
//                        then app will close
//                        retryBluetoothConnection(null);

                        fragmentManager.popBackStack();
                        Timber.i("showFailedMessage PositiveButton");
//                        moveToFragment(new DataRetrievalFragment(GPSDeviceList), android.R.id.content, false);
                    }
                })
                .setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }
                );

        // Create the Alert dialog
        AlertDialog alertDialog = builder.create();
        if (!activity.isFinishing()) {
            //show dialog
            // Show the Alert Dialog box
            alertDialog.show();
        }
    }

    private void addRange() {
        rangeListModels.add(new RangeListModel(200, "m"));
        rangeListModels.add(new RangeListModel(500, "m"));
        rangeListModels.add(new RangeListModel(1, "km"));
        rangeListModels.add(new RangeListModel(1.5, "km"));
        rangeListModels.add(new RangeListModel(3, "km"));
        rangeListModels.add(new RangeListModel(5, "km"));
        rangeListModels.add(new RangeListModel(10, "km"));
    }

    private void hitGetAllProjects() {
        allProjectsHashMap.put("action", "list_plantation");
        allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getListPlantations(allProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetTransplantationListDetails() {
        projectListDetailsHashMap.put("action", "plantation_project_list_details");
        projectListDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
        projectListDetailsHashMap.put("plantation_project_id", projectId);

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getProjectListDetails(projectListDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

//    private void hitGetSurveyListDetails() {
//        surveyListDetailsHashMap.put("action", "survey_list_details");
//        surveyListDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
//        surveyListDetailsHashMap.put("project_id", projectId);
//
//        if (isNetworkAvailable(activity)) {
//            commonFunction.showLoader(activity);
//            plantationPresenter.getSurveyListDetails(surveyListDetailsHashMap);
//        } else {
//            commonFunction.showNoNetwork(activity);
//        }
//    }

    private void addTransplantationMarkers() {

        for (int i = 0; i < markerTransplantationList.size(); i++) {
            markerTransplantationList.get(i).remove();
        }

        if (projectListDetailsModel != null && projectListDetailsModel.getResult() != null && !projectListDetailsModel.getResult().isEmpty()) {
            transplantationVisibleCount = 0;
            setTransplantationMarkers();

        } else {
            binding.etTransplantationTreeCount.setText("0");
        }
    }

    private void addSurveyMarkers() {

//        binding.etSurveyTreeCount.setText("" + surveyListDetailsModel.getResult().size());

        for (int i = 0; i < markerSurveyList.size(); i++) {
            markerSurveyList.get(i).remove();
        }

        if (surveyListDetailsModel != null && surveyListDetailsModel.getResult() != null && !surveyListDetailsModel.getResult().isEmpty()) {
            surveyVisibleCount = 0;
            setSurveyMarkers();
        } else {
            binding.etSurveyTreeCount.setText("0");
        }
    }

    private void setSurveyMarkers() {
        for (int i = 0; i < markerSurveyList.size(); i++) {
            markerSurveyList.get(i).remove();
        }
        if (surveyListDetailsModel.getResult() != null)
            for (SurveyListDetailsModel.Result list : surveyListDetailsModel.getResult()) {
                if (commonFunction.NullPointerValidator(list.getLatitude()) && commonFunction.NullPointerValidator(list.getLongitude())) {

                    latLng = new LatLng(Double.parseDouble(list.getLatitude()), Double.parseDouble(list.getLongitude()));

                    markerOptionsSurvey = new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                            .snippet(list.getSurveyNo())
                            .title(list.getRfidCode());
                    markerSurvey = googleMap.addMarker(markerOptionsSurvey);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 25));
                    markerSurveyMap.put(markerSurvey, list);
                    markerSurveyList.add(markerSurvey);
                    Timber.i("addMarkers: %s", markerSurvey.getTitle());

                    if (markerSurvey != null) {

                        if (!isCircleContains(circle, markerSurvey.getPosition())) {
                            markerSurvey.setVisible(false);
                        } else {
                            markerSurvey.setVisible(true);
                            surveyVisibleCount++;
                        }

                        String str_Count = surveyVisibleCount + " / " + surveyListDetailsModel.getResult().size();
                        binding.etSurveyTreeCount.setText(str_Count);
                    } else {
                        binding.etSurveyTreeCount.setText("0");
                    }
                }
            }

    }

    private void setTransplantationMarkers() {
        for (int i = 0; i < markerTransplantationList.size(); i++) {
            markerTransplantationList.get(i).remove();
        }
        if (projectListDetailsModel.getResult() != null)
            for (PlantationListModel.Result list : projectListDetailsModel.getResult()) {
                if (commonFunction.NullPointerValidator(list.getLatitude()) && commonFunction.NullPointerValidator(list.getLongitude())) {

                    latLng = new LatLng(Double.parseDouble(list.getLatitude()), Double.parseDouble(list.getLongitude()));

                    markerOptionsTransplantation = new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .snippet(list.getNewTreeNo())
                            .title(list.getRfidCode());
                    markerTransplantation = googleMap.addMarker(markerOptionsTransplantation);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 25));
                    markerTransplantationMap.put(markerTransplantation, list);
                    markerTransplantationList.add(markerTransplantation);
                    Timber.i("addTransplantationMarkers: %s", markerTransplantation.getTitle());
//                setTransplantationMarkers();

                    if (markerTransplantation != null) {

                        if (!isCircleContains(circle, markerTransplantation.getPosition())) {
                            markerTransplantation.setVisible(false);
                        } else {
                            markerTransplantation.setVisible(true);
                            transplantationVisibleCount++;
                        }

                        String str_Count = transplantationVisibleCount + " / " + projectListDetailsModel.getResult().size();
                        binding.etTransplantationTreeCount.setText(str_Count);
                    } else {
                        binding.etTransplantationTreeCount.setText("0");
                    }
                }
            }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackDataRetrieval: {
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.etProjectName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (listPlantationModel != null && listPlantationModel.getResult() != null && !listPlantationModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new AllPlantationBottomSheetFragment(listPlantationModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "all_project_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etRange:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (rangeListModels != null && !rangeListModels.isEmpty()) {
                    bottomSheetDialogFragment = new RangeListBottomSheetFragment(rangeListModels, this);
                    bottomSheetDialogFragment.show(fragmentManager, "range_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Range List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
        }
    }

    /*@Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.setOnMarkerClickListener(this);
    }*/

    private boolean isCircleContains(Circle circle, LatLng point) {
        double r = circle.getRadius();
        LatLng center = circle.getCenter();
        double cX = center.latitude;
        double cY = center.longitude;
        double pX = point.latitude;
        double pY = point.longitude;

        float[] results = new float[1];

        Location.distanceBetween(cX, cY, pX, pY, results);
        distance = results[0];
        /*if (currentGPS != null)
            actionBar.setText(updateMarkerCounter + ": In isCircleContains : " + currentGPS.toString() + "\t" + String.format("%.4f", results[0]) + "\t");*/
        Timber.i("Distance: %s", String.format("%.2f", results[0]));

        if (gpsModel != null) {
            Timber.i("isCircleContains: Distance - " + updateMarkerCounter + ": In isCircleContains : " + gpsModel.toString() + "\t" + String.format("%.4f", results[0]) + "\t");
            binding.tvDistance.setText(updateMarkerCounter + ": In isCircleContains : " + gpsModel.toString() + "\t" + String.format("%.4f", results[0]) + "\t");
        }
        if (Double.parseDouble(String.format("%.2f", results[0])) <= r) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.setOnMarkerClickListener(this);

        currentLocation = new LatLng(0.0, 0.0);
        markerOptionsCurrentLocation = new MarkerOptions()
                .position(currentLocation)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .title("Your Location");
        markerCurrentLocation = googleMap.addMarker(markerOptionsCurrentLocation);
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLng(currentLocation));

        onRangeClick(1);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (destCircle != null) {
            destCircle.remove();
        }

        selectedMarker = marker;

        if (selectedMarker.getTitle().equals("Your Location")) {
            return false;
        }

        String imageURL = "";

        try {
            imageURL = markerTransplantationMap.get(marker).getPhoto1();
        } catch (Exception exception) {
            imageURL = markerSurveyMap.get(marker).getPhoto1();
            Timber.e("onMarkerClick: exception - " + exception);
        }

        if (isNetworkAvailable(activity)) {
            if (commonFunction.NullPointerValidator(imageURL)) {
                Glide.with(activity).load(imageURL).into(binding.ivTree1st);
            }
        } else {
            commonFunction.showNoNetwork(activity);
        }

        final Location location = new Location("yourprovidername");
        location.setLatitude(selectedMarker.getPosition().latitude);
        location.setLongitude(selectedMarker.getPosition().latitude);

        float[] results = new float[1];
        Location.distanceBetween(currentLocation.latitude, currentLocation.longitude,
                selectedMarker.getPosition().latitude, selectedMarker.getPosition().latitude,
                results);

        mCurrentLocation = new Location("currentLocation");
        mCurrentLocation.setLatitude(currentLocation.latitude);
        mCurrentLocation.setLongitude(currentLocation.longitude);

        Timber.i("onMarkerClick: Distance1: %s", mCurrentLocation.distanceTo(location));
        Timber.i("onMarkerClick: Distance2: %s", results[0]);

//        Timber.i("onMarkerClick: Distance2: %s", isCircleContains(destCircle, new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude())));

        destCircle = googleMap.addCircle(new CircleOptions()
                .center(selectedMarker.getPosition())
                .radius(Constants.BOUNDARY_RADIUS)
                .strokeColor(Color.BLUE));

        return false;
    }

    @Override
    public void onProjectClick(int position) {
        binding.etProjectName.setText(listProjectsModel.getResult().get(position).getProjectName());
        projectId = listProjectsModel.getResult().get(position).getId();
        hitGetTransplantationListDetails();
//        hitGetSurveyListDetails();
    }

    @Override
    public void onRangeClick(int position) {
        binding.etRange.setText(rangeListModels.get(position).getValue() + " " + rangeListModels.get(position).getUnit());
        double radius = 0;
        if (rangeListModels.get(position).getUnit().equalsIgnoreCase("km")) {
            radius = rangeListModels.get(position).getValue() * 1000;
        } else {
            radius = rangeListModels.get(position).getValue();
        }

        if (circle != null) {
            circle.remove();
        }

        // Drawing circle on the map
        circle = googleMap.addCircle(new CircleOptions()
                .center(currentLocation)
                .radius(radius)
                .strokeColor(Color.RED));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                circle.getCenter(), getZoomLevel(circle)));

        transplantationVisibleCount = 0;
        surveyVisibleCount = 0;

        setTransplantationMarkers();
        setSurveyMarkers();
    }

    public int getZoomLevel(Circle circle) {
        int zoomLevel = 11;
        if (circle != null) {
            double radius = circle.getRadius() + circle.getRadius() / 2;
            double scale = radius / 500;
            zoomLevel = (int) (16 - Math.log(scale) / Math.log(2));
        }
        return zoomLevel;
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();

        Timber.i("onSuccessCompanyDetails: companyDetailsModel: %s", new Gson().toJson(companyDetailsModel));

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }

        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(GPSSelectedBluetooth));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessListProjects(ListPlantationModel listPlantationModel) {
        commonFunction.dismissLoader();
        if (listPlantationModel != null && listPlantationModel.getResult() != null && !listPlantationModel.getResult().isEmpty()) {
            this.listPlantationModel = listPlantationModel;
            addTransplantationMarkers();
        }
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessProjectListDetails(PlantationListModel plantationListModel) {
        commonFunction.dismissLoader();
        Timber.i("onSuccessProjectListDetails: projectListDetailsModel - %s", new Gson().toJson(projectListDetailsModel));
        if (projectListDetailsModel != null) {
            this.projectListDetailsModel = plantationListModel;
            addTransplantationMarkers();
        }
    }



    @Override
    public void onErrorProjectListDetails(String error) {
        commonFunction.dismissLoader();
//        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
        Timber.e("onErrorProjectListDetails: error - %s", error);
        binding.etTransplantationTreeCount.setText("0");

        if (markerTransplantationList != null && !markerTransplantationList.isEmpty()) {
            for (int i = 0; i < markerTransplantationList.size(); i++) {
                markerTransplantationList.get(i).remove();
            }
        }
    }

    @Override
    public void onSuccessSurveyListDetails(SurveyListDetailsModel surveyListDetailsModel) {
        commonFunction.dismissLoader();
        if (surveyListDetailsModel != null) {
            this.surveyListDetailsModel = surveyListDetailsModel;
            addSurveyMarkers();
        }
    }

    @Override
    public void onErrorSurveyListDetails(String error) {
        commonFunction.dismissLoader();
//        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
        Timber.e("onErrorSurveyListDetails: error - %s", error);
        binding.etSurveyTreeCount.setText("0");

        if (markerSurveyList != null && !markerSurveyList.isEmpty()) {
            for (int i = 0; i < markerSurveyList.size(); i++) {
                markerSurveyList.get(i).remove();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case Constants.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        Timber.i("RESULT_OK");
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        Timber.i("RESULT_CANCELED");
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        plantationPresenter.onDispose();

        try {
            if (receiver.isInitialStickyBroadcast()) {
                activity.unregisterReceiver(receiver);
            }
            if (mChatService != null) {
                mChatService.stop();
                Timber.i("onDestroyView: mChatService.stop");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("onDestroyView: %s", e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (receiver.isInitialStickyBroadcast()) {
                activity.unregisterReceiver(receiver);
            }
            if (mChatService != null) {
                mChatService.stop();
                Timber.i("onDestroy: mChatService.stop");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("onDestroy: %s", e.getMessage());
        }
    }
}