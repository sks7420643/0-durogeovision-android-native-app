package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.model.StatusRangeModel;

import java.util.ArrayList;

public class StatusRangeAdapter extends RecyclerView.Adapter<StatusRangeAdapter.ViewHolder>{
    ArrayList<StatusRangeModel> statusRangeModel = new ArrayList<>();
    OnItemClickListener onItemClickListener;

    public StatusRangeAdapter(ArrayList<StatusRangeModel> statusRangeModel, OnItemClickListener onItemClickListener) {
        this.statusRangeModel = statusRangeModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public StatusRangeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StatusRangeAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_status_range, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StatusRangeAdapter.ViewHolder holder, int position) {
        holder.tvItemName.setText(statusRangeModel.get(position).getStatus());
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return statusRangeModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
        }
    }
}
