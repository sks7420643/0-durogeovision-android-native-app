package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.model.CurrencyNameModel;
import com.treeambulance.service.model.ReportTypeModel;

import java.util.ArrayList;

public class CurrencyNameAdapter extends RecyclerView.Adapter<CurrencyNameAdapter.ViewHolder> {

    ArrayList<CurrencyNameModel> currencyNameModel = new ArrayList<>();
    OnItemClickListener onItemClickListener;

    public CurrencyNameAdapter(ArrayList<CurrencyNameModel> currencyNameModel, OnItemClickListener onItemClickListener) {
        this.currencyNameModel = currencyNameModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_currency_name, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvItemName.setText(currencyNameModel.get(position).getCurrencyName());
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return currencyNameModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
        }
    }
}
