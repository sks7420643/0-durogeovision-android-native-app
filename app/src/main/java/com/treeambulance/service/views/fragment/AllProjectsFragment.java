package com.treeambulance.service.views.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentAllProjectsBinding;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.presenter.AllProjectsPresenter;
import com.treeambulance.service.views.adapter.AllProjectAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class AllProjectsFragment extends BaseFragment implements View.OnClickListener,
        AllProjectsPresenter.ContactInterface, AllProjectAdapter.AllProjectClickListener, CustomDialogBottomSheetFragment.CustomDialogOnButtonClick {
    FragmentAllProjectsBinding binding;
    AllProjectsPresenter allProjectsPresenter;
    HashMap<String, String> allProjectsHashMap;
    HashMap<String, String> deleteProjectsHashMap;
    ListProjectsModel listProjectsModel;
    private int position = 0;

    public AllProjectsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_all_projects, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listProjectsModel = new ListProjectsModel();
        allProjectsHashMap = new HashMap<>();
        deleteProjectsHashMap = new HashMap<>();
        allProjectsPresenter = new AllProjectsPresenter(this);

        binding.srlAllProjects.setOnRefreshListener(() -> {
            hitGetAllProjects();
            binding.srlAllProjects.setRefreshing(false);
        });
        hitGetAllProjects();
    }

    private void hitGetAllProjects() {
        allProjectsHashMap.put("action", "list_projects");
        allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            allProjectsPresenter.getListProjects(allProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void deleteProject(int position) {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment(
                "Make sure want to delete " + listProjectsModel.getResult().get(position).getProjectName()
                        + " ?", this);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    private void editProject(int position) {
        moveToFragment(new AddProjectFragment(listProjectsModel.getResult().get(position)), android.R.id.content, false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackAllProject:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
        }
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
            deleteProjectsHashMap.put("action", "inactive_project");
            deleteProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            deleteProjectsHashMap.put("id", listProjectsModel.getResult().get(position).getId());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                allProjectsPresenter.getDeleteProjects(deleteProjectsHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onSuccessListProjects(ListProjectsModel listProjectsModel) {
        commonFunction.dismissLoader();

        if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
            this.listProjectsModel = listProjectsModel;
            binding.rvProjectList.setAdapter(new AllProjectAdapter(listProjectsModel.getResult(), this));
            binding.tvNoData.setVisibility(View.GONE);
            binding.rvProjectList.setVisibility(View.VISIBLE);
        } else {
            binding.tvNoData.setVisibility(View.VISIBLE);
            binding.rvProjectList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessDeleteProjects(CommonModel commonModel) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), commonModel.getMsg());
        hitGetAllProjects();
    }

    @Override
    public void onErrorDeleteProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void allProjectOnClick(int position, String action) {

        this.position = position;

        switch (action) {
            case "edit":
                editProject(this.position);
                break;
            case "delete":
                deleteProject(this.position);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        allProjectsPresenter.onDispose();
    }
}