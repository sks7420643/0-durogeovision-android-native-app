package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentStateListBottomSheetBinding;
import com.treeambulance.service.model.ListPlantationModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.views.adapter.AllPlantationListAdapter;
import com.treeambulance.service.views.adapter.AllProjectListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AllPlantationBottomSheetFragment extends BaseBottomSheet implements AllPlantationListAdapter.OnItemClickListener {

    private FragmentStateListBottomSheetBinding binding;
    private ArrayList<ListPlantationModel.Result> listProjectsResultModel;
    private OnPlantationClickListener onProjectClickListener;

    public AllPlantationBottomSheetFragment() {
        // Required empty public constructor
    }

    public AllPlantationBottomSheetFragment(ArrayList<ListPlantationModel.Result> listProjectsResultModel, OnPlantationClickListener onProjectClickListener) {
        this.onProjectClickListener = onProjectClickListener;
        this.listProjectsResultModel = listProjectsResultModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_state_list_bottom_sheet,
                container,
                false
        );

        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvStateList.setAdapter(new AllPlantationListAdapter(listProjectsResultModel, this));
    }

    @Override
    public void onItemClick(int position) {
        onProjectClickListener.onProjectClick(position);
        dismiss();
    }

    public interface OnPlantationClickListener {
        public void onProjectClick(int position);
    }
}