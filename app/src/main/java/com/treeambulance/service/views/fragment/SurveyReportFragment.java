package com.treeambulance.service.views.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentReportBinding;
import com.treeambulance.service.model.CityModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.DistrictNameModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.OperatorNameModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.model.StateNameModel;
import com.treeambulance.service.model.VillageListModel;
import com.treeambulance.service.model.VillageNameModel;
import com.treeambulance.service.presenter.ReportPresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.CityMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.OperatorMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StateMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.VillageMultiListBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import timber.log.Timber;

public class SurveyReportFragment extends BaseFragment implements View.OnClickListener, ReportPresenter.ContactInterface,
        OperatorMultiListBottomSheetFragment.OperatorListMultiClickListener, StateMultiListBottomSheetFragment.StateListMultiClickListener,
        CityMultiListBottomSheetFragment.CityListMultiClickListener, VillageMultiListBottomSheetFragment.VillageListMultiClickListener {

    private final Calendar myCalendar = Calendar.getInstance();
    private FragmentReportBinding binding;
    private String selected_date = "", str_FromDate = "", str_ToDate = "", projectId = "", operatorId = "";

    private final DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (selected_date.equalsIgnoreCase("from_date")) {
                str_FromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                binding.etFromDate.setText(commonFunction.getReverseDate(str_FromDate));
            } else if (selected_date.equalsIgnoreCase("to_date")) {
                str_ToDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                binding.etToDate.setText(commonFunction.getReverseDate(str_ToDate));
            }
        }
    };

    private ReportPresenter reportPresenter;

    private HashMap<String, String> getOperatorListHashMap;
    private HashMap<String, String> getVillageListHashMap;
    private HashMap<String, String> getSurveyReportHashMap;

    private ArrayList<OperatorListModel.Result> operatorListResultModel;
    private ArrayList<OperatorListModel.Result> operatorSelectedListResultModel;
    private ArrayList<StateCityModel> stateCityModel;
    private ArrayList<StateCityModel> stateCitySelectedModel;
    private ArrayList<CityModel> cityModel;
    private ArrayList<CityModel> citySelectedModel;
    private ArrayList<VillageListModel.Result> villageListResultModel;
    private ArrayList<VillageListModel.Result> villageListResultSelectModel;

    public SurveyReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.llOperatorName.setVisibility(View.VISIBLE);
        binding.llStatus.setVisibility(View.GONE);

        villageListResultModel = new ArrayList<>();
        stateCityModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        operatorListResultModel = new ArrayList<>();

        reportPresenter = new ReportPresenter(this);
        getOperatorListHashMap = new HashMap<>();
        getVillageListHashMap = new HashMap<>();
        getSurveyReportHashMap = new HashMap<>();

        binding.tvTitle.setText("Survey Report");

        loadJSONFromAsset();
        hitGetOperatorList();
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/states_and_districts.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray));

        String str_response = new Gson().toJson(jsonArray);

        try {
            Type type = new TypeToken<ArrayList<StateCityModel>>() {
            }.getType();

            stateCityModel = new Gson().fromJson(str_response, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(stateCityModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }
    }

    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            reportPresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetVillageList() {

        getVillageListHashMap.put("action", "village_list");
        getVillageListHashMap.put("type", "survey");
        getVillageListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
        getVillageListHashMap.put("district", new Gson().toJson(citySelectedModel));

        Timber.i("hitGetVillageList: getVillageListHashMap - %s", getVillageListHashMap);

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            reportPresenter.getVillageList(getVillageListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackSurveyTransplantationConsolidatedReport: {
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.etFromDate: {
                hitDatePickerDialog("from_date");
            }
            break;
            case R.id.etToDate:
                hitDatePickerDialog("to_date");
                break;
            case R.id.etOperatorName: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (operatorListResultModel != null && !operatorListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new OperatorMultiListBottomSheetFragment(operatorListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "operator_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Operator List is Empty " + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.etState: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateMultiListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.etDistrict: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityMultiListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "City List is Empty " + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.etVillage: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (villageListResultModel != null && !villageListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new VillageMultiListBottomSheetFragment(villageListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "village_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Village List is Empty " + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.btSubmit: {
//                onSuccessSurveyReport(null);
//                webView("http://192.168.0.250/tree_ambulance/app_api/example.php?fr_date=2021-3-31&to_date=2021-4-1&company_id=15&village_list=[{\\\"village\\\":\\\"Madakulam\\\"},{\\\"village\\\":\\\"Maadakulam\\\"}]&operator_name=[{\\\"name\\\":\\\"Saravana\\\"}]");
                validator();
            }
            break;
        }
    }

    private void webView(String webUrl) {
        moveToFragment(new WebViewFragment(webUrl), android.R.id.content, false);
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etFromDate.getText().toString().trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select From Date");
        } else if (!commonFunction.NullPointerValidator(binding.etToDate.getText().toString().trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select To Date");
        } else if (operatorSelectedListResultModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one Operator");
        } else if (stateCitySelectedModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one State");
        } else if (citySelectedModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one District");
        } else if (villageListResultSelectModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one Village");
        } else {

            ArrayList<OperatorNameModel> operatorName = new ArrayList<>();
            for (OperatorListModel.Result list : operatorSelectedListResultModel) {
                operatorName.add(new OperatorNameModel(list.getName()));
            }

            ArrayList<StateNameModel> stateName = new ArrayList<>();
            for (StateCityModel list : stateCitySelectedModel) {
                stateName.add(new StateNameModel(list.getState()));
            }

            ArrayList<DistrictNameModel> districtName = new ArrayList<>();
            for (CityModel list : citySelectedModel) {
                districtName.add(new DistrictNameModel(list.getDistrict()));
            }

            ArrayList<VillageNameModel> villageName = new ArrayList<>();
            for (VillageListModel.Result list : villageListResultSelectModel) {
                villageName.add(new VillageNameModel(list.getVillage()));
            }

            getSurveyReportHashMap.put("action", "get_survey_report_excel");
            getSurveyReportHashMap.put("from_date", str_FromDate);
            getSurveyReportHashMap.put("to_date", str_ToDate);
            getSurveyReportHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            getSurveyReportHashMap.put("operator_name", new Gson().toJson(operatorName));
            getSurveyReportHashMap.put("state_list", new Gson().toJson(stateName));
            getSurveyReportHashMap.put("district_list", new Gson().toJson(districtName));
            getSurveyReportHashMap.put("village_list", new Gson().toJson(villageName));

            Timber.i("validator: %s", getSurveyReportHashMap);
            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                reportPresenter.getSurveyReport(getSurveyReportHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }

        }
    }

    private void hitDatePickerDialog(String date) {
        selected_date = date;
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, /*R.style.datepicker,*/ onDateSetListener,
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListResultModel = operatorListModel.getResult();
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessVillageList(VillageListModel villageListModel) {
        commonFunction.dismissLoader();
        if (villageListModel != null && villageListModel.getResult() != null && !villageListModel.getResult().isEmpty()) {
            this.villageListResultModel = villageListModel.getResult();
        }
    }

    @Override
    public void onErrorVillageList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessSurveyReport(CommonModel commonModel) {

        commonFunction.dismissLoader();

        Timber.i("onSuccessSurveyReport: commonModel - %s", new Gson().toJson(commonModel));

        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setData(Uri.parse(commonModel.getResult()));
        startActivity(browserIntent);

        /*new Handler(Looper.getMainLooper()).postDelayed(() -> {
            FileLoader.with(context)
                    .load(commonModel.getResult(), false)
//                .load("https://archaryaiasacademy.in/tree_ambulance/app_api/survey_report/Surveyreport.pdf", false)
                    .fromDirectory("Download", FileLoader.DIR_EXTERNAL_PUBLIC)
                    .asFile(new FileRequestListener<File>() {
                        @Override
                        public void onLoad(FileLoadRequest request, FileResponse<File> response) {
                            commonFunction.dismissLoader();

                            Timber.i("onSuccessSurveyReport: onLoad: %s", new Gson().toJson(response.getBody()));

                            String loadedFile = response.getBody().getPath();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                File file = new File(loadedFile);
                                Uri uri = FileProvider.getUriForFile(context, activity.getPackageName() + ".provider", file);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(uri, "application/vnd.ms-excel");
//                            intent.setData(uri);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
//                            intent.setDataAndType(Uri.parse(loadedFile), "application/pdf");
                                intent.setDataAndType(Uri.parse(loadedFile), "application/vnd.ms-excel");
                                intent = Intent.createChooser(intent, "Open File");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }

                            commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), "Downloaded Successfully");
                            Timber.i("onLoad: SuccessDownload: %s", loadedFile);
                        }

                        @Override
                        public void onError(FileLoadRequest request, Throwable t) {
                            commonFunction.dismissLoader();
                            commonFunction.FailureAlert(activity, activity.getResources().getString(R.string.failed_title), "File download failed");
                            Timber.e("onLoad: errorDownload: %s", t.getMessage());
                        }
                    });
        }, commonFunction.DOWNLOAD_DELAY_TIME);*/
    }

    @Override
    public void onErrorSurveyReport(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessTransplantationReport(CommonModel commonModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorTransplantationReport(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessMaintenanceReport(CommonModel commonModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorMaintenanceReport(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessConsolidatedReport(CommonModel commonModel) {
        commonFunction.dismissLoader();

    }

    @Override
    public void onErrorConsolidatedReport(String error) {
        commonFunction.dismissLoader();

    }

    @Override
    public void onOperatorMultiClick(ArrayList<OperatorListModel.Result> operatorListResultsModel) {
        operatorListResultModel = new ArrayList<>();
        operatorSelectedListResultModel = new ArrayList<>();
        operatorListResultModel = operatorListResultsModel;
        StringBuilder operatorsName = new StringBuilder();

        for (OperatorListModel.Result list : operatorListResultModel) {
            if (list.isChecked()) {
                operatorSelectedListResultModel.add(list);
            }
        }

        for (int i = 0; i < operatorListResultsModel.size(); i++) {
            if (operatorListResultsModel.get(i).isChecked()) {
                if (operatorsName.toString().trim().equals("")) {
                    operatorsName.append(operatorListResultsModel.get(i).getName());
                } else {
                    operatorsName.append(", ").append(operatorListResultsModel.get(i).getName());
                }
            }
        }

        Timber.i("onOperatorMultiClick: %s", new Gson().toJson(operatorSelectedListResultModel));
        binding.etOperatorName.setText(operatorsName.toString());
    }

    @Override
    public void onStateMultiClick(ArrayList<StateCityModel> stateListResultsModel) {
        cityModel = new ArrayList<>();
        citySelectedModel = new ArrayList<>();
        villageListResultModel = new ArrayList<>();
        villageListResultSelectModel = new ArrayList<>();
        stateCityModel = new ArrayList<>();
        stateCitySelectedModel = new ArrayList<>();
        stateCityModel = stateListResultsModel;
        StringBuilder statesName = new StringBuilder();

        for (StateCityModel list : stateCityModel) {
            if (list.isChecked()) {
                stateCitySelectedModel.add(list);
                for (String listDistrict : list.getDistricts()) {
                    cityModel.add(new CityModel(listDistrict, false));
                }
            }
        }

        for (int i = 0; i < stateListResultsModel.size(); i++) {
            if (stateListResultsModel.get(i).isChecked()) {
                if (statesName.toString().trim().equals("")) {
                    statesName.append(stateListResultsModel.get(i).getState());
                } else {
                    statesName.append(", ").append(stateListResultsModel.get(i).getState());
                }
            }
        }

        Timber.i("onStateMultiClick: %s", new Gson().toJson(stateCitySelectedModel));
        binding.etState.setText(statesName.toString());
        binding.etDistrict.setText("");
        binding.etVillage.setText("");
    }

    @Override
    public void onCityMultiClick(ArrayList<CityModel> cityListResultsModel) {
        cityModel = new ArrayList<>();
        citySelectedModel = new ArrayList<>();
        villageListResultModel = new ArrayList<>();
        villageListResultSelectModel = new ArrayList<>();
        cityModel = cityListResultsModel;
        StringBuilder districtName = new StringBuilder();

        for (CityModel list : cityModel) {
            if (list.isChecked()) {
                citySelectedModel.add(list);
            }
        }

        for (int i = 0; i < cityListResultsModel.size(); i++) {
            if (cityListResultsModel.get(i).isChecked()) {
                if (districtName.toString().trim().equals("")) {
                    districtName.append(cityListResultsModel.get(i).getDistrict());
                } else {
                    districtName.append(", ").append(cityListResultsModel.get(i).getDistrict());
                }
            }
        }

        Timber.i("onCityMultiClick: %s", new Gson().toJson(citySelectedModel));
        binding.etDistrict.setText(districtName.toString());
        binding.etVillage.setText("");
        hitGetVillageList();
    }

    @Override
    public void onVillageMultiClick(ArrayList<VillageListModel.Result> villageListResultsModel) {
        villageListResultModel = new ArrayList<>();
        villageListResultSelectModel = new ArrayList<>();
        villageListResultModel = villageListResultsModel;
        StringBuilder villageName = new StringBuilder();

        for (VillageListModel.Result list : villageListResultModel) {
            if (list.isChecked()) {
                villageListResultSelectModel.add(list);
            }
        }

        for (int i = 0; i < villageListResultsModel.size(); i++) {
            if (villageListResultsModel.get(i).isChecked()) {
                if (villageName.toString().trim().equals("")) {
                    villageName.append(villageListResultsModel.get(i).getVillage());
                } else {
                    villageName.append(", ").append(villageListResultsModel.get(i).getVillage());
                }
            }
        }

        Timber.i("onVillageMultiClick: %s", new Gson().toJson(villageListResultSelectModel));
        binding.etVillage.setText(villageName.toString());
    }
}