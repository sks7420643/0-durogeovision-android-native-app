package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentStateListBottomSheetBinding;
import com.treeambulance.service.views.adapter.CityListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CityListBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentStateListBottomSheetBinding binding;
    private ArrayList<String> cityModel = new ArrayList<>();
    private OnCityClickListener onCityClickListener;

    public CityListBottomSheetFragment(ArrayList<String> cityModel, OnCityClickListener onCityClickListener) {
        // Required empty public constructor
        this.cityModel = cityModel;
        this.onCityClickListener = onCityClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_state_list_bottom_sheet,
                container,
                false
        );
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvStateList.setAdapter(new CityListAdapter(cityModel, this));
    }

    @Override
    public void onItemClick(int position) {
        onCityClickListener.onCityClick(position);
        dismiss();
    }

    public interface OnCityClickListener {
        public void onCityClick(int position);
    }
}