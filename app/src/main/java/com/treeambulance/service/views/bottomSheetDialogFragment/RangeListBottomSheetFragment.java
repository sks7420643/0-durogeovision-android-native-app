package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentRangeListBottomSheetBinding;
import com.treeambulance.service.model.RangeListModel;
import com.treeambulance.service.views.adapter.RangeListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class RangeListBottomSheetFragment extends BaseBottomSheet implements RangeListAdapter.OnItemClickListener {

    private FragmentRangeListBottomSheetBinding binding;
    private ArrayList<RangeListModel> rangeListModels;
    private OnRangeClickListener onRangeClickListener;

    public RangeListBottomSheetFragment() {
        // Required empty public constructor
    }

    public RangeListBottomSheetFragment(ArrayList<RangeListModel> rangeListModels, OnRangeClickListener onRangeClickListener) {
        this.onRangeClickListener = onRangeClickListener;
        this.rangeListModels = rangeListModels;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_range_list_bottom_sheet,
                container,
                false
        );

        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvRangeList.setAdapter(new RangeListAdapter(rangeListModels, this));
    }

    @Override
    public void onItemClick(int position) {
        onRangeClickListener.onRangeClick(position);
        dismiss();
    }

    public interface OnRangeClickListener {
        public void onRangeClick(int position);
    }
}