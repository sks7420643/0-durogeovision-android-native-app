package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentOperatorListBottomSheetBinding;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.views.adapter.OperatorsListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class OperatorListBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentOperatorListBottomSheetBinding binding;
    private ArrayList<OperatorListModel.Result> operatorListResultsModel;
    private OperatorListClickListener operatorListClickListener;

    public OperatorListBottomSheetFragment() {
        // Required empty public constructor
    }

    public OperatorListBottomSheetFragment(ArrayList<OperatorListModel.Result> operatorListResultsModel, OperatorListClickListener operatorListClickListener) {
        this.operatorListClickListener = operatorListClickListener;
        this.operatorListResultsModel = operatorListResultsModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_operator_list_bottom_sheet,
                container,
                false
        );

        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvList.setAdapter(new OperatorsListAdapter(operatorListResultsModel, this));
    }

    @Override
    public void onItemClick(int position) {
        operatorListClickListener.onOperatorClick(position);
        dismiss();
    }

    public interface OperatorListClickListener {
        public void onOperatorClick(int position);
    }
}