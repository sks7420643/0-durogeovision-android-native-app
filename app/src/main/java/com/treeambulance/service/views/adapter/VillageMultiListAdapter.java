package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterMultiListBinding;
import com.treeambulance.service.model.CityModel;
import com.treeambulance.service.model.VillageListModel;

import java.util.ArrayList;

public class VillageMultiListAdapter extends RecyclerView.Adapter<VillageMultiListAdapter.ViewHolder> {

    private ArrayList<VillageListModel.Result> villageListModels;
    private OnVillageMultiListSelected onVillageMultiListSelected;

    public VillageMultiListAdapter(ArrayList<VillageListModel.Result> villageListModels, OnVillageMultiListSelected onVillageMultiListSelected) {
        this.villageListModels = villageListModels;
        this.onVillageMultiListSelected = onVillageMultiListSelected;
    }

    @NonNull
    @Override
    public VillageMultiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_multi_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VillageMultiListAdapter.ViewHolder holder, int position) {
        holder.binding.tvItemName.setText(villageListModels.get(position).getVillage());
        holder.binding.cbItem.setChecked(villageListModels.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (villageListModels.get(position).isChecked()) {
                onVillageMultiListSelected.onVillageSelected(position, false);
            } else if (!villageListModels.get(position).isChecked()) {
                onVillageMultiListSelected.onVillageSelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return villageListModels.size();
    }

    public interface OnVillageMultiListSelected {
        public void onVillageSelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterMultiListBinding binding;

        public ViewHolder(@NonNull AdapterMultiListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
