package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentOperatorMultiListBottomSheetBinding;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.views.adapter.OperatorMultiListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class OperatorMultiListBottomSheetFragment extends BaseBottomSheet implements OperatorMultiListAdapter.OnOperatorMultiListSelected, View.OnClickListener {
    private FragmentOperatorMultiListBottomSheetBinding binding;
    private ArrayList<OperatorListModel.Result> operatorListResultsModel;
    private OperatorListMultiClickListener operatorListMultiClickListener;
    private OperatorMultiListAdapter operatorMultiListAdapter;

    public OperatorMultiListBottomSheetFragment() {
        // Required empty public constructor
    }

    public OperatorMultiListBottomSheetFragment(ArrayList<OperatorListModel.Result> operatorListResultsModel, OperatorListMultiClickListener operatorListMultiClickListener) {
        this.operatorListMultiClickListener = operatorListMultiClickListener;
        this.operatorListResultsModel = operatorListResultsModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_operator_multi_list_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        operatorMultiListAdapter = new OperatorMultiListAdapter(operatorListResultsModel, this);
        binding.rvOperatorsList.setAdapter(operatorMultiListAdapter);
    }

    @Override
    public void onOperatorSelected(int position, boolean isChecked) {
        operatorListResultsModel.get(position).setChecked(isChecked);
        operatorMultiListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                operatorListMultiClickListener.onOperatorMultiClick(operatorListResultsModel);
                dismiss();
            }
            break;
            case R.id.btSelectAll: {

                for (int i = 0; i < operatorListResultsModel.size(); i++) {
                    operatorListResultsModel.get(i).setChecked(true);
                }

                operatorListMultiClickListener.onOperatorMultiClick(operatorListResultsModel);
                dismiss();
            }
            break;
            case R.id.btDeselectAll: {

                for (int i = 0; i < operatorListResultsModel.size(); i++) {
                    operatorListResultsModel.get(i).setChecked(false);
                }

                operatorListMultiClickListener.onOperatorMultiClick(operatorListResultsModel);
                dismiss();
            }
            break;
        }
    }

    public interface OperatorListMultiClickListener {
        void onOperatorMultiClick(ArrayList<OperatorListModel.Result> operatorListResultsModel);
    }
}