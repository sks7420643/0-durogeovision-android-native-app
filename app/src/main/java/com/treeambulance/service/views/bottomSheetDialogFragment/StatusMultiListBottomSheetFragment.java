package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentStatusMultiListBottomSheetBinding;
import com.treeambulance.service.model.StatusListModel;
import com.treeambulance.service.views.adapter.StatusMultiListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class StatusMultiListBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener, StatusMultiListAdapter.OnStatusMultiListSelected {
    private FragmentStatusMultiListBottomSheetBinding binding;
    private ArrayList<StatusListModel> statusListResultsModel;
    private StatusListMultiClickListener statusListMultiClickListener;
    private StatusMultiListAdapter statusMultiListAdapter;

    public StatusMultiListBottomSheetFragment() {
        // Required empty public constructor
    }

    public StatusMultiListBottomSheetFragment(ArrayList<StatusListModel> statusListResultsModel, StatusListMultiClickListener statusListMultiClickListener) {
        this.statusListMultiClickListener = statusListMultiClickListener;
        this.statusListResultsModel = statusListResultsModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_status_multi_list_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        statusMultiListAdapter = new StatusMultiListAdapter(statusListResultsModel, this);
        binding.rvStatusList.setAdapter(statusMultiListAdapter);
    }

    @Override
    public void onStatusSelected(int position, boolean isChecked) {
        statusListResultsModel.get(position).setChecked(isChecked);
        statusMultiListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                statusListMultiClickListener.onStatusMultiClick(statusListResultsModel);
                dismiss();
            }
            break;
            case R.id.btSelectAll: {

                for (int i = 0; i < statusListResultsModel.size(); i++) {
                    statusListResultsModel.get(i).setChecked(true);
                }

                statusListMultiClickListener.onStatusMultiClick(statusListResultsModel);
                dismiss();
            }
            break;
            case R.id.btDeselectAll: {

                for (int i = 0; i < statusListResultsModel.size(); i++) {
                    statusListResultsModel.get(i).setChecked(false);
                }

                statusListMultiClickListener.onStatusMultiClick(statusListResultsModel);
                dismiss();
            }
            break;
        }
    }

    public interface StatusListMultiClickListener {
        void onStatusMultiClick(ArrayList<StatusListModel> statusListModel);
    }
}