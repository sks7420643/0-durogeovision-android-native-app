package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentVillageMultiListBottomSheetBinding;
import com.treeambulance.service.model.VillageListModel;
import com.treeambulance.service.views.adapter.VillageMultiListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class VillageMultiListBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener, VillageMultiListAdapter.OnVillageMultiListSelected {
    private FragmentVillageMultiListBottomSheetBinding binding;
    private ArrayList<VillageListModel.Result> villageListResultsModel;
    private VillageListMultiClickListener villageListMultiClickListener;
    private VillageMultiListAdapter villageMultiListAdapter;

    public VillageMultiListBottomSheetFragment() {
        // Required empty public constructor
    }

    public VillageMultiListBottomSheetFragment(ArrayList<VillageListModel.Result> villageListResultsModel, VillageListMultiClickListener villageListMultiClickListener) {
        this.villageListResultsModel = villageListResultsModel;
        this.villageListMultiClickListener = villageListMultiClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_village_multi_list_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        villageMultiListAdapter = new VillageMultiListAdapter(villageListResultsModel, this);
        binding.rvVillageList.setAdapter(villageMultiListAdapter);
    }

    @Override
    public void onVillageSelected(int position, boolean isChecked) {
        villageListResultsModel.get(position).setChecked(isChecked);
        villageMultiListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                villageListMultiClickListener.onVillageMultiClick(villageListResultsModel);
                dismiss();
            }
            break;
            case R.id.btSelectAll: {

                for (int i = 0; i < villageListResultsModel.size(); i++) {
                    villageListResultsModel.get(i).setChecked(true);
                }

                villageListMultiClickListener.onVillageMultiClick(villageListResultsModel);
                dismiss();
            }
            break;
            case R.id.btDeselectAll: {

                for (int i = 0; i < villageListResultsModel.size(); i++) {
                    villageListResultsModel.get(i).setChecked(false);
                }

                villageListMultiClickListener.onVillageMultiClick(villageListResultsModel);
                dismiss();
            }
            break;
        }
    }

    public interface VillageListMultiClickListener {
        void onVillageMultiClick(ArrayList<VillageListModel.Result> villageListResultsModel);
    }
}