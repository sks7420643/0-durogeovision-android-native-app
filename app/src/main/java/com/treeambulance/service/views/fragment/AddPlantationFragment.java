package com.treeambulance.service.views.fragment;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.location.Address;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.bluetoothReader.BluetoothChatServiceForContinuosData;
import com.treeambulance.service.databinding.FragmentAddPlantationBinding;
import com.treeambulance.service.model.AddPlantationProjectModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.PlantationListModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.other.AppLocationService;
import com.treeambulance.service.presenter.AddPlantationPresenter;
import com.treeambulance.service.utility.Constants;
import com.treeambulance.service.views.adapter.PlantationProjectListAdapter;
import com.treeambulance.service.views.adapter.TreeManagementListAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.CityListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CountryListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StateListBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

public class AddPlantationFragment extends BaseFragment implements View.OnClickListener, CountryListBottomSheetFragment.CountryListClickListener,
        StateListBottomSheetFragment.OnStateClickListener, CityListBottomSheetFragment.OnCityClickListener, AddPlantationPresenter.ContactInterface, PlantationProjectListAdapter.PlantationClickListener, CustomDialogBottomSheetFragment.CustomDialogOnButtonClick {

    private FragmentAddPlantationBinding binding;

    private ArrayList<StateCityModel> stateCityModel;
    private CompanyDetailsModel companyDetailsModel;
    private ArrayList<String> cityModel,GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private AddPlantationPresenter addPlantationPresenter;
    private HashMap<String, String> addProjectsHashMap,getCompanyDetailsHashMap,getCountryListHashMap;
    private ArrayList<CountryListModel.Result> countryListResultModel;
    private PlantationListModel plantationListModel;
    private String action = "add_project";
    private HashMap<String, String> getPlantationListHashMap;
    private LocationManager locationManager;
    private Context mContext;
    AppLocationService appLocationService;
    private String state,district,village,country;
    private String mConnectedDeviceName = "",str_CountryType ="";
    private HashMap<String, String> getDeleteTreeManagementHashMap;
    private BluetoothChatServiceForContinuosData mChatService = null;
    //    private ArrayAdapter<String> mConversationArrayAdapter;
    private int counter = 0;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean secure;
    private Set<BluetoothDevice> setBluetoothDevice;
    private double latd,lngd;
    private int position = 0;
    private boolean isEdit = false;
    public AddPlantationFragment() {
        action = "add_project";
    }

    public AddPlantationFragment(PlantationListModel result) {
        this.plantationListModel = result;
        action = "edit_project";
    }

    private final Handler mHandler2 = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
//            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
//                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTING:
//                            setStatus(String.valueOf(R.string.title_connecting));
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_LISTEN:
                        case BluetoothChatServiceForContinuosData.STATE_NONE:
//                            setStatus(String.valueOf(R.string.title_not_connected));
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
//                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_LONG).show();
                    if (mBluetoothAdapter.isEnabled()) {
                        sendToProcessMessage(readMessage);
                    }else{
                        commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Bluetooth Connection Failed");
                    }

//                    mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    commonFunction.showShortToast("Connected to " + mConnectedDeviceName);
                    break;
                case Constants.MESSAGE_TOAST:
                    if (msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Unable to connect device") || msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Device connection was lost")) {
                        counter++;
                        Timber.i("");
                        if (counter < GPSDeviceList.size()) {
                            attempt();
                        }
                    }
                    commonFunction.showShortToast("GPS Reading is in Progress... Attempt: " + counter);
                    if (counter > GPSDeviceList.size()) {

                        commonFunction.showShortToast(msg.getData().getString(Constants.TOAST));
                        commonFunction.showShortToast("Bluetooth Connection Failed");
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_plantation, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stateCityModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        GPSDeviceList = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();
        countryListResultModel = new ArrayList<>();
        getCountryListHashMap = new HashMap<>();
        addPlantationPresenter = new AddPlantationPresenter(this);
        addProjectsHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        getPlantationListHashMap = new HashMap<>();
        mChatService = new BluetoothChatServiceForContinuosData(context, mHandler2);
        plantationListModel = new PlantationListModel();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        getDeleteTreeManagementHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        // Attempt to connect to the device
        secure = false;

        loadJSONFromAsset();



        if (isEdit) {
            binding.tvTitle.setText("Edit Project");
            binding.btAddProject.setText("Update Project");
            binding.etProject.setText(plantationListModel.getResult().get(position).getPlantationName());
            binding.etDistrict.setText(plantationListModel.getResult().get(position).getDistrict());
            binding.etVillage.setText(plantationListModel.getResult().get(position).getVillage());
            binding.etState.setText(plantationListModel.getResult().get(position).getState());
            binding.etCountryName.setText(plantationListModel.getResult().get(position).getCountry());
            binding.etLocationName.setText(plantationListModel.getResult().get(position).getLocation());

            for (StateCityModel stateCityDetail : stateCityModel) {
                if (stateCityDetail.getState().equalsIgnoreCase(plantationListModel.getResult().get(position).getState())) {
                    cityModel.addAll(stateCityDetail.getDistricts());
                }
            }
        }

        mContext = getActivity();

        addBluetoothItemsToList();
        hitGetCompanyDetails();
        hitGetCountryList();
        hitGetPlantationProjectList();
    }

    private void hitGetCountryList(){
        getCountryListHashMap.put("action", "country_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addPlantationPresenter.getCountryList(getCountryListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetPlantationProjectList() {

        getPlantationListHashMap.put("action", "list_plantation");
        getPlantationListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addPlantationPresenter.getPlantationProject(getPlantationListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        for (BluetoothDevice bluetoothDevice : list) {

            GPSFromBluetooth.add(bluetoothDevice.getAddress());
        }

        Timber.i("addBluetoothItemsToList: GPSFromBluetooth: %s", new Gson().toJson(GPSFromBluetooth));
    }

    private void attempt() {
        if (GPSDeviceList != null && !GPSDeviceList.isEmpty()) {
            Timber.i("attempt: counter " + counter + " - " + GPSDeviceList.get(counter));
            BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(GPSDeviceList.get(counter));
            mChatService.connect(device1, secure);
        }
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addPlantationPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void sendToProcessMessage(String readMessage) {
        String[] lineSeparatedArray = readMessage.split("\n");
        if (lineSeparatedArray.length > 1) {
            //Check if we have lan lat in our line
            for (int i = 0; i < lineSeparatedArray.length; i++) {
                String[] splitByGNRMC = lineSeparatedArray[i].split("GNRMC");
                if (splitByGNRMC.length > 1) {
                    try {
                        Timber.i("splitByGNRMC: " + splitByGNRMC[1].split(",")[3] + "\t" + splitByGNRMC[1].split(",")[5]);
                        double lat = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[3]));
                        double lon = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[5]));
                        Timber.i("lon: " + lon + "\t lat: " + lat);
                        latd = lat;
                        lngd = lon;
                        if (latd != 0.0 && lngd != 0.0) {
                            double latitude = latd;
                            double longitude = lngd;

                            List<Address> addresses = commonFunction.getAddress((Activity) mContext,latitude,longitude);
                            Address address = addresses.get(0);
                            country = address.getCountryName();
                            district = address.getLocality();
                            state = address.getAdminArea();

                            StringBuilder sb = new StringBuilder();
                            if (address.getSubThoroughfare() != null){
                                sb.append(address.getSubThoroughfare()).append(",");
                            }
                            if (address.getThoroughfare() != null){
                                sb.append(address.getThoroughfare()).append(",");
                            }
                            if (address.getSubLocality() != null){
                                sb.append(address.getSubLocality());
                            }

                            binding.etCountryName.setText(country);
                            village = sb.toString();
                            binding.etVillage.setText(village);

                            if (stateCityModel != null && !stateCityModel.isEmpty()) {
                                cityModel = new ArrayList<>();
                                for (int j = 0; j < stateCityModel.size(); j++) {
                                    cityModel.addAll(stateCityModel.get(j).getDistricts());
                                    if(stateCityModel.get(j).getState().toLowerCase().equals(state.toLowerCase())){
                                        binding.etState.setText(state);
                                        break;
                                    }
                                }
                            }
                            if (cityModel != null && !cityModel.isEmpty()) {
                                for (int j = 0; j < cityModel.size(); j++) {
                                    if(cityModel.get(j).toLowerCase().equals(district.toLowerCase())){
                                        binding.etDistrict.setText(district);
                                        break;
                                    }
                                }
                            }
                            if (mBluetoothAdapter.isEnabled()) {
                                mChatService.stop();
                            }
//
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Timber.e("sendToProcessMessage: splitData - %s", e.getMessage());
                    }
                }
                Timber.i("sendToProcessMessage: lineSeparatedArray - " + lineSeparatedArray[i] + "\t" + i);
            }
        }
    }

    private double decimalToDMS(double value) {
        Timber.i("decimalToDMS: value %s", value);
        int degrees = (int) (value / 100);
        Timber.i("decimalToDMS: degrees %s", degrees);
        double minutes = (value - degrees * 100) / 60;
        Timber.i("decimalToDMS: minutes %s", minutes);
        return degrees + minutes;
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/states_and_districts.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray));

        String str_response = new Gson().toJson(jsonArray);

        try {
            Type type = new TypeToken<ArrayList<StateCityModel>>() {
            }.getType();

            stateCityModel = new Gson().fromJson(str_response, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(stateCityModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackAddProject:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
//                fragmentManager.beginTransaction().remove(this).commit();
                break;
            case R.id.etState:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
//                    commonFunction.showLongToast("State List is Empty " + getResources().getString(R.string.something_wrong));

                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etCountryName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (countryListResultModel != null && !countryListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new CountryListBottomSheetFragment(countryListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "country_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etDistrict:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }
                break;
           case R.id.btAddProject:
                validation();
                break;
        }
    }

    private void validation() {

        if (!commonFunction.NullPointerValidator(binding.etProject.getText().toString().trim())) {
            binding.etProject.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Plantation Name");
        }  else if (!commonFunction.NullPointerValidator(binding.etCountryName.getText().toString().trim())) {
            binding.etCountryName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Country");
        } else if (!commonFunction.NullPointerValidator(binding.etState.getText().toString().trim())) {
            binding.etState.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select State");
        } else if (!commonFunction.NullPointerValidator(binding.etDistrict.getText().toString().trim())) {
            binding.etDistrict.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select District");
        } else if (!commonFunction.NullPointerValidator(binding.etVillage.getText().toString().trim())) {
            binding.etVillage.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Village Name");
        } else if (!commonFunction.NullPointerValidator(binding.etLocationName.getText().toString().trim())) {
            binding.etLocationName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Location Name");
        } else {

            if (isEdit) {
                addProjectsHashMap.put("id", plantationListModel.getResult().get(position).getId());
            } else {
                addProjectsHashMap.remove("id");
            }

            addProjectsHashMap.put("action", "plantation_add");
            addProjectsHashMap.put("country", binding.etCountryName.getText().toString().trim());
            addProjectsHashMap.put("state", binding.etState.getText().toString().trim());
            addProjectsHashMap.put("district", binding.etDistrict.getText().toString().trim());
            addProjectsHashMap.put("village", binding.etVillage.getText().toString().trim());
            addProjectsHashMap.put("location", binding.etLocationName.getText().toString().trim());
            addProjectsHashMap.put("plantation_name", binding.etProject.getText().toString().trim());
            addProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                addPlantationPresenter.getAddEditPlantation(addProjectsHashMap);
                isEdit = false;
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onStateClick(int position) {
        cityModel = new ArrayList<>();
        binding.etState.setText(stateCityModel.get(position).getState());

        cityModel.addAll(stateCityModel.get(position).getDistricts());
        Timber.i("onStateClick %s", new Gson().toJson(cityModel));
    }

    @Override
    public void onCityClick(int position) {
        binding.etDistrict.setText(cityModel.get(position));
        Timber.i("onCityClick %s", new Gson().toJson(cityModel.get(position)));
    }

    @Override
    public void onSuccessAddPlantationProjects(AddPlantationProjectModel addProjectsModel) {
        binding.etCountryName.setText("");
        binding.etLocationName.setText("");
        binding.etState.setText("");
        binding.etDistrict.setText("");
        binding.etVillage.setText("");
        binding.etProject.setText("");
        commonFunction.dismissLoader();

        if (isEdit) {
            commonFunction.SuccessAlert(activity, "Success...!", "Project Updated Successfully"/*"addProjectsModel.getMsg()"*/);
            hitGetPlantationProjectList();
        } else {
            commonFunction.SuccessAlert(activity, "Success...!", "Project Added Successfully"/*"addProjectsModel.getMsg()"*/);
            hitGetPlantationProjectList();
        }

    }

    @Override
    public void onErrorAddPlantationProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessPlantationProjects(PlantationListModel plantationListModel) {
        commonFunction.dismissLoader();
        if (plantationListModel != null && plantationListModel.getResult() != null && !plantationListModel.getResult().isEmpty()) {
            this.plantationListModel = plantationListModel;
            binding.rvPlantationList.setAdapter(new PlantationProjectListAdapter(plantationListModel.getResult(), this));
            binding.tvNoData.setVisibility(View.GONE);
            binding.rvPlantationList.setVisibility(View.VISIBLE);
        } else {
            binding.tvNoData.setVisibility(View.VISIBLE);
            binding.rvPlantationList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorPlantationProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        Timber.i("onSuccessCompanyDetails: companyDetailsModel - %s", new Gson().toJson(companyDetailsModel));
        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }
        if (GPSSelectedBluetooth != null && !GPSSelectedBluetooth.isEmpty()) {
            attempt();
        }
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
        if (countryListModel != null && countryListModel.getResult() != null && !countryListModel.getResult().isEmpty()) {
            this.countryListResultModel = countryListModel.getResult();;
        }
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessDeleteProjects(CommonModel commonModel) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), commonModel.getMsg());
        hitGetPlantationProjectList();
    }

    @Override
    public void onErrorDeleteProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        addPlantationPresenter.onDispose();
    }

    @Override
    public void onCountryClick(int position) {
        binding.etCountryName.setText(countryListResultModel.get(position).getName());
        str_CountryType = countryListResultModel.get(position).getName();
    }

    private void deleteProject(int position) {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment(
                "Make sure want to delete " + plantationListModel.getResult().get(position).getPlantationName()
                        + " ?", this);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    private void editProject(int position) {
        binding.etProject.setText(plantationListModel.getResult().get(position).getPlantationName());
        binding.etDistrict.setText(plantationListModel.getResult().get(position).getDistrict());
        binding.etVillage.setText(plantationListModel.getResult().get(position).getVillage());
        binding.etState.setText(plantationListModel.getResult().get(position).getState());
        binding.etCountryName.setText(plantationListModel.getResult().get(position).getCountry());
        binding.etLocationName.setText(plantationListModel.getResult().get(position).getLocation());
        for (StateCityModel stateCityDetail : stateCityModel) {
            if (stateCityDetail.getState().equalsIgnoreCase(plantationListModel.getResult().get(position).getState())) {
                cityModel.addAll(stateCityDetail.getDistricts());
            }
        }
    }

    @Override
    public void plantationOnClick(int position, String action) {
        this.position = position;

        switch (action) {
            case "edit":
                isEdit = true;
                editProject(this.position);
                break;
            case "delete":
                isEdit = false;
                deleteProject(this.position);
                break;
            default:
                isEdit = false;
        }
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
            getDeleteTreeManagementHashMap.put("action", "delete_plantation");
            getDeleteTreeManagementHashMap.put("plantation_id", plantationListModel.getResult().get(position).getId());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                addPlantationPresenter.getDeleteOperator(getDeleteTreeManagementHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }
}