package com.treeambulance.service.views.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.BuildConfig;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentSupervisorModeBinding;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.presenter.SupervisorModePresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.ChangePasswordBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.ReportsBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class SupervisorModeFragment extends BaseFragment implements View.OnClickListener, ReportsBottomSheetFragment.ReportsOnClickListener, SupervisorModePresenter.ContactInterface, ChangePasswordBottomSheetFragment.ChangePasswordOnClickListener {

    private FragmentSupervisorModeBinding binding;
    private SupervisorModePresenter supervisorModePresenter;
    private HashMap<String, String> supervisorModeHashMap;

    public SupervisorModeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_supervisor_mode, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        supervisorModeHashMap = new HashMap<>();
        supervisorModePresenter = new SupervisorModePresenter(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackSupervisorMode:
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.btCreateProject:
                moveToFragment(new AddProjectFragment(), android.R.id.content, false);
                break;
            case R.id.btProjectMaps:
                moveToFragment(new ProjectMapViewFragment(), android.R.id.content, false);
                break;
            case R.id.btPlantationMaps:
                moveToFragment(new PlantationMapViewFragment(), android.R.id.content, false);
                break;
            case R.id.btOperatorManagement:
                moveToFragment(new ManageOperatorFragment(), android.R.id.content, false);
                break;
            case R.id.btTreeManagement:
                moveToFragment(new TreeManagementFragment(), android.R.id.content, false);
                break;
            case R.id.btReportGeneration:
                moveToFragment(new ReportGenerationFragment(), android.R.id.content, false);
                break;
            case R.id.btNewPlanManagement:
                moveToFragment(new AddPlantationFragment(), android.R.id.content, false);
                break;
            case R.id.btChangePassword:
                openChangePassword();
                break;
        }
    }

    private void openChangePassword() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new ChangePasswordBottomSheetFragment(this);
        bottomSheetDialogFragment.show(fragmentManager, "change_password_dialog_sheet");
    }

    private void webView(String webUrl) {
        moveToFragment(new WebViewFragment(webUrl), android.R.id.content, false);
    }

    private void openBrowser(String webUrl) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(webUrl));
        startActivity(i);
    }

    @Override
    public void reportsClickListener(String toFragment) {
        switch (toFragment) {
            case "survey_report": {
                moveToFragment(new SurveyReportFragment(), android.R.id.content, false);
            }
            break;
            case "transplantation_report": {
                moveToFragment(new TransplantationReportFragment(), android.R.id.content, false);
            }
            break;
            case "maintenance_report": {
                moveToFragment(new MaintenanceReportFragment(), android.R.id.content, false);
            }
            break;
            case "consolidated_report": {
                moveToFragment(new ConsolidatedReportFragment(), android.R.id.content, false);
            }
            break;
        }
    }

    @Override
    public void changePasswordClickListener(HashMap<String, String> changePasswordHashMap) {

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            supervisorModePresenter.getChangePassword(changePasswordHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    @Override
    public void onSuccessChangePassword(CommonModel commonModel) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), commonModel.getMsg());
    }

    @Override
    public void onErrorChangePassword(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        supervisorModePresenter.onDispose();
    }
}