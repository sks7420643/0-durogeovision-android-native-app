package com.treeambulance.service.views.fragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentEditCompanyBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.EditCompanyModel;
import com.treeambulance.service.presenter.EditCompanyPresenter;
import com.treeambulance.service.views.adapter.EditSelectedBluetoothDeviceAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.BluetoothDeviceBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class EditCompanyFragment extends BaseFragment implements View.OnClickListener,
        BluetoothDeviceBottomSheetFragment.AddSelectedDrives, EditCompanyPresenter.ContactInterface {

    private static final int REQUEST_ENABLE_BT = 1;
    private FragmentEditCompanyBinding binding;
    private BluetoothAdapter bluetoothAdapter;

    private EditSelectedBluetoothDeviceAdapter editSelectedRFIDDeviceAdapter;
    private EditSelectedBluetoothDeviceAdapter editSelectedGPSDeviceAdapter;

    private Set<BluetoothDevice> setBluetoothDevice;

    private ArrayList<BluetoothDeviceListModel> rfidBluetoothDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> gpsBluetoothDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> rfidSelectedDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> gpsSelectedDeviceModelsList;

    private HashMap<String, String> getCompanyDetailsHashMap;
    private HashMap<String, RequestBody> editCompanyHashMap;

    private String onSelected = "",selected = "",imgPath1stTree;
    private EditCompanyPresenter editCompanyPresenter;

    public EditCompanyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_company, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        setBluetoothDevice = bluetoothAdapter.getBondedDevices();

        rfidBluetoothDeviceModelsList = new ArrayList<>();
        gpsBluetoothDeviceModelsList = new ArrayList<>();
        rfidSelectedDeviceModelsList = new ArrayList<>();
        gpsSelectedDeviceModelsList = new ArrayList<>();

        editCompanyHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();

        binding.setStrCompanyName(sharedHelper.getFromUser("company_name"));
        if (commonFunction.NullPointerValidator(sharedHelper.getFromUser("company_logo"))) {
            if (commonFunction.NullPointerValidator(sharedHelper.getFromUser("company_logo"))) {
                Glide.with(activity).load(sharedHelper.getFromUser("company_logo")).into(binding.ivLogoName);
                imgPath1stTree = sharedHelper.getFromUser("company_logo");
            }
        }
        Timber.i("onViewCreated: setBluetoothDevice: %s", new Gson().toJson(setBluetoothDevice));

        editCompanyPresenter = new EditCompanyPresenter(this);
        hitGetCompanyDetails();
        addBluetoothItemsToList();
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            editCompanyPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        rfidBluetoothDeviceModelsList.add(new BluetoothDeviceListModel("", 0, "Not Available", 0, false));
        /*gpsBluetoothDeviceModelsList.add(new BluetoothDeviceListModel("", 0, "Not Available", 0, false));*/

        for (BluetoothDevice bluetoothDevice : list) {
            rfidBluetoothDeviceModelsList.add(new BluetoothDeviceListModel(bluetoothDevice.getAddress(), bluetoothDevice.getBondState(), bluetoothDevice.getName(), bluetoothDevice.getType(), false));
            gpsBluetoothDeviceModelsList.add(new BluetoothDeviceListModel(bluetoothDevice.getAddress(), bluetoothDevice.getBondState(), bluetoothDevice.getName(), bluetoothDevice.getType(), false));
        }

        Timber.i("addBluetoothItemsToList: bluetoothDeviceModelsList: %s", new Gson().toJson(rfidBluetoothDeviceModelsList));
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etCompanyName.getText().toString().trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Enter the Company Name");
        } else if (!commonFunction.NullPointerValidator(imgPath1stTree.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Company Logo");
        } else if (rfidSelectedDeviceModelsList.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one RFID Device");
        } else if (gpsSelectedDeviceModelsList.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one GPS Device");
        } else {


            editCompanyHashMap.put("action", RequestBody.create("edit_company", MediaType.parse("text/plain")));
            editCompanyHashMap.put("company_id", RequestBody.create(sharedHelper.getFromUser("company_id"), MediaType.parse("text/plain")));
            editCompanyHashMap.put("company_name", RequestBody.create(binding.etCompanyName.getText().toString().trim(), MediaType.parse("text/plain")));
            editCompanyHashMap.put("rfid_list", RequestBody.create(new Gson().toJson(rfidSelectedDeviceModelsList), MediaType.parse("text/plain")));
            editCompanyHashMap.put("gps_list", RequestBody.create(new Gson().toJson(gpsSelectedDeviceModelsList), MediaType.parse("text/plain")));


            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                if (imgPath1stTree.indexOf("storage") > 0){
                    editCompanyPresenter.getEditCompany(editCompanyHashMap,
                            commonFunction.requestImage("logo_image", imgPath1stTree, context)
                    );
                }else{
                    editCompanyHashMap.put("image_name", RequestBody.create(imgPath1stTree, MediaType.parse("text/plain")));
                    editCompanyPresenter.getEditCompanyData(editCompanyHashMap);
                }
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo","Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setItems(options, (dialog, item) -> {

            /*if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                dialog.dismiss();
            }*/
            if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(takePicture, 2);
                dialog.dismiss();
            } else if (options[item].equals("Choose from Gallery")) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                dialog.dismiss();
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        Uri tempUri = commonFunction.getImageUri(context, selectedImage);
                        File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                        if (selected.equalsIgnoreCase("logo")) {
                            imgPath1stTree = finalFile.getPath();
                            binding.ivLogoName.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                            Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                        }
                    }

                    break;
                case 2:
                    if (resultCode == RESULT_OK) {

                        File file = new File(Environment.getExternalStorageDirectory().toString());

                        for (File temp : file.listFiles()) {
                            if (temp.getName().equals("temp.jpg")) {
                                file = temp;
                                break;
                            }
                        }

                        try {
                            Bitmap bitmap;
                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
                            bitmap = commonFunction.getResizedBitmap(bitmap);

                            Uri tempUri = commonFunction.getImageUri(context, bitmap);
                            File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                            if (selected.equalsIgnoreCase("logo")) {
                                imgPath1stTree = finalFile.getPath();
                                binding.ivLogoName.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                                Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = activity.getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                if (selected.equalsIgnoreCase("logo")) {
                                    imgPath1stTree = picturePath;
                                    binding.ivLogoName.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                                    Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                                }
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void loadGPSAdapter() {
        if (editSelectedGPSDeviceAdapter == null) {
            editSelectedGPSDeviceAdapter = new EditSelectedBluetoothDeviceAdapter(gpsSelectedDeviceModelsList, position -> {
                gpsSelectedDeviceModelsList.remove(position);
                editSelectedGPSDeviceAdapter.notifyDataSetChanged();
                binding.tvNoOfGPSDevice.setText(gpsSelectedDeviceModelsList.size() + " Devices");
            });

            binding.rvGPSDevices.setAdapter(editSelectedGPSDeviceAdapter);
        } else {
            editSelectedGPSDeviceAdapter.notifyDataSetChanged();
        }

        binding.tvNoOfGPSDevice.setText(gpsSelectedDeviceModelsList.size() + " Devices");
        binding.rvGPSDevices.setVisibility(View.VISIBLE);
        binding.tvNoGPS.setVisibility(View.GONE);
    }

    private void loadRFIDAdapter() {

        if (editSelectedRFIDDeviceAdapter == null) {
            editSelectedRFIDDeviceAdapter = new EditSelectedBluetoothDeviceAdapter(rfidSelectedDeviceModelsList, position -> {
                rfidSelectedDeviceModelsList.remove(position);
                editSelectedRFIDDeviceAdapter.notifyDataSetChanged();
                binding.tvNoOfRFIDDevice.setText(rfidSelectedDeviceModelsList.size() + " Devices");
            });

            binding.rvRFIDDevices.setAdapter(editSelectedRFIDDeviceAdapter);
        } else {
            editSelectedRFIDDeviceAdapter.notifyDataSetChanged();
        }
        binding.tvNoOfRFIDDevice.setText(rfidSelectedDeviceModelsList.size() + " Devices");
        binding.rvRFIDDevices.setVisibility(View.VISIBLE);
        binding.tvNoRFID.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackEditCompany: {
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.ivLogoName:
                selected = "logo";
                selectImage();
                break;
            case R.id.etAddRFIDDevice: {

                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }

                for (int i = 0; i < rfidBluetoothDeviceModelsList.size(); i++) {
                    rfidBluetoothDeviceModelsList.get(i).setChecked(false);
                }

                if (rfidBluetoothDeviceModelsList != null && !rfidBluetoothDeviceModelsList.isEmpty()) {
                    onSelected = "rfid";
                    bottomSheetDialogFragment = new BluetoothDeviceBottomSheetFragment(rfidBluetoothDeviceModelsList, this);
                    bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Pair with RFID Scanner" + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.etAddGPSDevice: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                for (int i = 0; i < gpsBluetoothDeviceModelsList.size(); i++) {
                    gpsBluetoothDeviceModelsList.get(i).setChecked(false);
                }
                if (gpsBluetoothDeviceModelsList != null && !gpsBluetoothDeviceModelsList.isEmpty()) {
                    onSelected = "gps";
                    bottomSheetDialogFragment = new BluetoothDeviceBottomSheetFragment(gpsBluetoothDeviceModelsList, this);
                    bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Pair with RFID Scanner" + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.btSave: {
                validator();
                Timber.i("CompanyName: " + binding.etCompanyName.getText().toString().trim() + " RFIDList: " + new Gson().toJson(rfidSelectedDeviceModelsList) + " GPSList: " + new Gson().toJson(gpsSelectedDeviceModelsList));
            }
            break;
        }
    }

    @Override
    public void addDrives(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel) {
        if (onSelected.equalsIgnoreCase("rfid")) {

            for (BluetoothDeviceListModel event1 : bluetoothDeviceListModel) {
                boolean isFound = false;
                for (BluetoothDeviceListModel event2 : rfidSelectedDeviceModelsList) {
                    if (event2.getAddress().equals(event1.getAddress()) || (event2.equals(event1))) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound && event1.isChecked()) {
                    rfidSelectedDeviceModelsList.add(event1);
                }
            }

            if (!rfidSelectedDeviceModelsList.isEmpty()) {
                Timber.i("addDrives: rfidSelectedDeviceModelsList - %s", new Gson().toJson(rfidSelectedDeviceModelsList));
                loadRFIDAdapter();
            } else {
                binding.tvNoOfRFIDDevice.setText("0 Devices");
                binding.rvRFIDDevices.setVisibility(View.GONE);
                binding.tvNoRFID.setVisibility(View.VISIBLE);
            }

        } else if (onSelected.equalsIgnoreCase("gps")) {
            /*gpsBluetoothDeviceModelsList = new ArrayList<>();
            gpsBluetoothDeviceModelsList = bluetoothDeviceListModel;*/

            for (BluetoothDeviceListModel event1 : bluetoothDeviceListModel) {
                boolean isFound = false;
                for (BluetoothDeviceListModel event2 : gpsSelectedDeviceModelsList) {
                    if (event2.getAddress().equals(event1.getAddress()) || (event2.equals(event1))) {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound && event1.isChecked()) {
                    gpsSelectedDeviceModelsList.add(event1);
                }
            }

            if (!gpsSelectedDeviceModelsList.isEmpty()) {
                Timber.i("addDrives: gpsSelectedDeviceModelsList - %s", new Gson().toJson(gpsSelectedDeviceModelsList));
                loadGPSAdapter();

            } else {
                binding.tvNoOfGPSDevice.setText("0 Devices");
                binding.rvGPSDevices.setVisibility(View.GONE);
                binding.tvNoGPS.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        if (companyDetailsModel.getResult().getLogoName() != null){
            Glide.with(activity).load(companyDetailsModel.getResult().getLogoName()).into(binding.ivLogoName);
            imgPath1stTree = companyDetailsModel.getResult().getLogoName();
            sharedHelper.putInUser("company_logo", companyDetailsModel.getResult().getLogoName());
        }else{
            imgPath1stTree = "";
//            binding.ivLogoName.se
        }
        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                gpsSelectedDeviceModelsList.add(new BluetoothDeviceListModel(list.getAddress(), Integer.parseInt(list.getBondstate()), list.getName(), Integer.parseInt(list.getType()), true));
                loadGPSAdapter();
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(gpsSelectedDeviceModelsList));

        if (!companyDetailsModel.getResult().getRfidList().isEmpty()) {
            for (CompanyDetailsModel.RfidList list : companyDetailsModel.getResult().getRfidList()) {
                rfidSelectedDeviceModelsList.add(new BluetoothDeviceListModel(list.getAddress(), Integer.parseInt(list.getBondstate()), list.getName(), Integer.parseInt(list.getType()), true));
                loadRFIDAdapter();
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "RFID Device List is Empty");
        }

        Timber.i("onSuccessCompanyDetails: RFIDSelectedBluetooth: %s", new Gson().toJson(rfidSelectedDeviceModelsList));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessEditCompany(EditCompanyModel model) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());
        sharedHelper.putInUser("company_id", model.getResult().getCompanyId());
        if (model.getResult().getLogoName() != null){
            Glide.with(activity).load(model.getResult().getLogoName()).into(binding.ivLogoName);
            imgPath1stTree = model.getResult().getLogoName();
            sharedHelper.putInUser("company_logo", model.getResult().getLogoName());
        }else{
            imgPath1stTree = "";
        }

        moveToFragment(new ProjectHomeFragment(), R.id.flHomeActivity, true);
        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onErrorEditCompany(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, getResources().getString(R.string.failed_title), error);
    }

    @Override
    public void onSuccessEditCompanyData(EditCompanyModel model) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());
        sharedHelper.putInUser("company_id", model.getResult().getCompanyId());
        if (model.getResult().getLogoName() != null){
            Glide.with(activity).load(model.getResult().getLogoName()).into(binding.ivLogoName);
            imgPath1stTree = model.getResult().getLogoName();
            sharedHelper.putInUser("company_logo", model.getResult().getLogoName());
        }else{
            imgPath1stTree = "";
        }
        moveToFragment(new ProjectHomeFragment(), R.id.flHomeActivity, true);
        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onErrorEditCompanyData(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, getResources().getString(R.string.failed_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        editCompanyPresenter.onDispose();
    }
}