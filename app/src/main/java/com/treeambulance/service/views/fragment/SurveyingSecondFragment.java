package com.treeambulance.service.views.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentSurveyingSecondBinding;
import com.treeambulance.service.eventBus.CloseFragmentEvent;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.presenter.SurveyingPresenter;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class SurveyingSecondFragment extends BaseFragment implements View.OnClickListener,
        SurveyingPresenter.ContactInterface {

    FragmentSurveyingSecondBinding binding;
    private HashMap<String, RequestBody> createSurveyingHashMap = new HashMap<>();
    private String selected = "", imgPath1stTree = "", imgPath2ndTree = "", imgPathSelifePhoto = "";
    private SurveyingPresenter surveyingPresenter;

    public SurveyingSecondFragment(HashMap<String, RequestBody> createSurveyingHashMap) {
        // Required empty public constructor
        this.createSurveyingHashMap = createSurveyingHashMap;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_surveying_second, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        surveyingPresenter = new SurveyingPresenter(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackSurveying2:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.ivTree1st:
                selected = "1stTree";
//                selectImage();
                takePhoto();
                break;
            case R.id.ivTree2nd:
                selected = "2ndTree";
//                selectImage();
                takePhoto();
                break;
            case R.id.ivSelfiePhoto:
                selected = "selfiePhoto";
//                selectImage();
                takePhoto();
                break;
            case R.id.btSave:
                validator();
                break;
        }
    }

    private void takePhoto() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(takePicture, 2);
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", /*"Choose from Gallery",*/ "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setItems(options, (dialog, item) -> {

            /*if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                dialog.dismiss();
            }*/
            if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(takePicture, 2);
                dialog.dismiss();
            } else if (options[item].equals("Choose from Gallery")) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                dialog.dismiss();
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void validator() {

        if (!commonFunction.NullPointerValidator(imgPath1stTree.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Tree 1st Photo");
        } else if (!commonFunction.NullPointerValidator(imgPath2ndTree.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Tree 2st Photo");
        } else if (!commonFunction.NullPointerValidator(imgPathSelifePhoto.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Supervisor Selfie Photo");
        } else {

            createSurveyingHashMap.put("company_id", RequestBody.create(sharedHelper.getFromUser("company_id"), MediaType.parse("text/plain")));

            if (commonFunction.NullPointerValidator(binding.etNotes.getText().toString().trim())) {
                createSurveyingHashMap.put("notes", RequestBody.create(binding.etNotes.getText().toString().trim(), MediaType.parse("text/plain")));
            }
            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                surveyingPresenter.getCreateSurveying(createSurveyingHashMap,
                        commonFunction.requestImage("tree_first_photo", imgPath1stTree, context),
                        commonFunction.requestImage("tree_second_photo", imgPath2ndTree, context),
                        commonFunction.requestImage("selfie_photo", imgPathSelifePhoto, context)
                );
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        Uri tempUri = commonFunction.getImageUri(context, selectedImage);
                        File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                        if (selected.equalsIgnoreCase("1stTree")) {
                            imgPath1stTree = finalFile.getPath();
                            binding.ivTree1st.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                            Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                        } else if (selected.equalsIgnoreCase("2ndTree")) {
                            imgPath2ndTree = finalFile.getPath();
                            binding.ivTree2nd.setImageBitmap(BitmapFactory.decodeFile(imgPath2ndTree));
                            Timber.i("onActivityResult: CAMERA 2ndTree: %s", imgPath2ndTree);
                        } else if (selected.equalsIgnoreCase("selfiePhoto")) {
                            imgPathSelifePhoto = finalFile.getPath();
                            binding.ivSelfiePhoto.setImageBitmap(BitmapFactory.decodeFile(imgPathSelifePhoto));
                            Timber.i("onActivityResult: CAMERA selfiePhoto: %s", imgPathSelifePhoto);
                        }
                    }

                    break;
                case 2:
                    if (resultCode == RESULT_OK) {

                        File file = new File(Environment.getExternalStorageDirectory().toString());

                        for (File temp : file.listFiles()) {
                            if (temp.getName().equals("temp.jpg")) {
                                file = temp;
                                break;
                            }
                        }

                        try {
                            Bitmap bitmap;
                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
                            bitmap = commonFunction.getResizedBitmap(bitmap);

                            Uri tempUri = commonFunction.getImageUri(context, bitmap);
                            File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                            if (selected.equalsIgnoreCase("1stTree")) {
                                binding.ivTree1st.setImageBitmap(bitmap);
                                imgPath1stTree = finalFile.getPath();
                            } else if (selected.equalsIgnoreCase("2ndTree")) {

                                binding.ivTree2nd.setImageBitmap(bitmap);
                                imgPath2ndTree = finalFile.getPath();
                            } else if (selected.equalsIgnoreCase("selfiePhoto")) {

                                binding.ivSelfiePhoto.setImageBitmap(bitmap);
                                imgPathSelifePhoto = finalFile.getPath();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = activity.getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                if (selected.equalsIgnoreCase("1stTree")) {
                                    imgPath1stTree = picturePath;
                                    binding.ivTree1st.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                                    Timber.i("onActivityResult: GALLERY 1stTree: " + imgPath1stTree);
                                } else if (selected.equalsIgnoreCase("2ndTree")) {
                                    imgPath2ndTree = picturePath;
                                    binding.ivTree2nd.setImageBitmap(BitmapFactory.decodeFile(imgPath2ndTree));
                                    Timber.i("onActivityResult: GALLERY 2ndTree: " + imgPath2ndTree);
                                } else if (selected.equalsIgnoreCase("selfiePhoto")) {
                                    imgPathSelifePhoto = picturePath;
                                    binding.ivSelfiePhoto.setImageBitmap(BitmapFactory.decodeFile(imgPathSelifePhoto));
                                    Timber.i("onActivityResult: GALLERY selfiePhoto: " + imgPathSelifePhoto);
                                }
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onSuccessListProjects(ListProjectsModel listProjectsModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, getResources().getString(R.string.failed_title), error);
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessCheckSurveyNo(CommonModel model) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCheckSurveyNo(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessCreateSurveying(CommonModel model) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());

        eventBus.postSticky(new CloseFragmentEvent("surveying_first"));
        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onErrorCreateSurveying(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, getResources().getString(R.string.failed_title), error);
    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessTreeManagementList(TreeManagementListModel addTreeManagementModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorTreeManagementList(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        surveyingPresenter.onDispose();
    }
}