package com.treeambulance.service.views.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentPlantationBinding;
import com.treeambulance.service.eventBus.CloseFragmentEvent;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.CurrencyNameModel;
import com.treeambulance.service.model.ListPlantationModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.model.TreeDetailsModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.presenter.PlantationPresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllPlantationBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CityListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CountryListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.GPSReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.OperatorListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.RFIDReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StateListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.TreeListBottomSheetFragment;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

public class PlantationFragment extends BaseFragment implements View.OnClickListener,CountryListBottomSheetFragment.CountryListClickListener,
        CustomDialogBottomSheetFragment.CustomDialogOnButtonClick, PlantationPresenter.ContactInterface,
        AllPlantationBottomSheetFragment.OnPlantationClickListener,StateListBottomSheetFragment.OnStateClickListener, CityListBottomSheetFragment.OnCityClickListener,OperatorListBottomSheetFragment.OperatorListClickListener,
        RFIDReadingBottomSheetFragment.RFIDReading, GPSReadingBottomSheetFragment.GPSReading,TreeListBottomSheetFragment.TreeClickListener{

    private FragmentPlantationBinding binding;
    private ArrayList<StateCityModel> stateCityModel;
    private PlantationPresenter plantationPresenter;
    private HashMap<String, String> allProjectsHashMap;
    private HashMap<String, String> getOperatorListHashMap,getTreeManagementListHashMap;
    private HashMap<String, String> getCompanyDetailsHashMap;
    private HashMap<String, String> getCheckSurveyNoHashMap, getCountryListHashMap;
    private HashMap<String, RequestBody> createSurveyingHashMap;
    private ArrayList<CountryListModel.Result> countryListResultModel;
    private ListPlantationModel listPlantationModel;
    private OperatorListModel operatorListModel;
    private TreeManagementListModel treeManagementListModel;
    private ArrayList<CurrencyNameModel> currencyNameModel;
    private CompanyDetailsModel companyDetailsModel;
    private ArrayList<String> cityModel,GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private Calendar myCalendar = Calendar.getInstance();
    private String str_PlantationDate = "", projectId = "", operatorId = "", str_CountryType ="", str_CurrencyName = "";;
    private Set<BluetoothDevice> setBluetoothDevice;
    AutoCompleteTextView textView=null;
    private ArrayAdapter<String> adapter;
    private DatePickerDialog.OnDateSetListener a_date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            str_PlantationDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            binding.etPlantingDate.setText(commonFunction.getReverseDate(str_PlantationDate));
        }
    };

    public PlantationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plantation, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        plantationPresenter = new PlantationPresenter(this);
        stateCityModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        allProjectsHashMap = new HashMap<>();
        getOperatorListHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        getCheckSurveyNoHashMap = new HashMap<>();
        createSurveyingHashMap = new HashMap<>();
        countryListResultModel = new ArrayList<>();
        getCountryListHashMap = new HashMap<>();
        getTreeManagementListHashMap = new HashMap<>();
        GPSDeviceList = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();
        currencyNameModel = new ArrayList<>();
        treeManagementListModel = new TreeManagementListModel();
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        binding.etPlantingDate.setText(commonFunction.getCurrentDate()[0]);
        str_PlantationDate = commonFunction.getCurrentDate()[1];

//        String[] stringArray = getResources().getStringArray(R.array.currency_status);
//        textView = (AutoCompleteTextView) getView().findViewById(R.id.autoCompleteTextView);
//
//        //Create adapter
//        adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_dropdown_item_1line, stringArray);
//
//        textView.setThreshold(1);
//
//        //Set adapter to AutoCompleteTextView
//        textView.setAdapter(adapter);

        hitGetAllProjects();
        hitGetOperatorList();
        hitGetCompanyDetails();
        addBluetoothItemsToList();
        hitGetCountryList();
        hitGetTreeManagementList();
        loadJSONFromAsset();
    }

    private void hitGetCountryList(){
        getCountryListHashMap.put("action", "country_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getCountryList(getCountryListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetTreeManagementList() {

        getTreeManagementListHashMap.put("action", "list_trees");
        getTreeManagementListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getTreeManagementList(getTreeManagementListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        for (BluetoothDevice bluetoothDevice : list) {

            GPSFromBluetooth.add(bluetoothDevice.getAddress());
        }

        Timber.i("addBluetoothItemsToList: GPSFromBluetooth: %s", new Gson().toJson(GPSFromBluetooth));

    }

    private void hitGetAllProjects() {
        allProjectsHashMap.put("action", "list_plantation");
        allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getListPlantations(allProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/states_and_districts.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray));

        String str_response = new Gson().toJson(jsonArray);

        try {
            Type type = new TypeToken<ArrayList<StateCityModel>>() {
            }.getType();

            stateCityModel = new Gson().fromJson(str_response, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(stateCityModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }

        JsonArray jsonArray1 = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/currency_name.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray currencyNameModel: %s", new Gson().toJson(jsonArray1));

        String str_response1 = new Gson().toJson(jsonArray1);

        try {
            Type type = new TypeToken<ArrayList<CurrencyNameModel>>() {
            }.getType();

            currencyNameModel = new Gson().fromJson(str_response1, type);

            Timber.i("loadJSONFromAsset: currencyNameModel: %s", new Gson().toJson(currencyNameModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: currencyNameModel: %s", e.getMessage());
        }



    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackSurveying1:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.etProjectName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (listPlantationModel != null && listPlantationModel.getResult() != null && !listPlantationModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new AllPlantationBottomSheetFragment(listPlantationModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "all_project_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etCountryName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (countryListResultModel != null && !countryListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new CountryListBottomSheetFragment(countryListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "country_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etPlantingDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, /*R.style.datepicker,*/ a_date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 30000);
                datePickerDialog.show();
                break;
            case R.id.etState:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etDistrict:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }
                break;
            case R.id.etOperatorName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new OperatorListBottomSheetFragment(operatorListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }
                break;
            case R.id.etLocalTreeName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new TreeListBottomSheetFragment(treeManagementListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the Local Tree Name");
                }
                break;
            case R.id.etBotanicalTreeName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new TreeListBottomSheetFragment(treeManagementListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the Botanical Tree Name");
                }
                break;
            case R.id.btNextPage:
                validator();
                break;
        }
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etProjectName.getText().toString().trim())) {
            binding.etProjectName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Plantation Name");
        } else if (!commonFunction.NullPointerValidator(binding.etPlantingDate.getText().toString().trim())) {
            binding.etPlantingDate.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Plantation Date");
        } else if (!commonFunction.NullPointerValidator(binding.etNewTreeNo.getText().toString().trim())) {
            binding.etNewTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the New Tree No");
        } else if (!commonFunction.NullPointerValidator(binding.etSurveyNo.getText().toString().trim())) {
            binding.etSurveyNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Land Survey No.");
        } else if (!commonFunction.NullPointerValidator(binding.etPlantedBy.getText().toString().trim())) {
            binding.etPlantedBy.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Planted By");
        } else if (!commonFunction.NullPointerValidator(binding.etAdoptedBy.getText().toString().trim())) {
            binding.etAdoptedBy.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Adopted By");
        } else if (!commonFunction.NullPointerValidator(binding.etHeightOfTree.getText().toString().trim())) {
            binding.etHeightOfTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Height of Tree");
        } else if (!commonFunction.NullPointerValidator(binding.etLocalTreeName.getText().toString().trim())) {
            binding.etLocalTreeName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Local Tree Name");
        } else if (!commonFunction.NullPointerValidator(binding.etBotanicalTreeName.getText().toString().trim())) {
            binding.etBotanicalTreeName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Botanical Tree Name");
        } else if (!commonFunction.NullPointerValidator(binding.etCountryName.getText().toString().trim())) {
            binding.etCountryName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Country");
        } else if (!commonFunction.NullPointerValidator(binding.etState.getText().toString().trim())) {
            binding.etState.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select State");
        } else if (!commonFunction.NullPointerValidator(binding.etDistrict.getText().toString().trim())) {
            binding.etDistrict.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select District");
        } else if (!commonFunction.NullPointerValidator(binding.etVillage.getText().toString().trim())) {
            binding.etVillage.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Village Name");
        } else if (!commonFunction.NullPointerValidator(binding.etLocationText.getText().toString().trim())) {
            binding.etLocationText.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Location");
        } else if (!commonFunction.NullPointerValidator(binding.etOperatorName.getText().toString().trim())) {
            binding.etOperatorName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Suprivisor Name");
        }  else {

            createSurveyingHashMap.put("action", RequestBody.create("create_plantation", MediaType.parse("text/plain")));
            createSurveyingHashMap.put("plant_project_id", RequestBody.create(projectId, MediaType.parse("text/plain")));
            createSurveyingHashMap.put("plant_date", RequestBody.create(str_PlantationDate, MediaType.parse("text/plain")));
            createSurveyingHashMap.put("new_tree_no", RequestBody.create(binding.etNewTreeNo.getText().toString(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("country", RequestBody.create(binding.etCountryName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("state", RequestBody.create(binding.etState.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("district", RequestBody.create(binding.etDistrict.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("village", RequestBody.create(binding.etVillage.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("planted_by", RequestBody.create(binding.etPlantedBy.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("adopted_by", RequestBody.create(binding.etAdoptedBy.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("height_of_tree", RequestBody.create(binding.etHeightOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("land_survey_no", RequestBody.create(binding.etSurveyNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("botanical_tree_name", RequestBody.create(binding.etBotanicalTreeName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("local_tree_name", RequestBody.create(binding.etLocalTreeName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("suprivisor_name", RequestBody.create(binding.etOperatorName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("aprx_value", RequestBody.create(binding.etAValueOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("age_tree_value", RequestBody.create(binding.etAgeOfTheTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("location_name", RequestBody.create(binding.etLocationText.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("contractor_name", RequestBody.create(binding.etContractorName.getText().toString().trim(), MediaType.parse("text/plain")));


            getCheckSurveyNoHashMap.put("action", "check_plantation_no");
            getCheckSurveyNoHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            getCheckSurveyNoHashMap.put("plant_project_id", projectId);

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
//                openRFIDReader();

                plantationPresenter.getCheckPlantation(getCheckSurveyNoHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void openRFIDReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment("Is RFID Available ?", this);
        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    private void openGPSReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }

//        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(GPSDeviceList, this);
        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(GPSSelectedBluetooth, this);
        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(fragmentManager, "GPS_reading_sheet");
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
//            moveToFragment(new SurveyingSecondFragment(createSurveyingHashMap), android.R.id.content, false);

            if (bottomSheetDialogFragment != null) {
                bottomSheetDialogFragment.dismiss();
            }

            bottomSheetDialogFragment = new RFIDReadingBottomSheetFragment(this);
            bottomSheetDialogFragment.setCancelable(false);
            bottomSheetDialogFragment.show(fragmentManager, "RFID_reading_sheet");
        } else {

            createSurveyingHashMap.put("rfid_code", RequestBody.create("not available", MediaType.parse("text/plain")));

            openGPSReader();
        }
    }

    @Override
    public void onSuccessListProject(ListPlantationModel listPlantationModel) {
        commonFunction.dismissLoader();
        if (listPlantationModel != null && listPlantationModel.getResult() != null && !listPlantationModel.getResult().isEmpty()) {
            this.listPlantationModel = listPlantationModel;
        }
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListModel = operatorListModel;
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }

        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(GPSSelectedBluetooth));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCheckSurveyNo(CommonModel model) {
        commonFunction.dismissLoader();
        openRFIDReader();
        //moveToFragment(new PlantationSecondFragment(createSurveyingHashMap), android.R.id.content, false);
    }

    @Override
    public void onErrorCheckSurveyNo(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
//        binding.etSurveyNo.requestFocus();
    }

    @Override
    public void onSuccessCreateSurveying(CommonModel model) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCreateSurveying(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
        if (countryListModel != null && countryListModel.getResult() != null && !countryListModel.getResult().isEmpty()) {
            this.countryListResultModel = countryListModel.getResult();;
        }
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessTreeManagementList(TreeManagementListModel treeManagementListModel) {
        commonFunction.dismissLoader();
        if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
            this.treeManagementListModel = treeManagementListModel;
        }
    }

    @Override
    public void onErrorTreeManagementList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCreateMaintenance(CommonModel model) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCreateMaintenance(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessTreeDetails(TreeDetailsModel listProjectsModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorTreeDetails(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onProjectClick(int position) {
        projectId = listPlantationModel.getResult().get(position).getId();
        binding.etProjectName.setText(listPlantationModel.getResult().get(position).getProjectName());
        binding.etCountryName.setText(listPlantationModel.getResult().get(position).getCountry());
        binding.etState.setText(listPlantationModel.getResult().get(position).getState());
        binding.etDistrict.setText(listPlantationModel.getResult().get(position).getDistrict());
        binding.etVillage.setText(listPlantationModel.getResult().get(position).getVillage());
        for (StateCityModel stateCityDetail : stateCityModel) {
            if (stateCityDetail.getState().equalsIgnoreCase(listPlantationModel.getResult().get(position).getState())) {
                cityModel.addAll(stateCityDetail.getDistricts());
            }
        }
        binding.etLocationText.setText(listPlantationModel.getResult().get(position).getLocation());
    }


    @Override
    public void getRFIDTagData(String tagData) {
        Timber.i("getRFIDTagData: %s", tagData);

        createSurveyingHashMap.put("rfid_code", RequestBody.create(tagData, MediaType.parse("text/plain")));

        openGPSReader();
//        moveToFragment(new PlantationSecondFragment(createSurveyingHashMap), android.R.id.content, false);
    }

    @Override
    public void getGPSTagData(boolean isRefresh, String latitude, String longitude) {

        if (isRefresh) {
            openGPSReader();
        } else {
//            commonFunction.showLongToast("LAT: " + latitude + "\nLON: " + longitude);

            createSurveyingHashMap.put("latitude", RequestBody.create(latitude, MediaType.parse("text/plain")));
            createSurveyingHashMap.put("longitude", RequestBody.create(longitude, MediaType.parse("text/plain")));

            moveToFragment(new PlantationSecondFragment(createSurveyingHashMap), android.R.id.content, false);
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void CloseActivityEventBus(CloseFragmentEvent closeFragmentEvent) {
        if (closeFragmentEvent.closeFragment.equalsIgnoreCase("surveying_first")) {
            fragmentManager.popBackStackImmediate();
        }
        eventBus.removeStickyEvent(closeFragmentEvent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (eventBus.isRegistered(this)) {
                eventBus.unregister(this);
            }
            if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
                bottomSheetDialogFragment.dismiss();
            }
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        plantationPresenter.onDispose();
    }

    @Override
    public void onCountryClick(int position) {
        binding.etCountryName.setText(countryListResultModel.get(position).getName());
        str_CountryType = countryListResultModel.get(position).getName();
    }


    @Override
    public void onStateClick(int position) {
        cityModel = new ArrayList<>();
        binding.etState.setText(stateCityModel.get(position).getState());

        cityModel.addAll(stateCityModel.get(position).getDistricts());
        Timber.i("onStateClick %s", new Gson().toJson(cityModel));
    }

    @Override
    public void onCityClick(int position) {
        binding.etDistrict.setText(cityModel.get(position));
        Timber.i("onCityClick %s", new Gson().toJson(cityModel.get(position)));
    }

    @Override
    public void onOperatorClick(int position) {
        binding.etOperatorName.setText(operatorListModel.getResult().get(position).getName());
        operatorId = operatorListModel.getResult().get(position).getId();
    }

    @Override
    public void onTreeClick(int position) {
        binding.etLocalTreeName.setText(treeManagementListModel.getResult().get(position).getTreeName());
        binding.etBotanicalTreeName.setText(treeManagementListModel.getResult().get(position).getBotanicalName());
    }
}