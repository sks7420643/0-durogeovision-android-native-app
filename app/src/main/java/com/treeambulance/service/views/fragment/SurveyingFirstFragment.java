package com.treeambulance.service.views.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentSurveyingFirstBinding;
import com.treeambulance.service.eventBus.CloseFragmentEvent;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.CurrencyNameModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.ReportTypeModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.model.StatusRangeModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.presenter.SurveyingPresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllProjectsBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CountryListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CurrencyNameBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.GPSReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.OperatorListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.RFIDReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.ReportTypeBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.TreeListBottomSheetFragment;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

public class SurveyingFirstFragment extends BaseFragment implements View.OnClickListener,CountryListBottomSheetFragment.CountryListClickListener,
        CustomDialogBottomSheetFragment.CustomDialogOnButtonClick, SurveyingPresenter.ContactInterface,
        AllProjectsBottomSheetFragment.OnProjectClickListener,OperatorListBottomSheetFragment.OperatorListClickListener,
        RFIDReadingBottomSheetFragment.RFIDReading, GPSReadingBottomSheetFragment.GPSReading, CurrencyNameBottomSheetFragment.OnCurrencyNameClickListener,TreeListBottomSheetFragment.TreeClickListener {

    private FragmentSurveyingFirstBinding binding;

    private SurveyingPresenter surveyingPresenter;
    private HashMap<String, String> allProjectsHashMap;
    private HashMap<String, String> getOperatorListHashMap,getTreeManagementListHashMap;
    private HashMap<String, String> getCompanyDetailsHashMap;
    private HashMap<String, String> getCheckSurveyNoHashMap, getCountryListHashMap;
    private HashMap<String, RequestBody> createSurveyingHashMap;
    private ArrayList<CountryListModel.Result> countryListResultModel;
    private ListProjectsModel listProjectsModel;
    private TreeManagementListModel treeManagementListModel;
    private OperatorListModel operatorListModel;
    private ArrayList<CurrencyNameModel> currencyNameModel;
    private CompanyDetailsModel companyDetailsModel;
    private ArrayList<String> GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private Calendar myCalendar = Calendar.getInstance();
    private String str_SurveyDate = "", projectId = "", operatorId = "", str_CountryType ="", str_CurrencyName = "";
    private double Pi_str = 3.14159;
    private Set<BluetoothDevice> setBluetoothDevice;

    private DatePickerDialog.OnDateSetListener a_date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            /*str_SurveyDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            binding.etSurveyDate.setText(commonFunction.getReverseDate(str_SurveyDate));*/
        }
    };

    public SurveyingFirstFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_surveying_first, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        surveyingPresenter = new SurveyingPresenter(this);

        allProjectsHashMap = new HashMap<>();
        getOperatorListHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        getCheckSurveyNoHashMap = new HashMap<>();
        createSurveyingHashMap = new HashMap<>();
        countryListResultModel = new ArrayList<>();
        getCountryListHashMap = new HashMap<>();
        GPSDeviceList = new ArrayList<>();
        getTreeManagementListHashMap = new HashMap<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();
        currencyNameModel = new ArrayList<>();
        treeManagementListModel = new TreeManagementListModel();
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        binding.etSurveyDate.setText(commonFunction.getCurrentDate()[0]);
        str_SurveyDate = commonFunction.getCurrentDate()[1];

        binding.etSizeOfGirth.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(!s.equals("") ) {
                    //do your work here

                }
            }



            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {
                if(!s.equals("") ) {
                    if (commonFunction.NullPointerValidator(binding.etSizeOfGirth.getText().toString().trim())) {
                        Double size = 0.0;
                        try {
                            size = Double.parseDouble(binding.etSizeOfGirth.getText().toString());
                        } catch (Exception e) {
                            size = 0.0;
                            e.printStackTrace();
                        }
                        Double radius = size / (2 * Pi_str);
                        if (commonFunction.NullPointerValidator(binding.etHeightOfTree.getText().toString().trim())) {
                            Double height = 0.0;
                            try {
                                height = Double.parseDouble(binding.etHeightOfTree.getText().toString());
                            } catch (Exception e) {
                                height = 0.0;
                                e.printStackTrace();
                            }
                            Double radius1 = (2 * Pi_str) * (radius * radius);
                            Double radius2 = (height * 100) * ((2 * Pi_str) * radius);
                            Double appVal = radius1 + radius2;
                            float number = Float.valueOf(String.valueOf(appVal));
                            if (commonFunction.NullPointerValidator(binding.etValueOfTree.getText().toString().trim())) {
                                Double etValue = 0.0;
                                try {
                                    etValue = Double.parseDouble(binding.etValueOfTree.getText().toString());
                                } catch (Exception e) {
                                    etValue = 0.0;
                                    e.printStackTrace();
                                }
                                float totalVal = Float.valueOf(String.valueOf(etValue)) * number;
                                int finalVal = Math.round(totalVal);
                                binding.etAValueOfTree.setText(String.valueOf(finalVal));
                            }
                        }
                    }
                }
            }
        });

        binding.etHeightOfTree.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(!s.equals("") ) {
                    //do your work here

                }
            }



            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {
                if(!s.equals("") ) {
                    if (commonFunction.NullPointerValidator(binding.etHeightOfTree.getText().toString().trim())){
                        Double size = 0.0;
                        try {
                            size = Double.parseDouble(binding.etHeightOfTree.getText().toString());
                        } catch (Exception e) {
                            size = 0.0;
                            e.printStackTrace();
                        }
                        Double radius = size / (2 * Pi_str);
                        if (commonFunction.NullPointerValidator(binding.etHeightOfTree.getText().toString().trim())) {
                            Double height = 0.0;
                            try {
                                height = Double.parseDouble(binding.etHeightOfTree.getText().toString());
                            } catch (Exception e) {
                                height = 0.0;
                                e.printStackTrace();
                            }
                            Double radius1 = (2 * Pi_str) * (radius * radius);
                            Double radius2 = (height * 100) * ((2 * Pi_str) * radius);
                            Double appVal = radius1 + radius2;
                            float number = Float.valueOf(String.valueOf(appVal));
                            if (commonFunction.NullPointerValidator(binding.etValueOfTree.getText().toString().trim())) {
                                Double etValue = 0.0;
                                try {
                                    etValue = Double.parseDouble(binding.etValueOfTree.getText().toString());
                                } catch (Exception e) {
                                    etValue = 0.0;
                                    e.printStackTrace();
                                }
                                float totalVal = Float.valueOf(String.valueOf(etValue)) * number;
                                int finalVal = Math.round(totalVal);
                                binding.etAValueOfTree.setText(String.valueOf(finalVal));
                            }
                        }
                    }
                }
            }
        });

        binding.etValueOfTree.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(!s.equals("") ) {
                    //do your work here

                }
            }



            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {
                if(!s.equals("") ) {
                    if (commonFunction.NullPointerValidator(binding.etValueOfTree.getText().toString().trim())){
                        Double size = 0.0;
                        try {
                            size = Double.parseDouble(binding.etValueOfTree.getText().toString());
                        } catch (Exception e) {
                            size = 0.0;
                            e.printStackTrace();
                        }
                        Double radius = size / (2 * Pi_str);
                        if (commonFunction.NullPointerValidator(binding.etHeightOfTree.getText().toString().trim())) {
                            Double height = 0.0;
                            try {
                                height = Double.parseDouble(binding.etHeightOfTree.getText().toString());
                            } catch (Exception e) {
                                height = 0.0;
                                e.printStackTrace();
                            }
                            Double radius1 = (2 * Pi_str) * (radius * radius);
                            Double radius2 = (height * 100) * ((2 * Pi_str) * radius);
                            Double appVal = radius1 + radius2;
                            float number = Float.valueOf(String.valueOf(appVal));
                            if (commonFunction.NullPointerValidator(binding.etValueOfTree.getText().toString().trim())) {
                                Double etValue = 0.0;
                                try {
                                    etValue = Double.parseDouble(binding.etValueOfTree.getText().toString());
                                } catch (Exception e) {
                                    etValue = 0.0;
                                    e.printStackTrace();
                                }
                                float totalVal = Float.valueOf(String.valueOf(etValue)) * number;
                                int finalVal = Math.round(totalVal);
                                binding.etAValueOfTree.setText(String.valueOf(finalVal));
                            }
                        }
                    }
                }
            }
        });

        hitGetAllProjects();
        hitGetOperatorList();
        hitGetCompanyDetails();
        addBluetoothItemsToList();
        hitGetCountryList();
        loadJSONFromAsset();
        hitGetTreeManagementList();
    }

    private void hitGetCountryList(){
        getCountryListHashMap.put("action", "country_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            surveyingPresenter.getCountryList(getCountryListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetTreeManagementList() {

        getTreeManagementListHashMap.put("action", "list_trees");
        getTreeManagementListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            surveyingPresenter.getTreeManagementList(getTreeManagementListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        for (BluetoothDevice bluetoothDevice : list) {

            GPSFromBluetooth.add(bluetoothDevice.getAddress());
        }

        Timber.i("addBluetoothItemsToList: GPSFromBluetooth: %s", new Gson().toJson(GPSFromBluetooth));

    }

    private void hitGetAllProjects() {
        allProjectsHashMap.put("action", "list_projects");
        allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            surveyingPresenter.getListProjects(allProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            surveyingPresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            surveyingPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray1 = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/currency_name.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray currencyNameModel: %s", new Gson().toJson(jsonArray1));

        String str_response1 = new Gson().toJson(jsonArray1);

        try {
            Type type = new TypeToken<ArrayList<CurrencyNameModel>>() {
            }.getType();

            currencyNameModel = new Gson().fromJson(str_response1, type);

            Timber.i("loadJSONFromAsset: currencyNameModel: %s", new Gson().toJson(currencyNameModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: currencyNameModel: %s", e.getMessage());
        }



    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackSurveying1:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.etProjectName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new AllProjectsBottomSheetFragment(listProjectsModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "all_project_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etCountryName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (countryListResultModel != null && !countryListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new CountryListBottomSheetFragment(countryListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "country_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etSurveyorName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new OperatorListBottomSheetFragment(operatorListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Surveyor is not available");
                }
                break;
            /*case R.id.etSurveyDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, *//*R.style.datepicker,*//* a_date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 30000);
                datePickerDialog.show();
                break;*/
            case R.id.etState:
                /*if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }*/
                break;
            case R.id.etDistrict:
                /*if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }*/
                break;
            case R.id.etLocalTreeName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new TreeListBottomSheetFragment(treeManagementListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the Local Tree Name");
                }
                break;
            case R.id.etBotanicalTreeName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new TreeListBottomSheetFragment(treeManagementListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the Botanical Tree Name");
                }
                break;
            case R.id.etAValueOfTree:
//                if (currencyNameModel != null && !currencyNameModel.isEmpty()) {
//                    bottomSheetDialogFragment = new CurrencyNameBottomSheetFragment(currencyNameModel, this);
//                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
//                } else {
//                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
//                }
            case R.id.btNextPage:
                validator();
                break;
        }
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etProjectName.getText().toString().trim())) {
            binding.etProjectName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Project Name");
        } else if (!commonFunction.NullPointerValidator(binding.etSurveyDate.getText().toString().trim())) {
            binding.etSurveyDate.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Survey Date");
        } else if (!commonFunction.NullPointerValidator(binding.etState.getText().toString().trim())) {
            binding.etState.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select State");
        } else if (!commonFunction.NullPointerValidator(binding.etDistrict.getText().toString().trim())) {
            binding.etDistrict.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select District");
        } else if (!commonFunction.NullPointerValidator(binding.etVillage.getText().toString().trim())) {
            binding.etVillage.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Village Name");
        } else if (!commonFunction.NullPointerValidator(binding.etLocationText.getText().toString().trim())) {
            binding.etLocationText.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Location");
        } else if (!commonFunction.NullPointerValidator(binding.etTreeNo.getText().toString().trim())) {
            binding.etTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Old Tree No");
        } else if (!commonFunction.NullPointerValidator(binding.etNewTreeNo.getText().toString().trim())) {
            binding.etNewTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the New Tree No");
        } else if (!commonFunction.NullPointerValidator(binding.etLandSurveyNo.getText().toString().trim())) {
            binding.etLandSurveyNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Land Survey No.");
        } else if (!commonFunction.NullPointerValidator(binding.etNameLand.getText().toString().trim())) {
            binding.etNameLand.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Name of the land owner");
        } else if (!commonFunction.NullPointerValidator(binding.etLocalTreeName.getText().toString().trim())) {
            binding.etLocalTreeName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Local Tree Name");
        } else if (!commonFunction.NullPointerValidator(binding.etBotanicalTreeName.getText().toString().trim())) {
            binding.etBotanicalTreeName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Botanical Tree Name");
        } else if (!commonFunction.NullPointerValidator(binding.etHealthStatus.getText().toString().trim())) {
            binding.etHealthStatus.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Health Status");
        } else if (!commonFunction.NullPointerValidator(binding.etSizeOfGirth.getText().toString().trim())) {
            binding.etSizeOfGirth.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Size Of Girth");
        } else if (!commonFunction.NullPointerValidator(binding.etHeightOfTree.getText().toString().trim())) {
            binding.etHeightOfTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Height of Tree");
        } else if (!commonFunction.NullPointerValidator(binding.etValueOfTree.getText().toString().trim())) {
            binding.etValueOfTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Approximate value of the Tree");
        } else if (!commonFunction.NullPointerValidator(binding.etAValueOfTree.getText().toString().trim())) {
            binding.etAValueOfTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Approximate value of the Tree");
        }else if (!commonFunction.NullPointerValidator(binding.etAgeOfTheTree.getText().toString().trim())) {
            binding.etAgeOfTheTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Age in Years");
        } else if (!commonFunction.NullPointerValidator(binding.etContractorName.getText().toString().trim())) {
            binding.etContractorName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Contractor Name");
        } else if (!commonFunction.NullPointerValidator(binding.etSurveyorName.getText().toString().trim())) {
            binding.etSurveyorName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Surveyor Name");
        } else {

            createSurveyingHashMap.put("action", RequestBody.create("create_surveying", MediaType.parse("text/plain")));
            createSurveyingHashMap.put("project_id", RequestBody.create(projectId, MediaType.parse("text/plain")));
            createSurveyingHashMap.put("date", RequestBody.create(str_SurveyDate, MediaType.parse("text/plain")));
            createSurveyingHashMap.put("country", RequestBody.create(binding.etCountryName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("state", RequestBody.create(binding.etState.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("district", RequestBody.create(binding.etDistrict.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("village", RequestBody.create(binding.etVillage.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("survey_no", RequestBody.create(binding.etLandSurveyNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("old_tree", RequestBody.create(binding.etTreeNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("new_tree", RequestBody.create(binding.etNewTreeNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("botanical_tree", RequestBody.create(binding.etBotanicalTreeName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("local_tree", RequestBody.create(binding.etLocalTreeName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("health_status", RequestBody.create(binding.etHealthStatus.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("grith_size", RequestBody.create(binding.etSizeOfGirth.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("height_tree", RequestBody.create(binding.etHeightOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("age_tree", RequestBody.create(binding.etAgeOfTheTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("surveyor_name", RequestBody.create(binding.etSurveyorName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("aprx_value", RequestBody.create(binding.etAValueOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("approx_val_per_kg", RequestBody.create(binding.etValueOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("location_name", RequestBody.create(binding.etLocationText.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("contractor_name", RequestBody.create(binding.etContractorName.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("land_owner_name", RequestBody.create(binding.etNameLand.getText().toString().trim(), MediaType.parse("text/plain")));
            createSurveyingHashMap.put("land_survey_number", RequestBody.create(binding.etLandSurveyNo.getText().toString().trim(), MediaType.parse("text/plain")));

            getCheckSurveyNoHashMap.put("action", "check_survey_no");
            getCheckSurveyNoHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            getCheckSurveyNoHashMap.put("project_id", projectId);
            getCheckSurveyNoHashMap.put("new_tree_no", binding.etNewTreeNo.getText().toString().trim());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                surveyingPresenter.getCheckSurveyNo(getCheckSurveyNoHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void openRFIDReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment("Is RFID Available ?", this);
        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    private void openGPSReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
//        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(GPSDeviceList, this);
        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(GPSSelectedBluetooth, this);
        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(fragmentManager, "GPS_reading_sheet");
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
//            moveToFragment(new SurveyingSecondFragment(createSurveyingHashMap), android.R.id.content, false);

            if (bottomSheetDialogFragment != null) {
                bottomSheetDialogFragment.dismiss();
            }

            bottomSheetDialogFragment = new RFIDReadingBottomSheetFragment(this);
            bottomSheetDialogFragment.setCancelable(false);
            bottomSheetDialogFragment.show(fragmentManager, "RFID_reading_sheet");
        } else {

            createSurveyingHashMap.put("rfid_code", RequestBody.create("not available", MediaType.parse("text/plain")));

            openGPSReader();
        }
    }

    @Override
    public void onSuccessListProjects(ListProjectsModel listProjectsModel) {
        commonFunction.dismissLoader();
        if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
            this.listProjectsModel = listProjectsModel;
        }
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListModel = operatorListModel;
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }

        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(GPSSelectedBluetooth));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCheckSurveyNo(CommonModel model) {
        commonFunction.dismissLoader();
        openRFIDReader();
        //moveToFragment(new SurveyingSecondFragment(createSurveyingHashMap), android.R.id.content, false);
    }

    @Override
    public void onErrorCheckSurveyNo(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
        binding.etNewTreeNo.requestFocus();
    }

    @Override
    public void onSuccessCreateSurveying(CommonModel model) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCreateSurveying(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
        if (countryListModel != null && countryListModel.getResult() != null && !countryListModel.getResult().isEmpty()) {
            this.countryListResultModel = countryListModel.getResult();;
        }
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessTreeManagementList(TreeManagementListModel treeManagementListModel) {
        commonFunction.dismissLoader();
        if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
            this.treeManagementListModel = treeManagementListModel;
        }
    }

    @Override
    public void onErrorTreeManagementList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onProjectClick(int position) {
        binding.etProjectName.setText(listProjectsModel.getResult().get(position).getProjectName());
        projectId = listProjectsModel.getResult().get(position).getId();
        binding.etCountryName.setText(listProjectsModel.getResult().get(position).getCountry());
        binding.etState.setText(listProjectsModel.getResult().get(position).getState());
        binding.etDistrict.setText(listProjectsModel.getResult().get(position).getDistrict());
        binding.etVillage.setText(listProjectsModel.getResult().get(position).getVillage());
        binding.etLocationText.setText(listProjectsModel.getResult().get(position).getLocation());
    }


    @Override
    public void getRFIDTagData(String tagData) {
        Timber.i("getRFIDTagData: %s", tagData);

        createSurveyingHashMap.put("rfid_code", RequestBody.create(tagData, MediaType.parse("text/plain")));

        openGPSReader();
//        moveToFragment(new SurveyingSecondFragment(createSurveyingHashMap), android.R.id.content, false);
    }

    @Override
    public void getGPSTagData(boolean isRefresh, String latitude, String longitude) {

        if (isRefresh) {
            openGPSReader();
        } else {
//            commonFunction.showLongToast("LAT: " + latitude + "\nLON: " + longitude);

            createSurveyingHashMap.put("latitude", RequestBody.create(latitude, MediaType.parse("text/plain")));
            createSurveyingHashMap.put("longitude", RequestBody.create(longitude, MediaType.parse("text/plain")));

            moveToFragment(new SurveyingSecondFragment(createSurveyingHashMap), android.R.id.content, false);
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void CloseActivityEventBus(CloseFragmentEvent closeFragmentEvent) {
        if (closeFragmentEvent.closeFragment.equalsIgnoreCase("surveying_first")) {
            fragmentManager.popBackStackImmediate();
        }
        eventBus.removeStickyEvent(closeFragmentEvent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (eventBus.isRegistered(this)) {
                eventBus.unregister(this);
            }
            if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
                bottomSheetDialogFragment.dismiss();
            }
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        surveyingPresenter.onDispose();
    }

    @Override
    public void onCountryClick(int position) {
        binding.etCountryName.setText(countryListResultModel.get(position).getName());
        str_CountryType = countryListResultModel.get(position).getName();
    }

    @Override
    public void onCurrencyNameClick(int position) {
        binding.etAValueOfTree.setText(currencyNameModel.get(position).getCurrencyName());
        str_CurrencyName = currencyNameModel.get(position).getCurrencyName();
    }

    @Override
    public void onOperatorClick(int position) {
        binding.etSurveyorName.setText(operatorListModel.getResult().get(position).getName());
        operatorId = operatorListModel.getResult().get(position).getId();
    }

    @Override
    public void onTreeClick(int position) {
        binding.etLocalTreeName.setText(treeManagementListModel.getResult().get(position).getTreeName());
        binding.etBotanicalTreeName.setText(treeManagementListModel.getResult().get(position).getBotanicalName());
    }
}