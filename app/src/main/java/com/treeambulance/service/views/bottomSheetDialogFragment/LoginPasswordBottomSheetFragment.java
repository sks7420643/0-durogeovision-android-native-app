package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentLoginPasswordBottomSheetBinding;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class LoginPasswordBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener {

    public HashMap<String, String> loginPasswordHashMap;
    FragmentLoginPasswordBottomSheetBinding binding;
    LoginPasswordOnClickListener loginPasswordOnClickListener;
    String str_action = "";

    public LoginPasswordBottomSheetFragment() {
        // Required empty public constructor
    }

    public LoginPasswordBottomSheetFragment(String action, LoginPasswordOnClickListener loginPasswordOnClickListener) {
        this.str_action = action;
        this.loginPasswordOnClickListener = loginPasswordOnClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_password_bottom_sheet, container, false);

        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loginPasswordHashMap = new HashMap<>();

        if (str_action.equalsIgnoreCase("supervisor_mode")) {

            binding.tvTitle.setText("Supervisor Mode");

            loginPasswordHashMap.put("action", "supervisor_login");
            loginPasswordHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        } else if (str_action.equalsIgnoreCase("durotek_mode")) {

            binding.tvTitle.setText("Durotek Mode");

            loginPasswordHashMap.put("action", "admin_login");
            loginPasswordHashMap.put("user_name", "admin");
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btSubmit: {
                if (commonFunction.NullPointerValidator(binding.edtPassword.getText().toString().trim())) {
                    loginPasswordHashMap.put("password", binding.edtPassword.getText().toString().trim());
                    loginPasswordOnClickListener.loginPasswordClickListener(str_action, loginPasswordHashMap);
                    dismiss();
                } else {
                    commonFunction.showLongToast("Please Enter Password");
                }
            }
            break;
        }
    }

    public interface LoginPasswordOnClickListener {
        public void loginPasswordClickListener(String action, HashMap<String, String> loginPasswordHashMap);
    }
}