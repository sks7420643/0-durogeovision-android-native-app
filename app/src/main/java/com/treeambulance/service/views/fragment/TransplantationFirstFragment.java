package com.treeambulance.service.views.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.bluetoothReader.BluetoothChatServiceForContinuosData;
import com.treeambulance.service.databinding.FragmentTransplantationFirstBinding;
import com.treeambulance.service.eventBus.CloseFragmentEvent;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.CurrencyNameModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.model.SurveyDetailsModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.other.AppLocationService;
import com.treeambulance.service.presenter.TransplantationPresenter;
import com.treeambulance.service.utility.Constants;
import com.treeambulance.service.views.adapter.StateListAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllProjectsBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CityListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CountryListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CurrencyNameBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.GPSReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.OperatorListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.RFIDReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StateListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.TreeListBottomSheetFragment;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

public class TransplantationFirstFragment extends BaseFragment implements View.OnClickListener, CountryListBottomSheetFragment.CountryListClickListener,
        StateListBottomSheetFragment.OnStateClickListener, CityListBottomSheetFragment.OnCityClickListener,OperatorListBottomSheetFragment.OperatorListClickListener,CurrencyNameBottomSheetFragment.OnCurrencyNameClickListener,
        CustomDialogBottomSheetFragment.CustomDialogOnButtonClick, TransplantationPresenter.ContactInterface, AllProjectsBottomSheetFragment.OnProjectClickListener, RFIDReadingBottomSheetFragment.RFIDReading, GPSReadingBottomSheetFragment.GPSReading, TreeListBottomSheetFragment.TreeClickListener {

    private final Calendar myCalendar = Calendar.getInstance();
    private final DatePickerDialog.OnDateSetListener a_date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            /*str_TransplantationDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            binding.etTransplantationDate.setText(commonFunction.getReverseDate(str_TransplantationDate));*/
        }
    };
    private FragmentTransplantationFirstBinding binding;
    private ArrayList<StateCityModel> stateCityModel;
    private ArrayList<String> cityModel, GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private ListProjectsModel listProjectsModel;
    private OperatorListModel operatorListModel;
    private CompanyDetailsModel companyDetailsModel;
    private TreeManagementListModel treeManagementListModel;
    private TransplantationPresenter transplantationPresenter;
    private HashMap<String, String> allProjectsHashMap, getOperatorListHashMap, getTreeManagementListHashMap,getCompanyDetailsHashMap, getCheckTreeNoHashMap,getCountryListHashMap;
    private ArrayList<CountryListModel.Result> countryListResultModel;
    private HashMap<String, RequestBody> createTransplantationHashMap;
    private String str_TransplantationDate = "", projectId = "", surveyId = "", operatorId = "", str_CountryType="";
    private String latitude = "0.0", longitude = "0.0", distance = "0";
    private double latd,lngd;
    private Set<BluetoothDevice> setBluetoothDevice;
    private LocationManager locationManager;
    private Context mContext;
    AppLocationService appLocationService;
    private String state="",district="",village="",country="";
    private String mConnectedDeviceName = "";
    private ArrayList<CurrencyNameModel> currencyNameModel;
    private BluetoothChatServiceForContinuosData mChatService = null;
    //    private ArrayAdapter<String> mConversationArrayAdapter;
    private int counter = 0;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean secure;
    private GPSReadingBottomSheetFragment.GPSReading gpsReading;
    public TransplantationFirstFragment() {
        // Required empty public constructor
    }

    private final Handler mHandler1 = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
//            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
//                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTING:
//                            setStatus(String.valueOf(R.string.title_connecting));
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_LISTEN:
                        case BluetoothChatServiceForContinuosData.STATE_NONE:
//                            setStatus(String.valueOf(R.string.title_not_connected));
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
//                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_LONG).show();
                    if (mBluetoothAdapter.isEnabled()) {
                        sendToProcessMessage(readMessage);
                    }else{
                        commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Bluetooth Connection Failed");
                    }

//                    mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    commonFunction.showShortToast("Connected to " + mConnectedDeviceName);
                    break;
                case Constants.MESSAGE_TOAST:
                    if (msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Unable to connect device") || msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Device connection was lost")) {
                        counter++;
                        Timber.i("");
                        if (counter < GPSDeviceList.size()) {
                            attempt();
                        }
                    }
                    commonFunction.showShortToast("GPS Reading is in Progress... Attempt: " + counter);
                    if (counter > GPSDeviceList.size()) {

                        commonFunction.showShortToast(msg.getData().getString(Constants.TOAST));
                        commonFunction.showShortToast("Bluetooth Connection Failed");
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_transplantation_first, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stateCityModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        GPSDeviceList = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();
        listProjectsModel = new ListProjectsModel();
        allProjectsHashMap = new HashMap<>();
        getTreeManagementListHashMap = new HashMap<>();
        getOperatorListHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        getCheckTreeNoHashMap = new HashMap<>();
        createTransplantationHashMap = new HashMap<>();
        countryListResultModel = new ArrayList<>();
        getCountryListHashMap = new HashMap<>();
        treeManagementListModel = new TreeManagementListModel();
        currencyNameModel = new ArrayList<>();
        mChatService = new BluetoothChatServiceForContinuosData(context, mHandler1);

        Timber.i("GPSDeviceList: %s", new Gson().toJson(GPSDeviceList));

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        // Attempt to connect to the device
        secure = false;



        transplantationPresenter = new TransplantationPresenter(this);
        loadJSONFromAsset();

        binding.etTransplantationDate.setText(commonFunction.getCurrentDate()[0]);
        str_TransplantationDate = commonFunction.getCurrentDate()[1];
        mContext = getActivity();
//        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }

//        appLocationService = new AppLocationService(
//                mContext);
//
//        Location gpsLocation = appLocationService
//                .getLocation(LocationManager.GPS_PROVIDER,mContext);


//        else {
//            commonFunction.showSettingsAlert(mContext);
//        }


        addBluetoothItemsToList();
        hitGetAllProjects();
        hitGetOperatorList();
        hitGetCompanyDetails();
        hitGetCountryList();
        hitGetTreeManagementList();
//
    }

    private void hitGetCountryList(){
        getCountryListHashMap.put("action", "country_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            transplantationPresenter.getCountryList(getCountryListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void attempt() {
        if (GPSDeviceList != null && !GPSDeviceList.isEmpty()) {
            Timber.i("attempt: counter " + counter + " - " + GPSDeviceList.get(counter));
            BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(GPSDeviceList.get(counter));
            mChatService.connect(device1, secure);
        }
    }

    private void sendToProcessMessage(String readMessage) {
        String[] lineSeparatedArray = readMessage.split("\n");
        if (lineSeparatedArray.length > 1) {
            //Check if we have lan lat in our line
            for (int i = 0; i < lineSeparatedArray.length; i++) {
                String[] splitByGNRMC = lineSeparatedArray[i].split("GNRMC");
                if (splitByGNRMC.length > 1) {
                    try {
                        Timber.i("splitByGNRMC: " + splitByGNRMC[1].split(",")[3] + "\t" + splitByGNRMC[1].split(",")[5]);
                        double lat = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[3]));
                        double lon = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[5]));
                        Timber.i("lon: " + lon + "\t lat: " + lat);
                        latd = lat;
                        lngd = lon;
                        if (latd != 0.0 && lngd != 0.0) {
                            double latitude = latd;
                            double longitude = lngd;

                            List<Address> addresses = commonFunction.getAddress((Activity) mContext,latitude,longitude);
                            Address address = addresses.get(0);
                            district = address.getLocality();
                            state = address.getAdminArea();
                            country = address.getCountryName();

                            StringBuilder sb = new StringBuilder();
                            if (address.getSubThoroughfare() != null){
                                sb.append(address.getSubThoroughfare()).append(",");
                            }
                            if (address.getThoroughfare() != null){
                                sb.append(address.getThoroughfare()).append(",");
                            }
                            if (address.getSubLocality() != null){
                                sb.append(address.getSubLocality());
                            }
                            village = sb.toString();
                            binding.etVillage.setText(village);

                            if (stateCityModel != null && !stateCityModel.isEmpty()) {
                                cityModel = new ArrayList<>();
                                for (int j = 0; j < stateCityModel.size(); j++) {
                                    cityModel.addAll(stateCityModel.get(j).getDistricts());
                                    if(stateCityModel.get(j).getState().toLowerCase().equals(state.toLowerCase())){
                                        binding.etState.setText(state);
                                        break;
                                    }
                                }
                            }
                            if (cityModel != null && !cityModel.isEmpty()) {
                                for (int j = 0; j < cityModel.size(); j++) {
                                    if(cityModel.get(j).toLowerCase().equals(district.toLowerCase())){
                                        binding.etDistrict.setText(district);
                                        break;
                                    }
                                }
                            }
                            binding.etCountryName.setText(country);

//
                        }
                        latitude = String.valueOf(lat);
                        longitude = String.valueOf(lon);

                        if (mBluetoothAdapter.isEnabled()) {
                            mChatService.stop();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Timber.e("sendToProcessMessage: splitData - %s", e.getMessage());
                    }
                }
                Timber.i("sendToProcessMessage: lineSeparatedArray - " + lineSeparatedArray[i] + "\t" + i);
            }
        }
    }

    private double decimalToDMS(double value) {
        Timber.i("decimalToDMS: value %s", value);
        int degrees = (int) (value / 100);
        Timber.i("decimalToDMS: degrees %s", degrees);
        double minutes = (value - degrees * 100) / 60;
        Timber.i("decimalToDMS: minutes %s", minutes);
        return degrees + minutes;
    }

    private void hitGetAllProjects() {
        allProjectsHashMap.put("action", "list_projects");
        allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            transplantationPresenter.getListProjects(allProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetTreeManagementList() {

        getTreeManagementListHashMap.put("action", "list_trees");
        getTreeManagementListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            transplantationPresenter.getTreeManagementList(getTreeManagementListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            transplantationPresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            transplantationPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/states_and_districts.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray));

        String str_response = new Gson().toJson(jsonArray);

        try {
            Type type = new TypeToken<ArrayList<StateCityModel>>() {
            }.getType();

            stateCityModel = new Gson().fromJson(str_response, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(stateCityModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }

        JsonArray jsonArray1 = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/currency_name.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray currencyNameModel: %s", new Gson().toJson(jsonArray1));

        String str_response1 = new Gson().toJson(jsonArray1);

        try {
            Type type = new TypeToken<ArrayList<CurrencyNameModel>>() {
            }.getType();

            currencyNameModel = new Gson().fromJson(str_response1, type);

            Timber.i("loadJSONFromAsset: currencyNameModel: %s", new Gson().toJson(currencyNameModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: currencyNameModel: %s", e.getMessage());
        }
    }

    private void openRFIDReader() {

        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment("Is RFID Available ?", this);
        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    private void openGPSReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
//        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(GPSDeviceList, this);
        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(GPSSelectedBluetooth, this);
        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(fragmentManager, "GPS_reading_sheet");
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(projectId) || !commonFunction.NullPointerValidator(binding.etBotanicalTreeName.getText().toString().trim()) ||
                !commonFunction.NullPointerValidator(binding.etLocalTreeName.getText().toString().trim()) ||
                !commonFunction.NullPointerValidator(binding.etSizeOfGirth.getText().toString().trim()) ||
                !commonFunction.NullPointerValidator(binding.etAgeOfTheTree.getText().toString().trim())) {
            binding.etProjectName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Fetch Survey Details");
        } else if (!commonFunction.NullPointerValidator(binding.etNewTreeNo.getText().toString().trim())) {
            binding.etNewTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter New Tree No.");
        } else if (!commonFunction.NullPointerValidator(binding.etTransplantationDate.getText().toString().trim())) {
            binding.etTransplantationDate.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Transplantation Date");
        } else if (!commonFunction.NullPointerValidator(binding.etCountryName.getText().toString().trim())) {
            binding.etCountryName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Country");
        } else if (!commonFunction.NullPointerValidator(binding.etState.getText().toString().trim())) {
            binding.etState.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select State");
        } else if (!commonFunction.NullPointerValidator(binding.etDistrict.getText().toString().trim())) {
            binding.etDistrict.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select District");
        } else if (!commonFunction.NullPointerValidator(binding.etVillage.getText().toString().trim())) {
            binding.etVillage.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Village Name");
        } else if (!commonFunction.NullPointerValidator(binding.etLocationText.getText().toString().trim())) {
            binding.etLocationText.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Location Name");
        }  else if (!commonFunction.NullPointerValidator(binding.etAValueOfTree.getText().toString().trim())) {
            binding.etAValueOfTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Approximate value of the Tree");
        }else if (!commonFunction.NullPointerValidator(binding.etContractorName.getText().toString().trim())) {
            binding.etContractorName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Operator Name");
        } else {

            createTransplantationHashMap.put("action", RequestBody.create("create_transplantation", MediaType.parse("text/plain")));
            createTransplantationHashMap.put("project_id", RequestBody.create(projectId, MediaType.parse("text/plain")));
            createTransplantationHashMap.put("new_tree_no", RequestBody.create(binding.etNewTreeNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("date", RequestBody.create(str_TransplantationDate, MediaType.parse("text/plain")));
            createTransplantationHashMap.put("country", RequestBody.create(binding.etCountryName.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("new_state", RequestBody.create(binding.etState.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("new_district", RequestBody.create(binding.etDistrict.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("new_village", RequestBody.create(binding.etVillage.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("botanical_tree", RequestBody.create(binding.etBotanicalTreeName.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("local_tree", RequestBody.create(binding.etLocalTreeName.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("grith_size", RequestBody.create(binding.etSizeOfGirth.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("age_tree", RequestBody.create(binding.etAgeOfTheTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("new_operator_name", RequestBody.create(binding.etSurveyorName.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("land_owner_name", RequestBody.create(binding.etNameLand.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("old_tree", RequestBody.create(binding.etTreeNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("land_survey_number", RequestBody.create(binding.etLangSurveyNo.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("height_tree", RequestBody.create(binding.etHeightOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("aprx_new_value", RequestBody.create(binding.etAValueOfTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("location_name", RequestBody.create(binding.etLocationText.getText().toString().trim(), MediaType.parse("text/plain")));
            createTransplantationHashMap.put("contractor_name", RequestBody.create(binding.etContractorName.getText().toString().trim(), MediaType.parse("text/plain")));

            getCheckTreeNoHashMap.put("action", "check_tree_no");
            getCheckTreeNoHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            getCheckTreeNoHashMap.put("project_id", projectId);
            getCheckTreeNoHashMap.put("new_tree_no", binding.etNewTreeNo.getText().toString().trim());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                transplantationPresenter.getCheckTreeNo(getCheckTreeNoHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void validatorGetSurveyDetails() {
        if (!commonFunction.NullPointerValidator(binding.etProjectName.getText().toString().trim())) {
            binding.etProjectName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Project Name");
        } else if (!commonFunction.NullPointerValidator(binding.etNewTreeNo.getText().toString().trim())) {
            binding.etNewTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Survey No");
        } else {

            allProjectsHashMap.put("action", "get_survey_details");
            allProjectsHashMap.put("project_id", projectId);
            allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            allProjectsHashMap.put("new_tree_no", binding.etNewTreeNo.getText().toString().trim());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                transplantationPresenter.getSurveyDetailsProjects(allProjectsHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackTransplantation1:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.etProjectName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new AllProjectsBottomSheetFragment(listProjectsModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "all_project_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etCountryName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (countryListResultModel != null && !countryListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new CountryListBottomSheetFragment(countryListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "country_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            /*case R.id.etTransplantationDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, *//*R.style.datepicker,*//* a_date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 30000);
                datePickerDialog.show();
                break;*/
            case R.id.etLocalTreeName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new TreeListBottomSheetFragment(treeManagementListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the Local Tree Name");
                }
                break;
            case R.id.etBotanicalTreeName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new TreeListBottomSheetFragment(treeManagementListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the Botanical Tree Name");
                }
                break;
            case R.id.etState:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etDistrict:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }
                break;
            case R.id.etSurveyorName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new OperatorListBottomSheetFragment(operatorListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }
                break;
            case R.id.etAValueOfTree:
//                if (currencyNameModel != null && !currencyNameModel.isEmpty()) {
//                    bottomSheetDialogFragment = new CurrencyNameBottomSheetFragment(currencyNameModel, this);
//                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
//                } else {
//                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
//                }
            case R.id.btSubmit:
                validatorGetSurveyDetails();
                break;
            case R.id.btNextPage:
                validator();
                break;
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        for (BluetoothDevice bluetoothDevice : list) {

            GPSFromBluetooth.add(bluetoothDevice.getAddress());
        }

        Timber.i("addBluetoothItemsToList: GPSFromBluetooth: %s", new Gson().toJson(GPSFromBluetooth));
    }

    @Override
    public void onStateClick(int position) {
        cityModel = new ArrayList<>();
        binding.etState.setText(stateCityModel.get(position).getState());

        cityModel.addAll(stateCityModel.get(position).getDistricts());
        Timber.i("onStateClick %s", new Gson().toJson(cityModel));
    }

    @Override
    public void onCityClick(int position) {
        binding.etDistrict.setText(cityModel.get(position));
        Timber.i("onCityClick %s", new Gson().toJson(cityModel.get(position)));
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
//            moveToFragment(new SurveyingSecondFragment(createSurveyingHashMap), android.R.id.content, false);

            if (bottomSheetDialogFragment != null) {
                bottomSheetDialogFragment.dismiss();
            }

            bottomSheetDialogFragment = new RFIDReadingBottomSheetFragment(this);
            bottomSheetDialogFragment.setCancelable(false);
            bottomSheetDialogFragment.show(fragmentManager, "RFID_reading_sheet");
        } else {

            createTransplantationHashMap.put("rfid_code", RequestBody.create("not available", MediaType.parse("text/plain")));

            openGPSReader();
        }
    }

    @Override
    public void onSuccessListProjects(ListProjectsModel listProjectsModel) {
        commonFunction.dismissLoader();

        if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
            this.listProjectsModel = listProjectsModel;
        }
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessSurveyDetailsProjects(SurveyDetailsModel surveyDetailsModel) {
        commonFunction.dismissLoader();
        projectId = surveyDetailsModel.getResult().getProjectId();
        surveyId = surveyDetailsModel.getResult().getSurveyNo();
        binding.etAValueOfTree.setText(surveyDetailsModel.getResult().getAppValue());
        binding.etHeightOfTree.setText(surveyDetailsModel.getResult().getHeightTree());
        binding.etLocationText.setText(surveyDetailsModel.getResult().getLocationName());
//        binding.etCountryName.setText(surveyDetailsModel.getResult().getCountry());
//        binding.etState.setText(surveyDetailsModel.getResult().getState());
//        binding.etVillage.setText(surveyDetailsModel.getResult().getVillage());
//        binding.etDistrict.setText(surveyDetailsModel.getResult().getDistrict());
        binding.etNameLand.setText(surveyDetailsModel.getResult().getNameLand());
        binding.etTreeNo.setText(surveyDetailsModel.getResult().getOldTree());
        binding.etNewTreeNo.setText(surveyDetailsModel.getResult().getNewTree());
//        binding.etContractorName.setText(surveyDetailsModel.getResult().getContractorName());
        binding.etLangSurveyNo.setText(surveyDetailsModel.getResult().getLanSurveyNumber());
        binding.etBotanicalTreeName.setText(surveyDetailsModel.getResult().getBotanicalName());
        binding.etLocalTreeName.setText(surveyDetailsModel.getResult().getLocalName());
        binding.etSizeOfGirth.setText(surveyDetailsModel.getResult().getGrithSize());
        binding.etAgeOfTheTree.setText(surveyDetailsModel.getResult().getTreeAge());
        binding.etNewTreeNo.setText(surveyDetailsModel.getResult().getNewTree());
//        binding.etSurveyorName.setText(surveyDetailsModel.getResult().getOperatorName());
        binding.etTransplantationDate.setText(surveyDetailsModel.getResult().getDate());

        latitude = surveyDetailsModel.getResult().getLatitude();
        longitude = surveyDetailsModel.getResult().getLongitude();
    }

    @Override
    public void onErrorSurveyDetailsProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListModel = operatorListModel;
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        Timber.i("onSuccessCompanyDetails: companyDetailsModel - %s", new Gson().toJson(companyDetailsModel));
        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }
        if (GPSSelectedBluetooth != null && !GPSSelectedBluetooth.isEmpty()) {
            attempt();
        }
        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(GPSSelectedBluetooth));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCheckTreeNo(CommonModel model) {
        commonFunction.dismissLoader();
        openRFIDReader();
        //moveToFragment(new TransplantationSecondFragment(createTransplantationHashMap), android.R.id.content, false);
    }

    @Override
    public void onErrorCheckTreeNo(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCreateTransplantation(CommonModel model) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorCreateTransplantation(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
        if (countryListModel != null && countryListModel.getResult() != null && !countryListModel.getResult().isEmpty()) {
            this.countryListResultModel = countryListModel.getResult();;
        }
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessTreeManagementList(TreeManagementListModel treeManagementListModel) {
        commonFunction.dismissLoader();
        if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
            this.treeManagementListModel = treeManagementListModel;
        }
    }

    @Override
    public void onErrorTreeManagementList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void getRFIDTagData(String tagData) {
        Timber.i("getRFIDTagData: %s", tagData);

        createTransplantationHashMap.put("rfid_code", RequestBody.create(tagData, MediaType.parse("text/plain")));

        openGPSReader();
    }

    @Override
    public void getGPSTagData(boolean isRefresh, String latitude, String longitude) {

        if (isRefresh) {
            openGPSReader();
        } else {
            commonFunction.showLongToast("LAT: " + latitude + "\nLON: " + longitude);

            createTransplantationHashMap.put("latitude", RequestBody.create(latitude, MediaType.parse("text/plain")));
            createTransplantationHashMap.put("longitude", RequestBody.create(longitude, MediaType.parse("text/plain")));

            distance = commonFunction.getDistance(Double.parseDouble(latitude), Double.parseDouble(longitude), Double.parseDouble(this.latitude), Double.parseDouble(this.longitude));

            Timber.i("getGPSTagData: %s", distance);

            createTransplantationHashMap.put("distance", RequestBody.create(distance, MediaType.parse("text/plain")));
            moveToFragment(new TransplantationSecondFragment(createTransplantationHashMap), android.R.id.content, false);
        }
    }

    @Override
    public void onProjectClick(int position) {
        binding.etProjectName.setText(listProjectsModel.getResult().get(position).getProjectName());
        projectId = listProjectsModel.getResult().get(position).getId();
        /*binding.etState.setText(listProjectsModel.getResult().get(position).getState());
        binding.etDistrict.setText(listProjectsModel.getResult().get(position).getDistrict());
        binding.etVillage.setText(listProjectsModel.getResult().get(position).getVillage());*/
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void CloseActivityEventBus(CloseFragmentEvent closeFragmentEvent) {
        if (closeFragmentEvent.closeFragment.equalsIgnoreCase("transplantation_first")) {
            fragmentManager.popBackStackImmediate();
        }
        eventBus.removeStickyEvent(closeFragmentEvent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!eventBus.isRegistered(this)) {
            eventBus.register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (eventBus.isRegistered(this)) {
                eventBus.unregister(this);
            }
            if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
                bottomSheetDialogFragment.dismiss();
            }
        } catch (Exception e) {
            Timber.e(e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        transplantationPresenter.onDispose();
    }

    @Override
    public void onCountryClick(int position) {
        binding.etCountryName.setText(countryListResultModel.get(position).getName());
        str_CountryType = countryListResultModel.get(position).getName();
    }

    @Override
    public void onTreeClick(int position) {
        binding.etLocalTreeName.setText(treeManagementListModel.getResult().get(position).getTreeName());
        binding.etBotanicalTreeName.setText(treeManagementListModel.getResult().get(position).getBotanicalName());
    }

    @Override
    public void onOperatorClick(int position) {
        binding.etSurveyorName.setText(operatorListModel.getResult().get(position).getName());
        operatorId = operatorListModel.getResult().get(position).getId();
    }

    @Override
    public void onCurrencyNameClick(int position) {
        binding.etAValueOfTree.setText(currencyNameModel.get(position).getCurrencyName());
    }
}