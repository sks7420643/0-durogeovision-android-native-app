package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterListBinding;
import com.treeambulance.service.databinding.AdapterPlantationListBinding;
import com.treeambulance.service.model.PlantationListModel;

import java.util.ArrayList;

public class PlantationProjectListAdapter extends RecyclerView.Adapter<PlantationProjectListAdapter.ViewHolder> {

    private final ArrayList<PlantationListModel.Result> plantationListModel;
    private PlantationClickListener onPlantationClickListener;
    public PlantationProjectListAdapter(ArrayList<PlantationListModel.Result> plantationListModel, PlantationClickListener onPlantationClickListener) {
        this.plantationListModel = plantationListModel;
        this.onPlantationClickListener = onPlantationClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_plantation_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setModel(plantationListModel.get(position));
        holder.binding.ivEdit.setOnClickListener(view -> onPlantationClickListener.plantationOnClick(position, "edit"));
        holder.binding.ivDelete.setOnClickListener(view -> onPlantationClickListener.plantationOnClick(position, "delete"));
    }

    public interface PlantationClickListener {
        public void plantationOnClick(int position, String action);
    }

    @Override
    public int getItemCount() {
        return plantationListModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public AdapterPlantationListBinding binding;
        public ViewHolder(AdapterPlantationListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
