package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentReportTypeBottomSheetBinding;
import com.treeambulance.service.databinding.FragmentReportsBottomSheetBinding;
import com.treeambulance.service.databinding.FragmentStateListBottomSheetBinding;
import com.treeambulance.service.model.ReportTypeModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.views.adapter.ReportTypeAdapter;
import com.treeambulance.service.views.adapter.StateListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ReportTypeBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentReportTypeBottomSheetBinding binding;
    private ArrayList<ReportTypeModel> reportTypeModel = new ArrayList<>();
    private OnReportTypeClickListener onReportTypeClickListener;

    public ReportTypeBottomSheetFragment(ArrayList<ReportTypeModel> reportTypeModel, OnReportTypeClickListener onReportTypeClickListener) {
        // Required empty public constructor
        this.reportTypeModel = reportTypeModel;
        this.onReportTypeClickListener = onReportTypeClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_report_type_bottom_sheet,
                container,
                false
        );
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvReportTye.setAdapter(new ReportTypeAdapter(reportTypeModel, this));
    }

    @Override
    public void onItemClick(int position) {
        onReportTypeClickListener.onReportTypeClick(position);
        dismiss();
    }

    public interface OnReportTypeClickListener {
        public void onReportTypeClick(int position);
    }
}