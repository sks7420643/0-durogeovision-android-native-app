package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterBluetoothDeviceBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;

import java.util.ArrayList;

public class BluetoothDeviceAdapter extends RecyclerView.Adapter<BluetoothDeviceAdapter.ViewHolder> {

    ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModels = new ArrayList<>();
    OnBluetoothDeviceSelected onBluetoothDeviceSelected;

    public BluetoothDeviceAdapter(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModels, OnBluetoothDeviceSelected onBluetoothDeviceSelected) {
        this.bluetoothDeviceListModels = bluetoothDeviceListModels;
        this.onBluetoothDeviceSelected = onBluetoothDeviceSelected;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_bluetooth_device, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.setModel(bluetoothDeviceListModels.get(position));
        holder.binding.cbItem.setChecked(bluetoothDeviceListModels.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (bluetoothDeviceListModels.get(position).isChecked()) {
                onBluetoothDeviceSelected.onSelected(position, false);
            } else if (!bluetoothDeviceListModels.get(position).isChecked()) {
                onBluetoothDeviceSelected.onSelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bluetoothDeviceListModels.size();
    }

    public interface OnBluetoothDeviceSelected {
        public void onSelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final AdapterBluetoothDeviceBinding binding;

        public ViewHolder(@NonNull AdapterBluetoothDeviceBinding adapterBluetoothDeviceBinding) {
            super(adapterBluetoothDeviceBinding.getRoot());
            this.binding = adapterBluetoothDeviceBinding;

        }
    }
}
