package com.treeambulance.service.views.fragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentCreateCompanyBinding;
import com.treeambulance.service.eventBus.CloseFragmentEvent;
import com.treeambulance.service.model.BluetoothDeviceListModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CreateCompanyModel;
import com.treeambulance.service.presenter.CreateCompanyPresenter;
import com.treeambulance.service.views.adapter.SelectedBluetoothDeviceAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.BluetoothDeviceBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class CreateCompanyFragment extends BaseFragment implements View.OnClickListener,
        BluetoothDeviceBottomSheetFragment.AddSelectedDrives, CreateCompanyPresenter.ContactInterface {

    private static final int REQUEST_ENABLE_BT = 1;
    private FragmentCreateCompanyBinding binding;
    private BluetoothAdapter bluetoothAdapter;
    private SelectedBluetoothDeviceAdapter selectedRFIDDeviceAdapter;
    private SelectedBluetoothDeviceAdapter selectedGPSDeviceAdapter;
    private Set<BluetoothDevice> setBluetoothDevice;
    private ArrayList<BluetoothDeviceListModel> rfidBluetoothDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> gpsBluetoothDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> rfidSelectedDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> gpsSelectedDeviceModelsList;
    private HashMap<String, RequestBody> createCompanyHashMap;
    private String onSelected = "";
    private String selected = "",imgPath1stTree;
    private CreateCompanyPresenter createCompanyPresenter;

    public CreateCompanyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_company, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }
        setBluetoothDevice = bluetoothAdapter.getBondedDevices();

        rfidBluetoothDeviceModelsList = new ArrayList<>();
        gpsBluetoothDeviceModelsList = new ArrayList<>();
        rfidSelectedDeviceModelsList = new ArrayList<>();
        gpsSelectedDeviceModelsList = new ArrayList<>();

        createCompanyHashMap = new HashMap<>();

        Timber.i("onViewCreated: setBluetoothDevice: %s", new Gson().toJson(setBluetoothDevice));

        createCompanyPresenter = new CreateCompanyPresenter(this);

        addBluetoothItemsToList();
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        rfidBluetoothDeviceModelsList.add(new BluetoothDeviceListModel("", 0, "Not Available", 0, false));
        /*gpsBluetoothDeviceModelsList.add(new BluetoothDeviceListModel("", 0, "Not Available", 0, false));*/

        for (BluetoothDevice bluetoothDevice : list) {
            rfidBluetoothDeviceModelsList.add(new BluetoothDeviceListModel(bluetoothDevice.getAddress(), bluetoothDevice.getBondState(), bluetoothDevice.getName(), bluetoothDevice.getType(), false));
            gpsBluetoothDeviceModelsList.add(new BluetoothDeviceListModel(bluetoothDevice.getAddress(), bluetoothDevice.getBondState(), bluetoothDevice.getName(), bluetoothDevice.getType(), false));
        }

        Timber.i("addBluetoothItemsToList: bluetoothDeviceModelsList: %s", new Gson().toJson(rfidBluetoothDeviceModelsList));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackCreateCompany: {
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.ivLogoName:
                selected = "logo";
                selectImage();
                break;
            case R.id.etAddRFIDDevice: {

                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (rfidBluetoothDeviceModelsList != null && !rfidBluetoothDeviceModelsList.isEmpty()) {
                    onSelected = "rfid";
                    bottomSheetDialogFragment = new BluetoothDeviceBottomSheetFragment(rfidBluetoothDeviceModelsList, this);
                    bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Pair with RFID Scanner" + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.etAddGPSDevice: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (gpsBluetoothDeviceModelsList != null && !gpsBluetoothDeviceModelsList.isEmpty()) {
                    onSelected = "gps";
                    bottomSheetDialogFragment = new BluetoothDeviceBottomSheetFragment(gpsBluetoothDeviceModelsList, this);
                    bottomSheetDialogFragment.setCancelable(false);
                    bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Pair with RFID Scanner" + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.btSave: {
                validator();
                Timber.i("CompanyName: " + binding.etCompanyName.getText().toString().trim() + " RFIDList: " + new Gson().toJson(rfidSelectedDeviceModelsList) + " GPSList: " + new Gson().toJson(gpsSelectedDeviceModelsList));
            }
            break;
        }
    }

    private void takePhoto() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(takePicture, 2);
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo","Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setItems(options, (dialog, item) -> {

            /*if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                dialog.dismiss();
            }*/
            if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(takePicture, 2);
                dialog.dismiss();
            } else if (options[item].equals("Choose from Gallery")) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                dialog.dismiss();
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        Uri tempUri = commonFunction.getImageUri(context, selectedImage);
                        File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                        if (selected.equalsIgnoreCase("logo")) {
                            imgPath1stTree = finalFile.getPath();
                            binding.ivLogoName.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                            Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                        }
                    }

                    break;
                case 2:
                    if (resultCode == RESULT_OK) {

                        File file = new File(Environment.getExternalStorageDirectory().toString());

                        for (File temp : file.listFiles()) {
                            if (temp.getName().equals("temp.jpg")) {
                                file = temp;
                                break;
                            }
                        }

                        try {
                            Bitmap bitmap;
                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
                            bitmap = commonFunction.getResizedBitmap(bitmap);

                            Uri tempUri = commonFunction.getImageUri(context, bitmap);
                            File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                            if (selected.equalsIgnoreCase("logo")) {
                                imgPath1stTree = finalFile.getPath();
                                binding.ivLogoName.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                                Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = activity.getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                if (selected.equalsIgnoreCase("logo")) {
                                    imgPath1stTree = picturePath;
                                    binding.ivLogoName.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                                    Timber.i("onActivityResult: CAMERA 1stTree: %s", imgPath1stTree);
                                }
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etCompanyName.getText().toString().trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Enter the Company Name");
        }else if (!commonFunction.NullPointerValidator(imgPath1stTree.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Company Logo");
        } else if (rfidSelectedDeviceModelsList.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one RFID Device");
        } else if (gpsSelectedDeviceModelsList.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one GPS Device");
        } else {

            createCompanyHashMap.put("action", RequestBody.create("create_company", MediaType.parse("text/plain")));
            createCompanyHashMap.put("company_name", RequestBody.create(binding.etCompanyName.getText().toString().trim(), MediaType.parse("text/plain")));
            createCompanyHashMap.put("rfid_list", RequestBody.create(new Gson().toJson(rfidSelectedDeviceModelsList), MediaType.parse("text/plain")));
            createCompanyHashMap.put("gps_list", RequestBody.create(new Gson().toJson(gpsSelectedDeviceModelsList), MediaType.parse("text/plain")));

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                createCompanyPresenter.getCreateCompany(createCompanyHashMap,
                        commonFunction.requestImage("logo_image", imgPath1stTree, context)
                );
//                createCompanyPresenter.getCreateCompany(createCompanyHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void addDrives(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel) {
        if (onSelected.equalsIgnoreCase("rfid")) {
            rfidBluetoothDeviceModelsList = new ArrayList<>();
            rfidBluetoothDeviceModelsList = bluetoothDeviceListModel;

            rfidSelectedDeviceModelsList = new ArrayList<>();
            for (BluetoothDeviceListModel list : rfidBluetoothDeviceModelsList) {
                if (list.isChecked()) {
                    rfidSelectedDeviceModelsList.add(list);
                }
            }
            if (!rfidSelectedDeviceModelsList.isEmpty()) {
                selectedRFIDDeviceAdapter = new SelectedBluetoothDeviceAdapter(rfidSelectedDeviceModelsList);
                binding.tvNoOfRFIDDevice.setText(rfidSelectedDeviceModelsList.size() + " Devices");
                binding.rvRFIDDevices.setAdapter(selectedRFIDDeviceAdapter);
                binding.rvRFIDDevices.setVisibility(View.VISIBLE);
                binding.tvNoRFID.setVisibility(View.GONE);
            } else {
                binding.tvNoOfRFIDDevice.setText("0 Devices");
                binding.rvRFIDDevices.setVisibility(View.GONE);
                binding.tvNoRFID.setVisibility(View.VISIBLE);
            }

        } else if (onSelected.equalsIgnoreCase("gps")) {
            gpsBluetoothDeviceModelsList = new ArrayList<>();
            gpsBluetoothDeviceModelsList = bluetoothDeviceListModel;

            gpsSelectedDeviceModelsList = new ArrayList<>();
            for (BluetoothDeviceListModel list : gpsBluetoothDeviceModelsList) {
                if (list.isChecked()) {
                    gpsSelectedDeviceModelsList.add(list);
                }
            }
            if (!gpsSelectedDeviceModelsList.isEmpty()) {
                selectedGPSDeviceAdapter = new SelectedBluetoothDeviceAdapter(gpsSelectedDeviceModelsList);
                binding.tvNoOfGPSDevice.setText(gpsSelectedDeviceModelsList.size() + " Devices");
                binding.rvGPSDevices.setAdapter(selectedGPSDeviceAdapter);
                binding.rvGPSDevices.setVisibility(View.VISIBLE);
                binding.tvNoGPS.setVisibility(View.GONE);
            } else {
                binding.tvNoOfGPSDevice.setText("0 Devices");
                binding.rvGPSDevices.setVisibility(View.GONE);
                binding.tvNoGPS.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onSuccessCreateCompany(CreateCompanyModel model) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());
        sharedHelper.putInUser("company_id", model.getResult().getCompanyId());

        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onErrorCreateCompany(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, getResources().getString(R.string.failed_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        createCompanyPresenter.onDispose();
    }
}