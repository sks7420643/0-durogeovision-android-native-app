package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterRangeListBinding;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.RangeListModel;

import java.util.ArrayList;

public class RangeListAdapter extends RecyclerView.Adapter<RangeListAdapter.ViewHolder> {

    ArrayList<RangeListModel> rangeListModels = new ArrayList<>();
    OnItemClickListener onItemClickListener;

    public RangeListAdapter(ArrayList<RangeListModel> rangeListModels,
                            OnItemClickListener onItemClickListener) {
        this.rangeListModels = rangeListModels;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RangeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RangeListAdapter.ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.adapter_range_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RangeListAdapter.ViewHolder holder, int position) {
        holder.binding.tvItemName.setText(rangeListModels.get(position).getValue() + " " + rangeListModels.get(position).getUnit());
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return rangeListModels.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterRangeListBinding binding;

        public ViewHolder(@NonNull AdapterRangeListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}