package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentRfidReadingBottomSheetBinding;
import com.treeambulance.service.rfidReader.RFIDHandler;
import com.treeambulance.service.views.adapter.RFIDListAdapter;
import com.zebra.rfid.api3.TagData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class RFIDReadingBottomSheetFragment extends BaseBottomSheet implements RFIDHandler.ResponseHandlerInterface,
        View.OnClickListener, OnItemClickListener {

    ArrayList<TagData> tagDataList;
    ArrayList<String> listRFID;
    ConcurrentHashMap<String, Integer> map;

    private RFIDHandler rFIDHandler;
    private FragmentRfidReadingBottomSheetBinding binding;
    private RFIDReading rfidReading;
    private String rfidDataToBeSaved = "";
    private BluetoothAdapter bluetoothAdapter;

    public RFIDReadingBottomSheetFragment() {
        // Required empty public constructor
    }

    public RFIDReadingBottomSheetFragment(RFIDReading rfidReading) {
        this.rfidReading = rfidReading;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rfid_reading_bottom_sheet, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        tagDataList = new ArrayList<>();
        map = new ConcurrentHashMap<>();
        rFIDHandler = new RFIDHandler(this);
        rFIDHandler.onCreate(activity);
    }

    public void clearTheRFIDValues() {
        // Binding resources Array to ListAdapter
        final String[] rfidArray = new String[0];

        activity.runOnUiThread(() -> {
            updateList(new ArrayList<>());
            map.clear();
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btClear: {
                clearTheRFIDValues();
            }
            break;
            case R.id.btCancel: {
                dismiss();
            }
            break;
        }
    }

    @Override
    public void handleTagData(TagData[] tagData) {
        final StringBuilder sb = new StringBuilder();

        for (int index = 0; index < tagData.length; index++) {
//                sb.append(tagData[index].getTagID() + "\n");
            if (map.containsKey(tagData[index].getTagID())) {
                map.put(tagData[index].getTagID(), map.get(tagData[index].getTagID()) + 1);
            } else {
                map.put(tagData[index].getTagID(), 1);
            }
        }
        // Binding resources Array to ListAdapter
        listRFID = new ArrayList<>();
        tagDataList = new ArrayList<>();
        for (String key : map.keySet()) {

            sb.append(key);
            listRFID.add(key);
        }

        if (tagData.length > 0) {
            rfidDataToBeSaved = tagData[tagData.length - 1].getTagID();
        }

        activity.runOnUiThread(() -> updateList(listRFID));

    }

    private void updateList(ArrayList<String> listRFID) {

        if (!listRFID.isEmpty()) {
            binding.rvRFIDList.setAdapter(new RFIDListAdapter(listRFID, this));
            binding.rvRFIDList.setVisibility(View.VISIBLE);
        } else {
            binding.rvRFIDList.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleTriggerPress(boolean pressed) {
        if (pressed) {
            /*activity.runOnUiThread(() -> {
                commonFunction.showLongToast("Trigger Pressed!");
            });*/
            rFIDHandler.performInventory();
        } else {
            rFIDHandler.stopInventory();
        }
    }

    @Override
    public void onItemClick(int position) {
//        rfidReading.getRFIDTagData(tagDataList.get(position));
        rfidReading.getRFIDTagData(listRFID.get(position));
        dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        rFIDHandler.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        rFIDHandler.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rFIDHandler.onDestroy();
    }

    public interface RFIDReading {
        //        public void getRFIDTagData(TagData tagData);
        public void getRFIDTagData(String tagData);
    }
}