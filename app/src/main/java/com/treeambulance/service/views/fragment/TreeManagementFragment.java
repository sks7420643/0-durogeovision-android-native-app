package com.treeambulance.service.views.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentAddTreeManagementBinding;
import com.treeambulance.service.model.AddTreeManagementModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.presenter.AddTreeManagementPresenter;
import com.treeambulance.service.views.adapter.TreeManagementListAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import timber.log.Timber;

public class TreeManagementFragment extends BaseFragment implements View.OnClickListener, AddTreeManagementPresenter.ContactInterface, TreeManagementListAdapter.TreeClickListener,CustomDialogBottomSheetFragment.CustomDialogOnButtonClick {
    private FragmentAddTreeManagementBinding binding;
    private HashMap<String, String> addProjectsHashMap,getCompanyDetailsHashMap;
    private AddTreeManagementPresenter addTreeManagementPresenter;
    private HashMap<String, String> getTreeManagementListHashMap;
    private HashMap<String, String> getCreateTreeManagementHashMap;
    private HashMap<String, String> getDeleteTreeManagementHashMap;
    private CompanyDetailsModel companyDetailsModel;
    private TreeManagementListModel treeManagementListModel;
    private int position = 0;
    private boolean isEdit = false;
    private AlertDialog.Builder alertDialogBuilder = null;
    public TreeManagementFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_tree_management, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addTreeManagementPresenter = new AddTreeManagementPresenter(this);
        getTreeManagementListHashMap = new HashMap<>();
        getCreateTreeManagementHashMap = new HashMap<>();
        getDeleteTreeManagementHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        addProjectsHashMap = new HashMap<>();
        treeManagementListModel = new TreeManagementListModel();
        hitGetCompanyDetails();
        hitGetTreeManagementList();
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addTreeManagementPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetTreeManagementList() {

        getTreeManagementListHashMap.put("action", "list_trees");
        getTreeManagementListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addTreeManagementPresenter.getTreeManagementList(getTreeManagementListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void deleteProject(int position) {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment(
                "Make sure want to delete " + treeManagementListModel.getResult().get(position).getTreeName()
                        + " ?", this);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackAddProject:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.btAddTreeManagement:
                validation();
                break;
        }
    }

    private void validation() {
        if (!commonFunction.NullPointerValidator(binding.etLocalTreeName.getText().toString().trim())) {
            binding.etLocalTreeName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Local Tree Name");
        } else if (!commonFunction.NullPointerValidator(binding.etBotanicalTreeName.getText().toString().trim())) {
            binding.etBotanicalTreeName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Botanical Tree Name");
        }else{
            addProjectsHashMap.put("action", "tree_add");
            addProjectsHashMap.put("etLocalTreeName", binding.etLocalTreeName.getText().toString().trim());
            addProjectsHashMap.put("etBotanicalTreeName", binding.etBotanicalTreeName.getText().toString().trim());
            addProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

            if (isEdit) {
                addProjectsHashMap.put("id", treeManagementListModel.getResult().get(position).getId());
            } else {
                addProjectsHashMap.remove("id");
            }

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                addTreeManagementPresenter.getAddEditTreeManagement(addProjectsHashMap);
                isEdit = false;
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onSuccessTreeManagement(AddTreeManagementModel addTreeManagementModel) {
        commonFunction.dismissLoader();
        binding.etLocalTreeName.setText("");
        binding.etBotanicalTreeName.setText("");
        try{
            commonFunction.showSuccessAlert(activity,"Tree Management",context.getString(R.string.success_title));
        }catch (Exception e){
            e.printStackTrace();
        }
//        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), addTreeManagementModel.getMsg());
        hitGetTreeManagementList();
    }

    @Override
    public void onErrorTreeManagement(String error) {
        commonFunction.dismissLoader();
        try{
            commonFunction.showErrorAlert(getContext(),"Tree Management",error);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        Timber.i("onSuccessCompanyDetails: companyDetailsModel - %s", new Gson().toJson(companyDetailsModel));
        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessTreeManagementList(TreeManagementListModel treeManagementListModel) {
        commonFunction.dismissLoader();
        if (treeManagementListModel != null && treeManagementListModel.getResult() != null && !treeManagementListModel.getResult().isEmpty()) {
            this.treeManagementListModel = treeManagementListModel;
            binding.rvTreeManagementList.setAdapter(new TreeManagementListAdapter(treeManagementListModel.getResult(), this));
            binding.tvNoData.setVisibility(View.GONE);
            binding.rvTreeManagementList.setVisibility(View.VISIBLE);
        } else {
            binding.tvNoData.setVisibility(View.VISIBLE);
            binding.rvTreeManagementList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorTreeManagementList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessDeleteTreeManagement(CommonModel commonModel) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), commonModel.getMsg());
        hitGetTreeManagementList();
    }

    @Override
    public void onErrorDeleteTreeManagement(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
            getDeleteTreeManagementHashMap.put("action", "delete_tree");
            getDeleteTreeManagementHashMap.put("tree_id", treeManagementListModel.getResult().get(position).getId());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                addTreeManagementPresenter.getDeleteOperator(getDeleteTreeManagementHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void editProject(int position) {
        binding.etLocalTreeName.setText(treeManagementListModel.getResult().get(position).getTreeName());
        binding.etBotanicalTreeName.setText(treeManagementListModel.getResult().get(position).getBotanicalName());
    }

    @Override
    public void treeOnClick(int position, String action) {
        this.position = position;

        switch (action) {
            case "edit":
                isEdit = true;
                editProject(this.position);
                break;
            case "delete":
                isEdit = false;
                deleteProject(this.position);
                break;
            default:
                isEdit = false;
        }
    }
}
