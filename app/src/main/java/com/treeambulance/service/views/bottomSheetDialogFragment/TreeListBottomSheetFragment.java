package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentOperatorListBottomSheetBinding;
import com.treeambulance.service.model.TreeManagementListModel;
import com.treeambulance.service.views.adapter.TreeListAdapter;
import com.treeambulance.service.views.adapter.TreeManagementListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class TreeListBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentOperatorListBottomSheetBinding binding;
    private ArrayList<TreeManagementListModel.Result> operatorListResultsModel;
    private TreeClickListener operatorListClickListener;

    public TreeListBottomSheetFragment() {
        // Required empty public constructor
    }

    public TreeListBottomSheetFragment(ArrayList<TreeManagementListModel.Result> operatorListResultsModel, TreeClickListener operatorListClickListener) {
        this.operatorListClickListener = operatorListClickListener;
        this.operatorListResultsModel = operatorListResultsModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_operator_list_bottom_sheet,
                container,
                false
        );

        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvList.setAdapter(new TreeListAdapter(operatorListResultsModel, this));
    }

    @Override
    public void onItemClick(int position) {
        operatorListClickListener.onTreeClick(position);
        dismiss();
    }

    public interface TreeClickListener {
        public void onTreeClick(int position);
    }
}