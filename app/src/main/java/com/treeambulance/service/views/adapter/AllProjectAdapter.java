package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterAllProjectBinding;
import com.treeambulance.service.model.ListProjectsModel;

import java.util.ArrayList;

import static com.treeambulance.service.other.Utiles.NullPointerValidator;

public class AllProjectAdapter extends RecyclerView.Adapter<AllProjectAdapter.ViewHolder> {
    private ArrayList<ListProjectsModel.Result> listProjectResultModel = new ArrayList<>();
    private AllProjectClickListener allProjectClickListener;

    public AllProjectAdapter(ArrayList<ListProjectsModel.Result> listProjectResultModel, AllProjectClickListener allProjectClickListener) {
        this.listProjectResultModel = listProjectResultModel;
        this.allProjectClickListener = allProjectClickListener;
    }

    @NonNull
    @Override
    public AllProjectAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterAllProjectBinding adapterAllProjectBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.adapter_all_project,
                parent,
                false);
        return new ViewHolder(adapterAllProjectBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull AllProjectAdapter.ViewHolder holder, int position) {
        holder.adapterAllProjectBinding.tvProject.setText(!NullPointerValidator(listProjectResultModel.get(position).getProjectName()) ? "--" : listProjectResultModel.get(position).getProjectName());
        holder.adapterAllProjectBinding.tvState.setText(!NullPointerValidator(listProjectResultModel.get(position).getState()) ? "--" : listProjectResultModel.get(position).getState());
        holder.adapterAllProjectBinding.tvDistrict.setText(!NullPointerValidator(listProjectResultModel.get(position).getDistrict()) ? "--" : listProjectResultModel.get(position).getDistrict());
        holder.adapterAllProjectBinding.tvVillage.setText(!NullPointerValidator(listProjectResultModel.get(position).getVillage()) ? "--" : listProjectResultModel.get(position).getVillage());

        holder.adapterAllProjectBinding.ivEdit.setOnClickListener(view -> allProjectClickListener.allProjectOnClick(position, "edit"));
        holder.adapterAllProjectBinding.ivDelete.setOnClickListener(view -> allProjectClickListener.allProjectOnClick(position, "delete"));
    }

    @Override
    public int getItemCount() {
        return listProjectResultModel.size();
    }

    public interface AllProjectClickListener {
        public void allProjectOnClick(int position, String action);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final AdapterAllProjectBinding adapterAllProjectBinding;

        public ViewHolder(@NonNull AdapterAllProjectBinding adapterAllProjectBinding) {
            super(adapterAllProjectBinding.getRoot());
            this.adapterAllProjectBinding = adapterAllProjectBinding;
        }
    }
}