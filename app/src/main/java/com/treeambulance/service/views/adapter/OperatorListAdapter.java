package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterOperatorListBinding;
import com.treeambulance.service.model.OperatorListModel;

import java.util.ArrayList;

public class OperatorListAdapter extends RecyclerView.Adapter<OperatorListAdapter.ViewHolder> {

    private final ArrayList<OperatorListModel.Result> operatorListResultsModel;
    private final OnItemClickListener onItemClickListener;

    public OperatorListAdapter(ArrayList<OperatorListModel.Result> operatorListResultsModel, OnItemClickListener onItemClickListener) {
        this.operatorListResultsModel = operatorListResultsModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_operator_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.setModel(operatorListResultsModel.get(position));

        holder.binding.ivDelete.setOnClickListener(v -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return operatorListResultsModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterOperatorListBinding binding;

        public ViewHolder(AdapterOperatorListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
