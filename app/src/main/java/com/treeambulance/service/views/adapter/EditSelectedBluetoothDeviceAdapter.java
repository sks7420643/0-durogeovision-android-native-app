package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterEditSelectedBluetoothDeviceBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;

import java.util.ArrayList;

public class EditSelectedBluetoothDeviceAdapter extends RecyclerView.Adapter<EditSelectedBluetoothDeviceAdapter.ViewHolder> {

    private final OnItemClickListener onItemClickListener;
    ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModels;

    public EditSelectedBluetoothDeviceAdapter(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModels, OnItemClickListener onItemClickListener) {
        this.bluetoothDeviceListModels = bluetoothDeviceListModels;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_edit_selected_bluetooth_device, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setModel(bluetoothDeviceListModels.get(position));

        holder.binding.ivDelete.setOnClickListener(v -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return bluetoothDeviceListModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final AdapterEditSelectedBluetoothDeviceBinding binding;

        public ViewHolder(@NonNull AdapterEditSelectedBluetoothDeviceBinding adapterEditSelectedBluetoothDeviceBinding) {
            super(adapterEditSelectedBluetoothDeviceBinding.getRoot());
            this.binding = adapterEditSelectedBluetoothDeviceBinding;
        }
    }
}
