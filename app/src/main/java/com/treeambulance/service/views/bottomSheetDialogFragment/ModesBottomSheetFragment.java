package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentModesBottomSheetBinding;

import org.jetbrains.annotations.NotNull;

public class ModesBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener {

    FragmentModesBottomSheetBinding binding;
    ModesOnClickListener modesOnClickListener;

    public ModesBottomSheetFragment() {
        // Required empty public constructor
    }

    public ModesBottomSheetFragment(ModesOnClickListener modesOnClickListener) {
        this.modesOnClickListener = modesOnClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_modes_bottom_sheet, container, false);

        binding.setClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btSupervisorMode: {
                modesOnClickListener.modesClickListener("supervisor_mode");
                dismiss();
            }
            break;
            case R.id.btDurotekMode: {
                modesOnClickListener.modesClickListener("durotek_mode");
                dismiss();
            }
            break;
        }
    }

    public interface ModesOnClickListener {
        public void modesClickListener(String toFragment);
    }
}