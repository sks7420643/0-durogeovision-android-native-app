package com.treeambulance.service.views.fragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentEditCompanyBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.EditCompanyModel;
import com.treeambulance.service.presenter.EditCompanyPresenter;
import com.treeambulance.service.views.adapter.EditSelectedBluetoothDeviceAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.BluetoothDeviceBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

public class DummyFragment extends BaseFragment implements View.OnClickListener,
        BluetoothDeviceBottomSheetFragment.AddSelectedDrives, EditCompanyPresenter.ContactInterface, OnItemClickListener {

    private static final int REQUEST_ENABLE_BT = 1;
    private FragmentEditCompanyBinding binding;
    private BluetoothAdapter bluetoothAdapter;

    private EditSelectedBluetoothDeviceAdapter editSelectedRFIDDeviceAdapter;
    private EditSelectedBluetoothDeviceAdapter editSelectedGPSDeviceAdapter;

    private Set<BluetoothDevice> setBluetoothDevice;

    private ArrayList<BluetoothDeviceListModel> rfidBluetoothDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> gpsBluetoothDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> rfidSelectedDeviceModelsList;
    private ArrayList<BluetoothDeviceListModel> gpsSelectedDeviceModelsList;

    private HashMap<String, String> getCompanyDetailsHashMap;
    private HashMap<String, RequestBody> editCompanyHashMap;
    private String onSelected = "",imgPath1stTree;

    private EditCompanyPresenter editCompanyPresenter;

    public DummyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dummy, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        setBluetoothDevice = bluetoothAdapter.getBondedDevices();

        rfidBluetoothDeviceModelsList = new ArrayList<>();
        gpsBluetoothDeviceModelsList = new ArrayList<>();
        rfidSelectedDeviceModelsList = new ArrayList<>();
        gpsSelectedDeviceModelsList = new ArrayList<>();

        editCompanyHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();

        binding.setStrCompanyName(sharedHelper.getFromUser("company_name"));
        Timber.i("onViewCreated: setBluetoothDevice: %s", new Gson().toJson(setBluetoothDevice));

        editCompanyPresenter = new EditCompanyPresenter(this);
        hitGetCompanyDetails();
        addBluetoothItemsToList();
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            editCompanyPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        rfidBluetoothDeviceModelsList.add(new BluetoothDeviceListModel("", 0, "Not Available", 0, false));
        /*gpsBluetoothDeviceModelsList.add(new BluetoothDeviceListModel("", 0, "Not Available", 0, false));*/

        for (BluetoothDevice bluetoothDevice : list) {
            rfidBluetoothDeviceModelsList.add(new BluetoothDeviceListModel(bluetoothDevice.getAddress(), bluetoothDevice.getBondState(), bluetoothDevice.getName(), bluetoothDevice.getType(), false));
            gpsBluetoothDeviceModelsList.add(new BluetoothDeviceListModel(bluetoothDevice.getAddress(), bluetoothDevice.getBondState(), bluetoothDevice.getName(), bluetoothDevice.getType(), false));
        }

        Timber.i("addBluetoothItemsToList: bluetoothDeviceModelsList: %s", new Gson().toJson(rfidBluetoothDeviceModelsList));
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etCompanyName.getText().toString().trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Enter the Company Name");
        } else if (rfidSelectedDeviceModelsList.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one RFID Device");
        } else if (gpsSelectedDeviceModelsList.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one GPS Device");
        } else {

            editCompanyHashMap.put("action", RequestBody.create("edit_company", MediaType.parse("text/plain")));
            editCompanyHashMap.put("company_id", RequestBody.create(sharedHelper.getFromUser("company_id"), MediaType.parse("text/plain")));
            editCompanyHashMap.put("company_name", RequestBody.create(binding.etCompanyName.getText().toString().trim(), MediaType.parse("text/plain")));
            editCompanyHashMap.put("rfid_list", RequestBody.create(new Gson().toJson(rfidSelectedDeviceModelsList), MediaType.parse("text/plain")));
            editCompanyHashMap.put("gps_list", RequestBody.create(new Gson().toJson(gpsSelectedDeviceModelsList), MediaType.parse("text/plain")));


            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                editCompanyPresenter.getEditCompany(editCompanyHashMap,
                        commonFunction.requestImage("logo_image", imgPath1stTree, context)
                );
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void loadGPSAdapter() {
        if (editSelectedGPSDeviceAdapter == null) {
            editSelectedGPSDeviceAdapter = new EditSelectedBluetoothDeviceAdapter(gpsSelectedDeviceModelsList, this);
            binding.rvGPSDevices.setAdapter(editSelectedGPSDeviceAdapter);
        } else {
            editSelectedGPSDeviceAdapter.notifyDataSetChanged();
        }
        binding.tvNoOfGPSDevice.setText(gpsSelectedDeviceModelsList.size() + " Devices");
        binding.rvGPSDevices.setVisibility(View.VISIBLE);
        binding.tvNoGPS.setVisibility(View.GONE);
    }

    private void loadRFIDAdapter() {

        if (editSelectedRFIDDeviceAdapter == null) {
            editSelectedRFIDDeviceAdapter = new EditSelectedBluetoothDeviceAdapter(rfidSelectedDeviceModelsList, this);
            binding.rvRFIDDevices.setAdapter(editSelectedRFIDDeviceAdapter);
        } else {
            editSelectedRFIDDeviceAdapter.notifyDataSetChanged();
        }
        binding.tvNoOfRFIDDevice.setText(rfidSelectedDeviceModelsList.size() + " Devices");
        binding.rvRFIDDevices.setVisibility(View.VISIBLE);
        binding.tvNoRFID.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackCreateCompany: {
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.etAddRFIDDevice: {

                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (rfidBluetoothDeviceModelsList != null && !rfidBluetoothDeviceModelsList.isEmpty()) {
                    onSelected = "rfid";
                    bottomSheetDialogFragment = new BluetoothDeviceBottomSheetFragment(rfidBluetoothDeviceModelsList, this);
                    bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Pair with RFID Scanner" + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.etAddGPSDevice: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (gpsBluetoothDeviceModelsList != null && !gpsBluetoothDeviceModelsList.isEmpty()) {
                    onSelected = "gps";
                    bottomSheetDialogFragment = new BluetoothDeviceBottomSheetFragment(gpsBluetoothDeviceModelsList, this);
                    bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Please Pair with RFID Scanner" + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.btSave: {
                validator();
                Timber.i("CompanyName: " + binding.etCompanyName.getText().toString().trim() + " RFIDList: " + new Gson().toJson(rfidSelectedDeviceModelsList) + " GPSList: " + new Gson().toJson(gpsSelectedDeviceModelsList));
            }
            break;
        }
    }

    public boolean isAdded(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel, BluetoothDeviceListModel model) {
        boolean isAvailable = false;
        for (int j = 0; j < bluetoothDeviceListModel.size(); j++) {
            System.out.println("new bluetoothDeviceListModel: " + bluetoothDeviceListModel.get(j).isChecked());
            System.out.println("new model: " + model.getAddress() != bluetoothDeviceListModel.get(j).getAddress());

            if (bluetoothDeviceListModel.get(j).isChecked() && model.getAddress() != bluetoothDeviceListModel.get(j).getAddress()) {
                isAvailable = true;
            }
        }
        return isAvailable;
    }

    @Override
    public void addDrives(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel) {
        if (onSelected.equalsIgnoreCase("rfid")) {

            /*rfidSelectedDeviceModelsList = new ArrayList<>();
            rfidSelectedDeviceModelsList = bluetoothDeviceListModel;*/

            ArrayList<BluetoothDeviceListModel> tempArray = new ArrayList<>();

            for (BluetoothDeviceListModel list : bluetoothDeviceListModel) {
                if (list.isChecked()) {
                    tempArray.add(list);
                    /*if (!rfidSelectedDeviceModelsList.contains(list)) {
                        rfidSelectedDeviceModelsList.add(list);
                    }*/
                }
            }

            Timber.i("addDrives: tempArray - %s", new Gson().toJson(tempArray));
            ArrayList<BluetoothDeviceListModel> tempBluetoothDeviceListModel = new ArrayList<>();

//            ArrayList<BluetoothDeviceListModel> tempBluetoothDeviceListModel = new ArrayList<>();

            for (int i = 0; i < tempArray.size(); i++) {
                if (isAdded(rfidSelectedDeviceModelsList, tempArray.get(i))) {
                    tempBluetoothDeviceListModel.add(tempArray.get(i));
                }
            }

            for (BluetoothDeviceListModel temp : tempBluetoothDeviceListModel) {
                if (rfidSelectedDeviceModelsList.contains(temp)) {
                    rfidSelectedDeviceModelsList.add(temp);
                }
            }
            Timber.i("addDrives: tempBluetoothDeviceListModel - %s", new Gson().toJson(tempBluetoothDeviceListModel));

            Timber.i("addDrives: rfidSelectedDeviceModelsList1 - %s", new Gson().toJson(rfidSelectedDeviceModelsList));
            /*Set<BluetoothDeviceListModel> set = new HashSet<>(rfidSelectedDeviceModelsList);
            rfidSelectedDeviceModelsList.clear();
            rfidSelectedDeviceModelsList.addAll(set);*/

            if (!rfidSelectedDeviceModelsList.isEmpty()) {
                Timber.i("addDrives: rfidSelectedDeviceModelsList - %s", new Gson().toJson(rfidSelectedDeviceModelsList));
                loadRFIDAdapter();
            } else {
                binding.tvNoOfRFIDDevice.setText("0 Devices");
                binding.rvRFIDDevices.setVisibility(View.GONE);
                binding.tvNoRFID.setVisibility(View.VISIBLE);
            }

        } else if (onSelected.equalsIgnoreCase("gps")) {
            /*gpsBluetoothDeviceModelsList = new ArrayList<>();
            gpsBluetoothDeviceModelsList = bluetoothDeviceListModel;*/

            for (BluetoothDeviceListModel list : bluetoothDeviceListModel) {
                if (!gpsSelectedDeviceModelsList.contains(list)) {
                    if (list.isChecked()) {
                        gpsSelectedDeviceModelsList.add(list);
                    }
                }
            }
            if (!gpsSelectedDeviceModelsList.isEmpty()) {
                loadGPSAdapter();

            } else {
                binding.tvNoOfGPSDevice.setText("0 Devices");
                binding.rvGPSDevices.setVisibility(View.GONE);
                binding.tvNoGPS.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        if (onSelected.equalsIgnoreCase("rfid")) {
            rfidSelectedDeviceModelsList.remove(position);
            editSelectedRFIDDeviceAdapter.notifyDataSetChanged();
        } else if (onSelected.equalsIgnoreCase("gps")) {
            gpsSelectedDeviceModelsList.remove(position);
            editSelectedGPSDeviceAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                gpsSelectedDeviceModelsList.add(new BluetoothDeviceListModel(list.getAddress(), Integer.parseInt(list.getBondstate()), list.getName(), Integer.parseInt(list.getType()), true));
                loadGPSAdapter();
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(gpsSelectedDeviceModelsList));

        if (!companyDetailsModel.getResult().getRfidList().isEmpty()) {
            for (CompanyDetailsModel.RfidList list : companyDetailsModel.getResult().getRfidList()) {
                rfidSelectedDeviceModelsList.add(new BluetoothDeviceListModel(list.getAddress(), Integer.parseInt(list.getBondstate()), list.getName(), Integer.parseInt(list.getType()), true));
                loadRFIDAdapter();
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "RFID Device List is Empty");
        }

        Timber.i("onSuccessCompanyDetails: RFIDSelectedBluetooth: %s", new Gson().toJson(rfidSelectedDeviceModelsList));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessEditCompany(EditCompanyModel model) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());
        sharedHelper.putInUser("company_id", model.getResult().getCompanyId());

        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onErrorEditCompany(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, getResources().getString(R.string.failed_title), error);
    }

    @Override
    public void onSuccessEditCompanyData(EditCompanyModel model) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorEditCompanyData(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        editCompanyPresenter.onDispose();
    }
}