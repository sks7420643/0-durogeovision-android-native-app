package com.treeambulance.service.views.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentDuroTekModeBinding;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.presenter.DuroTekModePresenter;
import com.treeambulance.service.views.activity.SelectCompanyListActivity;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class DuroTekModeFragment extends BaseFragment implements View.OnClickListener, CustomDialogBottomSheetFragment.CustomDialogOnButtonClick, DuroTekModePresenter.ContactInterface {

    private FragmentDuroTekModeBinding binding;
    private DuroTekModePresenter duroTekModePresenter;
    private HashMap<String, String> resetPasswordHashMap;

    public DuroTekModeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_duro_tek_mode, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        resetPasswordHashMap = new HashMap<>();
        duroTekModePresenter = new DuroTekModePresenter(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackAdmin: {
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.btCreateCompany: {
                moveToFragment(new CreateCompanyFragment(), android.R.id.content, false);
            }
            break;
            case R.id.btEditCompany: {
                moveToFragment(new EditCompanyFragment(), android.R.id.content, false);
            }
            break;
            case R.id.btResetPassword: {
//                moveToFragment(new CreateCompanyFragment(), android.R.id.content, false);
                openResetPassword();
            }
            break;
            case R.id.btExistingCompany: {
                setIntent(new Intent(activity, SelectCompanyListActivity.class), 1);
            }
            break;
        }
    }

    private void openResetPassword() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment("Sure you want to Reset Supervisor Password for " + sharedHelper.getFromUser("company_name") + " ?", this);
        bottomSheetDialogFragment.show(fragmentManager, "reset_password_dialog_sheet");
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
            resetPasswordHashMap.put("action", "reset_password");
            resetPasswordHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                duroTekModePresenter.getResetPassword(resetPasswordHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onSuccessResetPassword(CommonModel commonModel) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), commonModel.getMsg());
    }

    @Override
    public void onErrorResetPassword(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        duroTekModePresenter.onDispose();
    }
}