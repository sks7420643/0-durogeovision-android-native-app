package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.bluetoothReader.BluetoothChatServiceForContinuosData;
import com.treeambulance.service.databinding.FragmentGpsReadingBottomSheetBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;
import com.treeambulance.service.utility.Constants;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;

import timber.log.Timber;

public class GPSReadingBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener {

    private static final int REQUEST_ENABLE_BT = 1;
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
            }
        }
    };
    private FragmentGpsReadingBottomSheetBinding binding;
    private String mConnectedDeviceName = "";
    private BluetoothChatServiceForContinuosData mChatService = null;
    //    private ArrayAdapter<String> mConversationArrayAdapter;
    private int counter = 0;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean secure;
    //    private String bluetoothGPSModule = "D4:CA:6E:8B:F6:55";
    private GPSReading gpsReading;
    private ArrayList<String> GPSDeviceList;
    private String latitude = "", longitude = "";
    /**
     * The Handler that gets information back from the BluetoothChatServiceForContinuosData
     */
    private final Handler mHandler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
//            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
//                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTING:
//                            setStatus(String.valueOf(R.string.title_connecting));
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_LISTEN:
                        case BluetoothChatServiceForContinuosData.STATE_NONE:
//                            setStatus(String.valueOf(R.string.title_not_connected));
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
//                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_LONG).show();
                    sendToProcessMessage(readMessage);
//                    mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    commonFunction.showShortToast("Connected to " + mConnectedDeviceName);
                    break;
                case Constants.MESSAGE_TOAST:
                    if (msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Unable to connect device") || msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Device connection was lost")) {
                        counter++;
                        Timber.i("");
                        if (counter < GPSDeviceList.size()) {
                            attempt();
                        }
                    }
                    binding.tvStatus.setText("GPS Reading is in Progress... Attempt: " + counter);
                    if (counter > GPSDeviceList.size()) {

                        commonFunction.showShortToast(msg.getData().getString(Constants.TOAST));
//                        binding.btRefresh.performClick();
                        showFailedMessage("Bluetooth Connection Failed", "Retry?");
                    }
                    break;
            }
        }
    };

    public GPSReadingBottomSheetFragment() {
        // Required empty public constructor
    }

    public GPSReadingBottomSheetFragment(ArrayList<String> GPSDeviceList, GPSReading gpsReading) {
        this.GPSDeviceList = GPSDeviceList;
        this.gpsReading = gpsReading;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gps_reading_bottom_sheet, container, false);

        binding.setLifecycleOwner(this);
        binding.setClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mChatService = new BluetoothChatServiceForContinuosData(context, mHandler);

        Timber.i("GPSDeviceList: %s", new Gson().toJson(GPSDeviceList));

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        // Attempt to connect to the device
        secure = false;

        attempt();

        try {
            Type type = new TypeToken<BluetoothDeviceListModel>() {
            }.getType();

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("onSuccess: API_WINNING_NUMBER: %s", e.getMessage());
        }
    }

    private void attempt() {
        if (GPSDeviceList != null && !GPSDeviceList.isEmpty()) {
            Timber.i("attempt: counter " + counter + " - " + GPSDeviceList.get(counter));
            BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(GPSDeviceList.get(counter));
            mChatService.connect(device1, secure);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btRefresh:
                gpsReading.getGPSTagData(true, "", "");
                break;
            case R.id.btSubmit:
                if (commonFunction.NullPointerValidator(latitude)
                        && commonFunction.NullPointerValidator(longitude)) {
                    dismiss();
                    gpsReading.getGPSTagData(false, latitude, longitude);
                } else {
                    commonFunction.showShortToast("Refresh to Get Location");
                }
                break;
            case R.id.btCancel:
                dismiss();
                break;
        }
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(String resId) {
        Context context1 = context;
        if (null == context1) {
            return;
        }
//        binding.tvStatus.setText(binding.tvStatus.getText() + " , " + resId);
        Timber.i("setStatus %s", resId);
    }

    private void showFailedMessage(String message, String title) {

        // Create the object of
        // AlertDialog Builder class
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(context);

        // Set the message show for the Alert time
        builder.setMessage(message);

        // Set Alert Title
        builder.setTitle(title);

        // Set Cancelable false
        // for when the user clicks on the outside
        // the Dialog Box then it will remain show
        builder.setCancelable(false);

        // Set the positive button with yes name
        // OnClickListener method is use of
        // DialogInterface interface.

        builder.setPositiveButton(
                "Ok",
                new DialogInterface
                        .OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        // When the user click yes button
                        // then app will close
//                                retryBluetoothConnection(null);
                        Timber.i("showFailedMessage PositiveButton");
                        binding.btRefresh.performClick();
                    }
                })
                .setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }
                );

        // Create the Alert dialog
        AlertDialog alertDialog = builder.create();
        if (!activity.isFinishing()) {
            //show dialog
            // Show the Alert Dialog box
            alertDialog.show();
        }
    }

    /*
     * Convert a NMEA decimal-decimal degree value into degrees/minutes/seconds
     * First. convert the decimal-decimal value to a decimal:
     * 5144.3855 (ddmm.mmmm) = 51 44.3855 = 51 + 44.3855/60 = 51.7397583 degrees
     *
     * Then convert the decimal to degrees, minutes seconds:
     * 51 degress + .7397583 * 60 = 44.385498 = 44 minutes
     * .385498 = 23.1 seconds
     * Result: 51 44' 23.1"
     *
     * @return String value of the decimal degree, using the proper units
     */
    private double decimalToDMS(double value) {
        Timber.i("decimalToDMS: value %s", value);
        int degrees = (int) (value / 100);
        Timber.i("decimalToDMS: degrees %s", degrees);
        double minutes = (value - degrees * 100) / 60;
        Timber.i("decimalToDMS: minutes %s", minutes);
        return degrees + minutes;
    }

    private void sendToProcessMessage(String readMessage) {
        String[] lineSeparatedArray = readMessage.split("\n");
        if (lineSeparatedArray.length > 1) {
            //Check if we have lan lat in our line
            for (int i = 0; i < lineSeparatedArray.length; i++) {
                String[] splitByGNRMC = lineSeparatedArray[i].split("GNRMC");
                if (splitByGNRMC.length > 1) {
                    try {
                        Timber.i("splitByGNRMC: " + splitByGNRMC[1].split(",")[3] + "\t" + splitByGNRMC[1].split(",")[5]);
                        double lat = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[3]));
                        double lon = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[5]));
                        Timber.i("lon: " + lon + "\t lat: " + lat);

                        latitude = String.valueOf(lat);
                        longitude = String.valueOf(lon);

                        updateMarker(lon, lat);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Timber.e("sendToProcessMessage: splitData - %s", e.getMessage());
                    }
                }
                Timber.i("sendToProcessMessage: lineSeparatedArray - " + lineSeparatedArray[i] + "\t" + i);
            }
        }
    }

    private void updateMarker(double lon, double lat) {
//        binding.tvData.setText(String.format("GPS Data Logged\nLAT %S\nLONG %S\nClick Submit to continue", lat, lon));
        binding.tvData.setText(String.format("Device Name: %S\nGPS Data Logged\nLAT %S\nLONG %S\nClick Submit to continue", mConnectedDeviceName, lat, lon));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            if (receiver.isInitialStickyBroadcast()) {
                activity.unregisterReceiver(receiver);
            }
            if (mChatService != null) {
                mChatService.stop();
                Timber.i("onDestroyView: mChatService.stop");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("onDestroyView: %s", e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (receiver.isInitialStickyBroadcast()) {
                activity.unregisterReceiver(receiver);
            }
            if (mChatService != null) {
                mChatService.stop();
                Timber.i("onDestroy: mChatService.stop");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("onDestroy: %s", e.getMessage());
        }
    }

    public interface GPSReading {
        public void getGPSTagData(boolean isRefresh, String latitude, String longitude);
    }

}