package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterMultiListBinding;
import com.treeambulance.service.model.CityModel;
import com.treeambulance.service.model.StateCityModel;

import java.util.ArrayList;

public class CityMultiListAdapter extends RecyclerView.Adapter<CityMultiListAdapter.ViewHolder> {

    private ArrayList<CityModel> cityModels;
    private OnCityMultiListSelected onCityMultiListSelected;

    public CityMultiListAdapter(ArrayList<CityModel> cityModels, OnCityMultiListSelected onCityMultiListSelected) {
        this.cityModels = cityModels;
        this.onCityMultiListSelected = onCityMultiListSelected;
    }

    @NonNull
    @Override
    public CityMultiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_multi_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CityMultiListAdapter.ViewHolder holder, int position) {
        holder.binding.tvItemName.setText(cityModels.get(position).getDistrict());
        holder.binding.cbItem.setChecked(cityModels.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (cityModels.get(position).isChecked()) {
                onCityMultiListSelected.onCitySelected(position, false);
            } else if (!cityModels.get(position).isChecked()) {
                onCityMultiListSelected.onCitySelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cityModels.size();
    }

    public interface OnCityMultiListSelected {
        public void onCitySelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterMultiListBinding binding;

        public ViewHolder(@NonNull AdapterMultiListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
