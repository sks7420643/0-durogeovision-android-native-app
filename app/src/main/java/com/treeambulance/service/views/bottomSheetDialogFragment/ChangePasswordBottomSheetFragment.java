package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentChangePasswordBottomSheetBinding;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class ChangePasswordBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener {

    public HashMap<String, String> changePasswordHashMap;
    FragmentChangePasswordBottomSheetBinding binding;
    ChangePasswordOnClickListener changePasswordOnClickListener;

    public ChangePasswordBottomSheetFragment() {
        // Required empty public constructor
    }

    public ChangePasswordBottomSheetFragment(ChangePasswordOnClickListener changePasswordOnClickListener) {
        this.changePasswordOnClickListener = changePasswordOnClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password_bottom_sheet, container, false);

        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        changePasswordHashMap = new HashMap<>();

        if (commonFunction.NullPointerValidator(sharedHelper.getFromUser("company_name"))) {
            binding.tvTitle.setText("Sure you want to Change Supervisor Password for " + sharedHelper.getFromUser("company_name") + " ?");
        } else {
            binding.tvTitle.setText("Sure you want to Change Supervisor Password ?");
        }

        changePasswordHashMap.put("action", "change_password");
        changePasswordHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btSubmit: {
                if (!commonFunction.NullPointerValidator(binding.edtPassword.getText().toString().trim())) {
                    commonFunction.showLongToast("Please Enter Old Password");
                } else if (!commonFunction.NullPointerValidator(binding.edtNewPassword.getText().toString().trim())) {
                    commonFunction.showLongToast("Please Enter New Password");
                } else if (!commonFunction.NullPointerValidator(binding.edtConfirmPassword.getText().toString().trim())) {
                    commonFunction.showLongToast("Please Enter New Confirm Password");
                } else if (!binding.edtNewPassword.getText().toString().trim().equalsIgnoreCase(binding.edtConfirmPassword.getText().toString().trim())) {
                    commonFunction.showLongToast("New and Confirm Password Mismatch");
                } else {
                    changePasswordHashMap.put("old_pwd", binding.edtPassword.getText().toString().trim());
                    changePasswordHashMap.put("new_pwd", binding.edtNewPassword.getText().toString().trim());
                    changePasswordOnClickListener.changePasswordClickListener(changePasswordHashMap);
                    dismiss();
                }
            }
            break;
        }
    }

    public interface ChangePasswordOnClickListener {
        public void changePasswordClickListener(HashMap<String, String> changePasswordHashMap);
    }
}