package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentStateMultiListBottomSheetBinding;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.views.adapter.StateMultiListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class StateMultiListBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener, StateMultiListAdapter.OnStateMultiListSelected {
    private FragmentStateMultiListBottomSheetBinding binding;
    private ArrayList<StateCityModel> stateListResultsModel;
    private StateListMultiClickListener stateListMultiClickListener;
    private StateMultiListAdapter stateMultiListAdapter;

    public StateMultiListBottomSheetFragment() {
        // Required empty public constructor
    }

    public StateMultiListBottomSheetFragment(ArrayList<StateCityModel> stateListResultsModel, StateListMultiClickListener stateListMultiClickListener) {
        this.stateListMultiClickListener = stateListMultiClickListener;
        this.stateListResultsModel = stateListResultsModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_state_multi_list_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stateMultiListAdapter = new StateMultiListAdapter(stateListResultsModel, this);
        binding.rvStateList.setAdapter(stateMultiListAdapter);
    }

    @Override
    public void onStateSelected(int position, boolean isChecked) {
        stateListResultsModel.get(position).setChecked(isChecked);
        stateMultiListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                stateListMultiClickListener.onStateMultiClick(stateListResultsModel);
                dismiss();
            }
            break;
            case R.id.btSelectAll: {

                for (int i = 0; i < stateListResultsModel.size(); i++) {
                    stateListResultsModel.get(i).setChecked(true);
                }

                stateListMultiClickListener.onStateMultiClick(stateListResultsModel);
                dismiss();
            }
            break;
            case R.id.btDeselectAll: {

                for (int i = 0; i < stateListResultsModel.size(); i++) {
                    stateListResultsModel.get(i).setChecked(false);
                }

                stateListMultiClickListener.onStateMultiClick(stateListResultsModel);
                dismiss();
            }
            break;
        }
    }

    public interface StateListMultiClickListener {
        void onStateMultiClick(ArrayList<StateCityModel> stateListResultsModel);
    }
}