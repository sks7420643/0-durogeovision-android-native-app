package com.treeambulance.service.views.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentProjectHomeBinding;
import com.treeambulance.service.views.bottomSheetDialogFragment.GPSReadingBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.RFIDReadingBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

public class ProjectHomeFragment extends BaseFragment implements View.OnClickListener, RFIDReadingBottomSheetFragment.RFIDReading, GPSReadingBottomSheetFragment.GPSReading {
    private FragmentProjectHomeBinding binding;

    public ProjectHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_project_home, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (commonFunction.NullPointerValidator(sharedHelper.getFromUser("company_name"))) {
            binding.tvTitle.setText(sharedHelper.getFromUser("company_name"));
            binding.tvTitle.setVisibility(View.VISIBLE);
        } else {
            binding.tvTitle.setVisibility(View.GONE);
        }
        if (commonFunction.NullPointerValidator(sharedHelper.getFromUser("company_logo"))) {
            if (commonFunction.NullPointerValidator(sharedHelper.getFromUser("company_logo"))) {
                Glide.with(activity).load(sharedHelper.getFromUser("company_logo")).into(binding.ivBackProjectHome);
            }
            binding.ivBackProjectHome.setVisibility(View.VISIBLE);
        } else {
            binding.ivBackProjectHome.setVisibility(View.GONE);
        }

//        binding.tvAboutUsValue.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btRFIDReader:
//                openRFIDReader();
                break;

            case R.id.btGPSReader:
//                openGPSReader();
//                moveToFragment(new DummyFragment(), android.R.id.content, false);
                /*if (bottomSheetDialogFragment != null)
                    bottomSheetDialogFragment.dismiss();

                bottomSheetDialogFragment = new RFIDReadingBottomSheetFragment();
                bottomSheetDialogFragment.show(fragmentManager, "bluetooth_device_list_sheet");*/
                break;
        }
    }

    /*private void openRFIDReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }

        bottomSheetDialogFragment = new RFIDReadingBottomSheetFragment(this);
        bottomSheetDialogFragment.show(fragmentManager, "RFID_reading_sheet");
    }

    private void openGPSReader() {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }

        bottomSheetDialogFragment = new GPSReadingBottomSheetFragment(this);
        bottomSheetDialogFragment.show(fragmentManager, "GPS_reading_sheet");
    }*/

    /*@Override
    public void getRFIDTagData(TagData tagData) {
        Timber.i("getRFIDTagData: %s", tagData.getTagID());
    }*/

    @Override
    public void getRFIDTagData(String tagData) {
        Timber.i("getRFIDTagData: %s", tagData);
    }

    @Override
    public void getGPSTagData(boolean isRefresh, String latitude, String longitude) {
        if (isRefresh) {
//            openGPSReader();
        } else {
            commonFunction.showLongToast("LAT: " + latitude + "\nLON: " + longitude);
        }
    }
}