package com.treeambulance.service.views.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentMaintenanceBinding;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.TreeDetailsModel;
import com.treeambulance.service.presenter.MaintenancePresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllProjectsBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.OperatorListBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class MaintenanceFragment extends BaseFragment implements View.OnClickListener,
        MaintenancePresenter.ContactInterface, AllProjectsBottomSheetFragment.OnProjectClickListener, OperatorListBottomSheetFragment.OperatorListClickListener {

    private final Calendar myCalendar = Calendar.getInstance();
    private final DatePickerDialog.OnDateSetListener a_date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            /*str_MaintenanceDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            binding.etMaintenanceDate.setText(commonFunction.getReverseDate(str_MaintenanceDate));*/
        }
    };
    private FragmentMaintenanceBinding binding;
    private String selected = "", imgPath1stTree = "", imgPath2ndTree = "", imgPathSelifePhoto = "";
    private MaintenancePresenter maintenancePresenter;
    private HashMap<String, String> allProjectsHashMap;
    private HashMap<String, String> treeDetailsHashMap;
    private HashMap<String, String> getOperatorListHashMap;
    private HashMap<String, RequestBody> createMaintenanceHashMap;
    private ListProjectsModel listProjectsModel;
    private OperatorListModel operatorListModel;
    private String str_MaintenanceDate = "", projectId = "", treeId = "", operatorId = "";
    AutoCompleteTextView textView=null;
    private ArrayAdapter<String> adapter;
    public MaintenanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_maintenance, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        allProjectsHashMap = new HashMap<>();
        treeDetailsHashMap = new HashMap<>();
        getOperatorListHashMap = new HashMap<>();
        createMaintenanceHashMap = new HashMap<>();

        listProjectsModel = new ListProjectsModel();
        operatorListModel = new OperatorListModel();

        maintenancePresenter = new MaintenancePresenter(this);

        binding.etMaintenanceDate.setText(commonFunction.getCurrentDate()[0]);
        str_MaintenanceDate = commonFunction.getCurrentDate()[1];

        String[] stringArray = getResources().getStringArray(R.array.maintenance_status);
        textView = (AutoCompleteTextView) getView().findViewById(R.id.autoCompleteTextView);

        //Create adapter
        adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_dropdown_item_1line, stringArray);

        textView.setThreshold(1);

        //Set adapter to AutoCompleteTextView
        textView.setAdapter(adapter);

        hitGetAllProjects();
        hitGetOperatorList();
    }

    private void hitGetAllProjects() {
        allProjectsHashMap.put("action", "list_projects");
        allProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            maintenancePresenter.getListProjects(allProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            maintenancePresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackMaintenance:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.etProjectName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new AllProjectsBottomSheetFragment(listProjectsModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "all_project_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            /*case R.id.etMaintenanceDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, *//*R.style.datepicker,*//* a_date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 30000);
                datePickerDialog.show();
                break;*/
            case R.id.etOperatorName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new OperatorListBottomSheetFragment(operatorListModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "operator_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Operator List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.ivTree1st:
                selected = "1stTree";
//                selectImage();
                takePhoto();
                break;
            case R.id.ivTree2nd:
                selected = "2ndTree";
//                selectImage();
                takePhoto();
                break;
            case R.id.ivSelfiePhoto:
                selected = "selfiePhoto";
//                selectImage();
                takePhoto();
                break;
            case R.id.btSubmit:
                validatorGetTreeDetails();
                break;
            case R.id.btSave:
                validator();
                break;
        }
    }

    private void takePhoto() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(takePicture, 2);
    }

    private void validatorGetTreeDetails() {
        if (!commonFunction.NullPointerValidator(binding.etProjectName.getText().toString().trim())) {
            binding.etProjectName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Project Name");
        } else if (!commonFunction.NullPointerValidator(binding.etNewTreeNo.getText().toString().trim())) {
            binding.etNewTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Tree No");
        } else {

//            treeDetailsHashMap.put("action", "get_tree_details");
            treeDetailsHashMap.put("action", "get_tree_maintenance_details");
            treeDetailsHashMap.put("project_id", projectId);
            treeDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            treeDetailsHashMap.put("new_tree_no", binding.etNewTreeNo.getText().toString().trim());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                maintenancePresenter.getTreeDetails(treeDetailsHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void validator() {

        if (!commonFunction.NullPointerValidator(projectId.trim()) && !commonFunction.NullPointerValidator(treeId.trim())) {
            binding.etTreeNo.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Fetch Tree Details");
        } else if (!commonFunction.NullPointerValidator(str_MaintenanceDate.trim())) {
            binding.etMaintenanceDate.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Maintenance Date");
        } else if (!commonFunction.NullPointerValidator(binding.autoCompleteTextView.getText().toString().trim())) {
            binding.autoCompleteTextView.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select Maintenance Status");
        } else if (!commonFunction.NullPointerValidator(binding.etOperatorName.getText().toString().trim())) {
            binding.etOperatorName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Operator Name");
        } else if (!commonFunction.NullPointerValidator(binding.etStatusTree.getText().toString().trim())) {
            binding.etStatusTree.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Health Status");
        } else if (!commonFunction.NullPointerValidator(imgPath1stTree.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Tree 1st Photo");
        } else if (!commonFunction.NullPointerValidator(imgPath2ndTree.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Tree 2st Photo");
        } else if (!commonFunction.NullPointerValidator(imgPathSelifePhoto.trim())) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Upload Supervisor Selfie Photo");
        } else {

            createMaintenanceHashMap.put("action", RequestBody.create("create_maintenance", MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("project_id", RequestBody.create(projectId, MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("company_id", RequestBody.create(sharedHelper.getFromUser("company_id"), MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("new_tree_no", RequestBody.create(treeId, MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("old_tree", RequestBody.create(binding.etTreeNo.getText().toString(), MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("land_survey_number", RequestBody.create(binding.etLangSurveyNo.getText().toString(), MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("maintenance_date", RequestBody.create(str_MaintenanceDate, MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("health_status", RequestBody.create(binding.etStatusTree.getText().toString().trim(), MediaType.parse("text/plain")));
            createMaintenanceHashMap.put("operator_name", RequestBody.create(binding.etOperatorName.getText().toString().trim(), MediaType.parse("text/plain")));
            if (commonFunction.NullPointerValidator(binding.etNotes.getText().toString().trim())) {
                createMaintenanceHashMap.put("notes", RequestBody.create(binding.etNotes.getText().toString().trim(), MediaType.parse("text/plain")));
            }
            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                maintenancePresenter.getCreateMaintenance(createMaintenanceHashMap,
                        commonFunction.requestImage("tree_first_photo", imgPath1stTree, context),
                        commonFunction.requestImage("tree_second_photo", imgPath2ndTree, context),
                        commonFunction.requestImage("selfie_photo", imgPathSelifePhoto, context)
                );
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", /*"Choose from Gallery",*/ "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setItems(options, (dialog, item) -> {

            /*if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                dialog.dismiss();
            }*/
            if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(takePicture, 2);
                dialog.dismiss();
            } else if (options[item].equals("Choose from Gallery")) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//one can be replaced with any action code
                dialog.dismiss();
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        Uri tempUri = commonFunction.getImageUri(context, selectedImage);
                        File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                        if (selected.equalsIgnoreCase("1stTree")) {
                            imgPath1stTree = finalFile.getPath();
                            binding.ivTree1st.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                            Timber.i("onActivityResult: CAMERA 1stTree: " + imgPath1stTree);
                        } else if (selected.equalsIgnoreCase("2ndTree")) {
                            imgPath2ndTree = finalFile.getPath();
                            binding.ivTree2nd.setImageBitmap(BitmapFactory.decodeFile(imgPath2ndTree));
                            Timber.i("onActivityResult: CAMERA 2ndTree: " + imgPath2ndTree);
                        } else if (selected.equalsIgnoreCase("selfiePhoto")) {
                            imgPathSelifePhoto = finalFile.getPath();
                            binding.ivSelfiePhoto.setImageBitmap(BitmapFactory.decodeFile(imgPathSelifePhoto));
                            Timber.i("onActivityResult: CAMERA selfiePhoto: " + imgPathSelifePhoto);
                        }
                    }
                    break;
                case 2:
                    if (resultCode == RESULT_OK) {

                        File file = new File(Environment.getExternalStorageDirectory().toString());

                        for (File temp : file.listFiles()) {
                            if (temp.getName().equals("temp.jpg")) {
                                file = temp;
                                break;
                            }
                        }

                        try {
                            Bitmap bitmap;
                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                            bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);
                            bitmap = commonFunction.getResizedBitmap(bitmap);

                            Uri tempUri = commonFunction.getImageUri(context, bitmap);
                            File finalFile = new File(commonFunction.getRealPathFromURI(activity, tempUri));
                            if (selected.equalsIgnoreCase("1stTree")) {
                                binding.ivTree1st.setImageBitmap(bitmap);
                                imgPath1stTree = finalFile.getPath();
                            } else if (selected.equalsIgnoreCase("2ndTree")) {

                                binding.ivTree2nd.setImageBitmap(bitmap);
                                imgPath2ndTree = finalFile.getPath();
                            } else if (selected.equalsIgnoreCase("selfiePhoto")) {

                                binding.ivSelfiePhoto.setImageBitmap(bitmap);
                                imgPathSelifePhoto = finalFile.getPath();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = activity.getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                if (selected.equalsIgnoreCase("1stTree")) {
                                    imgPath1stTree = picturePath;
                                    binding.ivTree1st.setImageBitmap(BitmapFactory.decodeFile(imgPath1stTree));
                                    Timber.i("onActivityResult: GALLERY 1stTree: " + imgPath1stTree);
                                } else if (selected.equalsIgnoreCase("2ndTree")) {
                                    imgPath2ndTree = picturePath;
                                    binding.ivTree2nd.setImageBitmap(BitmapFactory.decodeFile(imgPath2ndTree));
                                    Timber.i("onActivityResult: GALLERY 2ndTree: " + imgPath2ndTree);
                                } else if (selected.equalsIgnoreCase("selfiePhoto")) {
                                    imgPathSelifePhoto = picturePath;
                                    binding.ivSelfiePhoto.setImageBitmap(BitmapFactory.decodeFile(imgPathSelifePhoto));
                                    Timber.i("onActivityResult: GALLERY selfiePhoto: " + imgPathSelifePhoto);
                                }
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onSuccessListProjects(ListProjectsModel listProjectsModel) {
        commonFunction.dismissLoader();
        if (listProjectsModel != null && listProjectsModel.getResult() != null && !listProjectsModel.getResult().isEmpty()) {
            this.listProjectsModel = listProjectsModel;
        }
    }

    @Override
    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessTreeDetails(TreeDetailsModel treeDetailsModel) {
        commonFunction.dismissLoader();

        Timber.i("onSuccessTreeDetails: treeDetailsModel - %s", new Gson().toJson(treeDetailsModel));

        projectId = treeDetailsModel.getResult().getProjectId();
        treeId = treeDetailsModel.getResult().getTreeNo();
        binding.etTreeNo.setText(treeDetailsModel.getResult().getOldTree());
        binding.etLangSurveyNo.setText(treeDetailsModel.getResult().getLandSurveyNumber());
        String imageURL = treeDetailsModel.getResult().getPhoto1();

        if (isNetworkAvailable(activity)) {
            if (commonFunction.NullPointerValidator(imageURL)) {
                Glide.with(activity).load(imageURL).into(binding.ivPreTree1st);
            }
        } else {
            commonFunction.showNoNetwork(activity);
        }

        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), "Tree Details Fetched...!");
    }

    @Override
    public void onErrorTreeDetails(String error) {
        commonFunction.dismissLoader();

        treeId = "";
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListModel = operatorListModel;
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onSuccessCreateMaintenance(CommonModel model) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), model.getMsg());

        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onErrorCreateMaintenance(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onProjectClick(int position) {
        binding.etProjectName.setText(listProjectsModel.getResult().get(position).getProjectName());
        projectId = listProjectsModel.getResult().get(position).getId();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
                bottomSheetDialogFragment.dismiss();
            }
        } catch (Exception e) {
            Timber.e("onDestroy: %s", e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        maintenancePresenter.onDispose();
    }

    @Override
    public void onOperatorClick(int position) {
        binding.etOperatorName.setText(operatorListModel.getResult().get(position).getName());
        operatorId = operatorListModel.getResult().get(position).getId();
    }
}