package com.treeambulance.service.views.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DownloadManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentReportGenerationBinding;
import com.treeambulance.service.model.CityModel;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.ReportTypeModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.model.StatusRangeModel;
import com.treeambulance.service.model.VillageListModel;
import com.treeambulance.service.other.AppLocationService;
import com.treeambulance.service.presenter.ReportGenerationPresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.CityMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CountryListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.OperatorMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.ReportTypeBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StateMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StatusRangeBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StatusRangeMultiListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.VillageMultiListBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

public class ReportGenerationFragment extends BaseFragment implements View.OnClickListener,
        OperatorMultiListBottomSheetFragment.OperatorListMultiClickListener, StateMultiListBottomSheetFragment.StateListMultiClickListener, StatusRangeMultiListBottomSheetFragment.StatusRangeMultiClickListener,
        CityMultiListBottomSheetFragment.CityListMultiClickListener, ReportTypeBottomSheetFragment.OnReportTypeClickListener, ReportGenerationPresenter.ContactInterface, VillageMultiListBottomSheetFragment.VillageListMultiClickListener, CountryListBottomSheetFragment.CountryListClickListener  {

    private final Calendar myCalendar = Calendar.getInstance();
    private final DatePickerDialog.OnDateSetListener from_date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            str_FromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            binding.etFromDate.setText(commonFunction.getReverseDate(str_FromDate));
        }
    };

    private final DatePickerDialog.OnDateSetListener to_date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            str_ToDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            binding.etToDate.setText(commonFunction.getReverseDate(str_ToDate));
        }
    };

    private FragmentReportGenerationBinding binding;
    private ArrayList<ReportTypeModel> reportTypeModel;
    private ArrayList<StatusRangeModel> statusRangeModel;
    private ArrayList<StatusRangeModel> stateRangeSelectedModel;
    private ArrayList<StateCityModel> stateCityModel;
    private ArrayList<StateCityModel> stateCitySelectedModel;
    private ArrayList<String> GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private ArrayList<CityModel> citySelectedModel;
    private ArrayList<CityModel> cityModel;
    private StringBuilder districtName,operatorsName,statesName,villageName;
    private ArrayList<OperatorListModel.Result> operatorListResultModel;
    private ArrayList<CountryListModel.Result> countryListResultModel;
    private ArrayList<OperatorListModel.Result> operatorSelectedListResultModel;
    private ArrayList<VillageListModel.Result> villageListResultModel;
    private ArrayList<VillageListModel.Result> villageListResultSelectModel;

    private ReportGenerationPresenter reportPresenter;
    private HashMap<String, String> allProjectsHashMap, getOperatorListHashMap, getCompanyDetailsHashMap, getCheckTreeNoHashMap,getCountryListHashMap;
    private HashMap<String, RequestBody> createTransplantationHashMap;
    private HashMap<String, String> getVillageListHashMap;
    private HashMap<String, String> getReportGenerationHashMap;
    private String str_FromDate = "", str_ToDate = "",projectId = "", surveyId = "", operatorId = "";
    private String str_ReportType = "",str_StatusType = "", str_CountryType = "";
    private String latitude = "0.0", longitude = "0.0", distance = "0";
    private Set<BluetoothDevice> setBluetoothDevice;
    private LocationManager locationManager;

    private Context mContext;
    AppLocationService appLocationService;
    private String state,district,village;
    DownloadManager manager;

    public ReportGenerationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_generation, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reportTypeModel = new ArrayList<>();
        statusRangeModel = new ArrayList<>();
        villageListResultModel = new ArrayList<>();
        stateCityModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        operatorListResultModel = new ArrayList<>();
        countryListResultModel = new ArrayList<>();
        GPSDeviceList = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();
        allProjectsHashMap = new HashMap<>();
        getOperatorListHashMap = new HashMap<>();
        getCountryListHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();
        getCheckTreeNoHashMap = new HashMap<>();
        createTransplantationHashMap = new HashMap<>();
        getVillageListHashMap = new HashMap<>();
        getReportGenerationHashMap = new HashMap<>();
        mContext = getActivity();
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        reportPresenter = new ReportGenerationPresenter(this);
        loadJSONFromAsset();
        str_FromDate = commonFunction.getCurrentDate()[1];
        str_ToDate = commonFunction.getCurrentDate()[1];
        hitGetOperatorList();
        hitGetCountryList();
    }


    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            reportPresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetCountryList(){
        getCountryListHashMap.put("action", "country_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            reportPresenter.getCountryList(getCountryListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetVillageList() {

        getVillageListHashMap.put("action", "village_list");
        getVillageListHashMap.put("type", "survey");
        getVillageListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
        getVillageListHashMap.put("district", new Gson().toJson(citySelectedModel));

        Timber.i("hitGetVillageList: getVillageListHashMap - %s", getVillageListHashMap);

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            reportPresenter.getVillageList(getVillageListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray1 = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/report_type.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray1));

        String str_response1 = new Gson().toJson(jsonArray1);

        try {
            Type type = new TypeToken<ArrayList<ReportTypeModel>>() {
            }.getType();

            reportTypeModel = new Gson().fromJson(str_response1, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(reportTypeModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }

        JsonArray jsonArray2 = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/states_and_districts.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray2));

        String str_response2 = new Gson().toJson(jsonArray2);

        try {
            Type type = new TypeToken<ArrayList<StateCityModel>>() {
            }.getType();

            stateCityModel = new Gson().fromJson(str_response2, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(stateCityModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }


        JsonArray jsonArray3 = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/status_range.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray status range: %s", new Gson().toJson(jsonArray1));

        String str_response3 = new Gson().toJson(jsonArray3);

        try {
            Type type = new TypeToken<ArrayList<StatusRangeModel>>() {
            }.getType();

            statusRangeModel = new Gson().fromJson(str_response3, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(statusRangeModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }

    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etReportType.getText().toString().trim())) {
            binding.etReportType.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, context.getResources().getString(R.string.select_report));
        } else if (!commonFunction.NullPointerValidator(binding.etCountryName.getText().toString().trim())) {
            binding.etCountryName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, context.getResources().getString(R.string.select_country));
        } else if (!commonFunction.NullPointerValidator(binding.etFromDate.getText().toString().trim())) {
            binding.etFromDate.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, context.getResources().getString(R.string.select_from_date));
        } else if (!commonFunction.NullPointerValidator(binding.etToDate.getText().toString().trim())) {
            binding.etToDate.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, context.getResources().getString(R.string.select_to_date));
        } else if (operatorSelectedListResultModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one Operator");
        } else if (stateCitySelectedModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one State");
        } else if (citySelectedModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one District");
        } else if (villageListResultSelectModel.isEmpty()) {
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select at least one Village");
        }   else {
//            ArrayList<OperatorNameModel> operatorName = new ArrayList<>();
//            for (OperatorListModel.Result list : operatorSelectedListResultModel) {
//                operatorName.add(new OperatorNameModel(list.getName()));
//            }
//
//            ArrayList<StateNameModel> stateName = new ArrayList<>();
//            for (StateCityModel list : stateCitySelectedModel) {
//                stateName.add(new StateNameModel(list.getState()));
//            }
//
//            ArrayList<DistrictNameModel> districtName = new ArrayList<>();
//            for (CityModel list : citySelectedModel) {
//                districtName.add(new DistrictNameModel(list.getDistrict()));
//            }
//
//            ArrayList<VillageNameModel> villageName = new ArrayList<>();
//            for (VillageListModel.Result list : villageListResultSelectModel) {
//                villageName.add(new VillageNameModel(list.getVillage()));
//            }
            getReportGenerationHashMap.put("action", "reports");
            getReportGenerationHashMap.put("type", str_ReportType);
            getReportGenerationHashMap.put("from_date", str_FromDate);
            getReportGenerationHashMap.put("to_date", str_ToDate);
            getReportGenerationHashMap.put("status", str_StatusType);
            getReportGenerationHashMap.put("cmpny_id", sharedHelper.getFromUser("company_id"));
            getReportGenerationHashMap.put("operator", operatorsName.toString());
            getReportGenerationHashMap.put("country", binding.etCountryName.getText().toString().trim());
            getReportGenerationHashMap.put("state", statesName.toString());
            getReportGenerationHashMap.put("city", districtName.toString());
            getReportGenerationHashMap.put("village", villageName.toString());
            getReportGenerationHashMap.put("submit", "Download");

            Timber.i("validator: getReportGenerationHashMap - %s", getReportGenerationHashMap);
            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                reportPresenter.getReportGeneration(getReportGenerationHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackReportGeneration:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.etReportType:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (reportTypeModel != null && !reportTypeModel.isEmpty()) {
                    bottomSheetDialogFragment = new ReportTypeBottomSheetFragment(reportTypeModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etCountryName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (countryListResultModel != null && !countryListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new CountryListBottomSheetFragment(countryListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "country_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etStatusName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (statusRangeModel != null && !statusRangeModel.isEmpty()) {
                    bottomSheetDialogFragment = new StatusRangeMultiListBottomSheetFragment(statusRangeModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etFromDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK,from_date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 30000);
                datePickerDialog.show();
                break;
            case R.id.etToDate:
                DatePickerDialog datePickerDialog1 = new DatePickerDialog(context,AlertDialog.THEME_DEVICE_DEFAULT_DARK,to_date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

//                datePickerDialog1.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 30000);
                datePickerDialog1.show();
                break;
            case R.id.etState:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateMultiListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etDistrict:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityMultiListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "City List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etOperatorName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (operatorListResultModel != null && !operatorListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new OperatorMultiListBottomSheetFragment(operatorListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "operator_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Operator List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etVillage: {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (villageListResultModel != null && !villageListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new VillageMultiListBottomSheetFragment(villageListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "village_multi_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Village List is Empty " + getResources().getString(R.string.something_wrong));
                }
            }
            break;
            case R.id.btSubmit:
                validator();
                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        reportPresenter.onDispose();
    }

    @Override
    public void onReportTypeClick(int position) {
        binding.etReportType.setText(reportTypeModel.get(position).getReportType());
        str_ReportType = reportTypeModel.get(position).getReportType();
        if(str_ReportType.equals("Maintenance")){
            binding.llStatus.setVisibility(View.VISIBLE);
        }else{
            binding.llStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListResultModel = operatorListModel.getResult();;
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
        if (countryListModel != null && countryListModel.getResult() != null && !countryListModel.getResult().isEmpty()) {
            this.countryListResultModel = countryListModel.getResult();;
        }
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessVillageList(VillageListModel villageListModel) {
        commonFunction.dismissLoader();
        Timber.i("onSuccessVillageList: villageListModel - %s", new Gson().toJson(villageListModel));
        if (villageListModel != null && villageListModel.getResult() != null && !villageListModel.getResult().isEmpty()) {
            this.villageListResultModel = villageListModel.getResult();
        }
    }

    @Override
    public void onErrorVillageList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessSurveyReport(CommonModel commonModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorSurveyReport(String error) {

    }

    @Override
    public void onSuccessTransplantationReport(CommonModel commonModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorTransplantationReport(String error) {

    }

    @Override
    public void onSuccessMaintenanceReport(CommonModel commonModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorMaintenanceReport(String error) {

    }

    @Override
    public void onSuccessConsolidatedReport(CommonModel commonModel) {
        commonFunction.dismissLoader();
    }

    @Override
    public void onErrorConsolidatedReport(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessReportGeneration(CommonModel commonModel) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        Timber.i("url>>>>>>>> - %s", commonModel.getMsg());
        downloadFile(commonModel.getMsg());
        commonFunction.dismissLoader();
    }

    public void downloadFile(String url) {
        Uri Download_Uri = Uri.parse(url);
        File file = new File("" + Download_Uri);
        Timber.i("filename>>>>>>>> - %s",file.getName());
        DownloadManager manager1 = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle(file.getName());
        request.setDescription(file.getName());
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/File"  + "/" + file.getName());
        manager1.enqueue(request);
        String filepath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/File" + "/" + file.getName();
        File file1 = new File(filepath);
        Uri path = Uri.fromFile(file1);

        Timber.i("filename read>>>> - %s",filepath);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path,"text/csv");
        startActivity(intent);
        Timber.i("filename open success>>>>");
//        DownloadManager.Request request1 = new DownloadManager.Request(Uri.parse(DownloadUrl));
//        request1.setDescription("Excel");   //appears the same in Notification bar while downloading
//        request1.setVisibleInDownloadsUi(false);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            request1.allowScanningByMediaScanner();
//            request1.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
//        }
//        request1.setDestinationInExternalFilesDir(mContext.getApplicationContext(), "/File", "sample.xlsx");
//
//        DownloadManager manager1 = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Objects.requireNonNull(manager1).enqueue(request1);
        if (DownloadManager.STATUS_SUCCESSFUL == 8) {
            Timber.i("url>>>>>>>> - %s","download sucess");
        }
    }

    @Override
    public void onErrorReportGeneration(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onVillageMultiClick(ArrayList<VillageListModel.Result> villageListResultsModel) {
        villageListResultModel = new ArrayList<>();
        villageListResultSelectModel = new ArrayList<>();
        villageListResultModel = villageListResultsModel;
        villageName = new StringBuilder();

        for (VillageListModel.Result list : villageListResultModel) {
            if (list.isChecked()) {
                villageListResultSelectModel.add(list);
            }
        }

        for (int i = 0; i < villageListResultsModel.size(); i++) {
            if (villageListResultsModel.get(i).isChecked()) {
                if (villageName.toString().trim().equals("")) {
                    villageName.append(villageListResultsModel.get(i).getVillage());
                } else {
                    villageName.append(", ").append(villageListResultsModel.get(i).getVillage());
                }
            }
        }

        Timber.i("onVillageMultiClick: %s", new Gson().toJson(villageListResultSelectModel));
        binding.etVillage.setText(villageName.toString());
    }

    @Override
    public void onCityMultiClick(ArrayList<CityModel> cityListResultsModel) {
        cityModel = new ArrayList<>();
        citySelectedModel = new ArrayList<>();
        villageListResultModel = new ArrayList<>();
        villageListResultSelectModel = new ArrayList<>();
        cityModel = cityListResultsModel;
        districtName = new StringBuilder();

        for (CityModel list : cityModel) {
            if (list.isChecked()) {
                citySelectedModel.add(list);
            }
        }

        for (int i = 0; i < cityListResultsModel.size(); i++) {
            if (cityListResultsModel.get(i).isChecked()) {
                if (districtName.toString().trim().equals("")) {
                    districtName.append(cityListResultsModel.get(i).getDistrict());
                } else {
                    districtName.append(", ").append(cityListResultsModel.get(i).getDistrict());
                }
            }
        }

        Timber.i("onCityMultiClick: %s", new Gson().toJson(citySelectedModel));
        binding.etDistrict.setText(districtName.toString());
        binding.etVillage.setText("");
        hitGetVillageList();
    }

    @Override
    public void onOperatorMultiClick(ArrayList<OperatorListModel.Result> operatorListResultsModel) {
        operatorListResultModel = new ArrayList<>();
        operatorSelectedListResultModel = new ArrayList<>();
        operatorListResultModel = operatorListResultsModel;
        operatorsName = new StringBuilder();

        for (OperatorListModel.Result list : operatorListResultModel) {
            if (list.isChecked()) {
                operatorSelectedListResultModel.add(list);
            }
        }

        for (int i = 0; i < operatorListResultsModel.size(); i++) {
            if (operatorListResultsModel.get(i).isChecked()) {
                if (operatorsName.toString().trim().equals("")) {
                    operatorsName.append(operatorListResultsModel.get(i).getName());
                } else {
                    operatorsName.append(", ").append(operatorListResultsModel.get(i).getName());
                }
            }
        }

        Timber.i("onOperatorMultiClick: %s", new Gson().toJson(operatorSelectedListResultModel));
        binding.etOperatorName.setText(operatorsName.toString());
    }

    @Override
    public void onStateMultiClick(ArrayList<StateCityModel> stateListResultsModel) {
        cityModel = new ArrayList<>();
        citySelectedModel = new ArrayList<>();
        villageListResultModel = new ArrayList<>();
        villageListResultSelectModel = new ArrayList<>();
        stateCityModel = new ArrayList<>();
        stateCitySelectedModel = new ArrayList<>();
        stateCityModel = stateListResultsModel;
        statesName = new StringBuilder();

        for (StateCityModel list : stateCityModel) {
            if (list.isChecked()) {
                stateCitySelectedModel.add(list);
                for (String listDistrict : list.getDistricts()) {
                    cityModel.add(new CityModel(listDistrict, false));
                }
            }
        }

        for (int i = 0; i < stateListResultsModel.size(); i++) {
            if (stateListResultsModel.get(i).isChecked()) {
                if (statesName.toString().trim().equals("")) {
                    statesName.append(stateListResultsModel.get(i).getState());
                } else {
                    statesName.append(", ").append(stateListResultsModel.get(i).getState());
                }
            }
        }

        Timber.i("onStateMultiClick: %s", new Gson().toJson(stateCitySelectedModel));
        binding.etState.setText(statesName.toString());
        binding.etDistrict.setText("");
        binding.etVillage.setText("");
    }

    @Override
    public void onStatusMultiClick(ArrayList<StatusRangeModel> statusRangeModels) {
        statusRangeModel = new ArrayList<>();
        statusRangeModel = statusRangeModels;
        stateRangeSelectedModel = new ArrayList<>();
        StringBuilder statusName = new StringBuilder();
        Timber.i("onStateMultiClick: %s",statusRangeModel.size());
        for (StatusRangeModel list : statusRangeModel) {
            if (list.isChecked()) {
                stateRangeSelectedModel.add(list);
            }
        }

        for (int i = 0; i < stateRangeSelectedModel.size(); i++) {
            if (stateRangeSelectedModel.get(i).isChecked()) {
                if (statusName.toString().trim().equals("")) {
                    statusName.append(stateRangeSelectedModel.get(i).getStatus());
                } else {
                    statusName.append(", ").append(stateRangeSelectedModel.get(i).getStatus());
                }
            }
        }

        binding.etStatusName.setText(statusName.toString());
        str_StatusType = statusName.toString();
    }

    @Override
    public void onCountryClick(int position) {
        Timber.i("countryListResultModel: %s",countryListResultModel.size());
        binding.etCountryName.setText(countryListResultModel.get(position).getName());
        str_CountryType = countryListResultModel.get(position).getName();
    }
}
