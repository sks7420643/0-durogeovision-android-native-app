package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentCountryListBottomSheetBinding;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.views.adapter.CountryListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CountryListBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentCountryListBottomSheetBinding binding;
    private ArrayList<CountryListModel.Result> countryListModel = new ArrayList<>();
    private CountryListClickListener countryListClickListener;

    public CountryListBottomSheetFragment(ArrayList<CountryListModel.Result> countryListModel, CountryListClickListener countryListClickListener) {
        this.countryListClickListener = countryListClickListener;
        this.countryListModel = countryListModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_country_list_bottom_sheet,
                container,
                false
        );

        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvCountryList.setAdapter(new CountryListAdapter(countryListModel, this));
    }

    @Override
    public void onItemClick(int position) {
        countryListClickListener.onCountryClick(position);
        dismiss();
    }

    public interface CountryListClickListener {
        public void onCountryClick(int position);
    }
}
