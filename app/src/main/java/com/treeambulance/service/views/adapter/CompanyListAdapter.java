package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterCompanyListBinding;
import com.treeambulance.service.model.CompanyListModel;

import java.util.ArrayList;

public class CompanyListAdapter extends RecyclerView.Adapter<CompanyListAdapter.ViewHolder> {

    private ArrayList<CompanyListModel.Result> companyListResultsModel;
    private OnItemClickListener onItemClickListener;

    public CompanyListAdapter(ArrayList<CompanyListModel.Result> companyListResultsModel, OnItemClickListener onItemClickListener) {
        this.companyListResultsModel = companyListResultsModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_company_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.setModel(companyListResultsModel.get(position));

        holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return companyListResultsModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public AdapterCompanyListBinding binding;

        public ViewHolder(@NonNull AdapterCompanyListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
