package com.treeambulance.service.views.fragment;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.bluetoothReader.BluetoothChatServiceForContinuosData;
import com.treeambulance.service.databinding.FragmentAddProjectBinding;
import com.treeambulance.service.model.AddProjectsModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.StateCityModel;
import com.treeambulance.service.other.AppLocationService;
import com.treeambulance.service.presenter.AddProjectsPresenter;
import com.treeambulance.service.utility.Constants;
import com.treeambulance.service.views.bottomSheetDialogFragment.CityListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.CountryListBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.StateListBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import timber.log.Timber;

public class AddProjectFragment extends BaseFragment implements View.OnClickListener, CountryListBottomSheetFragment.CountryListClickListener,
        StateListBottomSheetFragment.OnStateClickListener, CityListBottomSheetFragment.OnCityClickListener, AddProjectsPresenter.ContactInterface {

    private FragmentAddProjectBinding binding;

    private ArrayList<StateCityModel> stateCityModel;
    private CompanyDetailsModel companyDetailsModel;
    private ArrayList<String> cityModel,GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private AddProjectsPresenter addProjectsPresenter;
    private HashMap<String, String> addProjectsHashMap,getCompanyDetailsHashMap,getCountryListHashMap;
    private ArrayList<CountryListModel.Result> countryListResultModel;
    private ListProjectsModel.Result listProjectsResultModel = new ListProjectsModel.Result();
    private String action = "add_project";
    private LocationManager locationManager;
    private Context mContext;
    AppLocationService appLocationService;
    private String state,district,village,country;
    private String mConnectedDeviceName = "",str_CountryType ="";
    private BluetoothChatServiceForContinuosData mChatService = null;
    //    private ArrayAdapter<String> mConversationArrayAdapter;
    private int counter = 0;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean secure;
    private Set<BluetoothDevice> setBluetoothDevice;
    private double latd,lngd;
    public AddProjectFragment() {
        action = "add_project";
    }

    public AddProjectFragment(ListProjectsModel.Result result) {
        this.listProjectsResultModel = result;
        action = "edit_project";
    }

    private final Handler mHandler2 = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(Message msg) {
//            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
//                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_CONNECTING:
//                            setStatus(String.valueOf(R.string.title_connecting));
                            break;
                        case BluetoothChatServiceForContinuosData.STATE_LISTEN:
                        case BluetoothChatServiceForContinuosData.STATE_NONE:
//                            setStatus(String.valueOf(R.string.title_not_connected));
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
//                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(getApplicationContext(), readMessage, Toast.LENGTH_LONG).show();
                    if (mBluetoothAdapter.isEnabled()) {
                        sendToProcessMessage(readMessage);
                    }else{
                        commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Bluetooth Connection Failed");
                    }

//                    mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    commonFunction.showShortToast("Connected to " + mConnectedDeviceName);
                    break;
                case Constants.MESSAGE_TOAST:
                    if (msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Unable to connect device") || msg.getData().getString(Constants.TOAST).equalsIgnoreCase("Device connection was lost")) {
                        counter++;
                        Timber.i("");
                        if (counter < GPSDeviceList.size()) {
                            attempt();
                        }
                    }
                    commonFunction.showShortToast("GPS Reading is in Progress... Attempt: " + counter);
                    if (counter > GPSDeviceList.size()) {

                        commonFunction.showShortToast(msg.getData().getString(Constants.TOAST));
                        commonFunction.showShortToast("Bluetooth Connection Failed");
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_project, container, false);
        binding.setLifecycleOwner(this);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stateCityModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        GPSDeviceList = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();
        countryListResultModel = new ArrayList<>();
        getCountryListHashMap = new HashMap<>();
        addProjectsPresenter = new AddProjectsPresenter(this);
        addProjectsHashMap = new HashMap<>();
        getCompanyDetailsHashMap = new HashMap<>();

        mChatService = new BluetoothChatServiceForContinuosData(context, mHandler2);


        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        // Attempt to connect to the device
        secure = false;

        loadJSONFromAsset();



        if (action.equalsIgnoreCase("edit_project")) {
            binding.tvTitle.setText("Edit Project");
            binding.btAddProject.setText("Update Project");
            binding.etProject.setText(listProjectsResultModel.getProjectName());
            binding.etState.setText(listProjectsResultModel.getState());
            binding.etDistrict.setText(listProjectsResultModel.getDistrict());
            binding.etVillage.setText(listProjectsResultModel.getVillage());

            for (StateCityModel stateCityDetail : stateCityModel) {
                if (stateCityDetail.getState().equalsIgnoreCase(listProjectsResultModel.getState())) {
                    cityModel.addAll(stateCityDetail.getDistricts());
                }
            }
        }

        mContext = getActivity();
//        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }

//        appLocationService = new AppLocationService(
//                mContext);
//
//        Location gpsLocation = appLocationService
//                .getLocation(LocationManager.GPS_PROVIDER,mContext);
        addBluetoothItemsToList();
        hitGetCompanyDetails();
        hitGetCountryList();
    }

    private void hitGetCountryList(){
        getCountryListHashMap.put("action", "country_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addProjectsPresenter.getCountryList(getCountryListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        for (BluetoothDevice bluetoothDevice : list) {

            GPSFromBluetooth.add(bluetoothDevice.getAddress());
        }

        Timber.i("addBluetoothItemsToList: GPSFromBluetooth: %s", new Gson().toJson(GPSFromBluetooth));
    }

    private void attempt() {
        if (GPSDeviceList != null && !GPSDeviceList.isEmpty()) {
            Timber.i("attempt: counter " + counter + " - " + GPSDeviceList.get(counter));
            BluetoothDevice device1 = mBluetoothAdapter.getRemoteDevice(GPSDeviceList.get(counter));
            mChatService.connect(device1, secure);
        }
    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            addProjectsPresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void sendToProcessMessage(String readMessage) {
        String[] lineSeparatedArray = readMessage.split("\n");
        if (lineSeparatedArray.length > 1) {
            //Check if we have lan lat in our line
            for (int i = 0; i < lineSeparatedArray.length; i++) {
                String[] splitByGNRMC = lineSeparatedArray[i].split("GNRMC");
                if (splitByGNRMC.length > 1) {
                    try {
                        Timber.i("splitByGNRMC: " + splitByGNRMC[1].split(",")[3] + "\t" + splitByGNRMC[1].split(",")[5]);
                        double lat = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[3]));
                        double lon = decimalToDMS(Double.parseDouble(splitByGNRMC[1].split(",")[5]));
                        Timber.i("lon: " + lon + "\t lat: " + lat);
                        latd = lat;
                        lngd = lon;
                        if (latd != 0.0 && lngd != 0.0) {
                            double latitude = latd;
                            double longitude = lngd;

                            List<Address> addresses = commonFunction.getAddress((Activity) mContext,latitude,longitude);
                            Address address = addresses.get(0);
                            district = address.getLocality();
                            state = address.getAdminArea();
                            country = address.getCountryName();

                            StringBuilder sb = new StringBuilder();
                            if (address.getSubThoroughfare() != null){
                                sb.append(address.getSubThoroughfare()).append(",");
                            }
                            if (address.getThoroughfare() != null){
                                sb.append(address.getThoroughfare()).append(",");
                            }
                            if (address.getSubLocality() != null){
                                sb.append(address.getSubLocality());
                            }
                            village = sb.toString();
                            binding.etVillage.setText(village);
                            binding.etCountryName.setText(country);
                            if (stateCityModel != null && !stateCityModel.isEmpty()) {
                                cityModel = new ArrayList<>();
                                for (int j = 0; j < stateCityModel.size(); j++) {
                                    cityModel.addAll(stateCityModel.get(j).getDistricts());
                                    if(stateCityModel.get(j).getState().toLowerCase().equals(state.toLowerCase())){
                                        binding.etState.setText(state);
                                        break;
                                    }
                                }
                            }
                            if (cityModel != null && !cityModel.isEmpty()) {
                                for (int j = 0; j < cityModel.size(); j++) {
                                    if(cityModel.get(j).toLowerCase().equals(district.toLowerCase())){
                                        binding.etDistrict.setText(district);
                                        break;
                                    }
                                }
                            }
                            if (mBluetoothAdapter.isEnabled()) {
                                mChatService.stop();
                            }
//
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Timber.e("sendToProcessMessage: splitData - %s", e.getMessage());
                    }
                }
                Timber.i("sendToProcessMessage: lineSeparatedArray - " + lineSeparatedArray[i] + "\t" + i);
            }
        }
    }

    private double decimalToDMS(double value) {
        Timber.i("decimalToDMS: value %s", value);
        int degrees = (int) (value / 100);
        Timber.i("decimalToDMS: degrees %s", degrees);
        double minutes = (value - degrees * 100) / 60;
        Timber.i("decimalToDMS: minutes %s", minutes);
        return degrees + minutes;
    }

    private void loadJSONFromAsset() {

        JsonArray jsonArray = JsonParser
                .parseString(commonFunction.getJsonDataFromAsset(context, "json/states_and_districts.json"))
                .getAsJsonArray();

        Timber.i("loadJSONFromAsset: jsonArray states_and_districts: %s", new Gson().toJson(jsonArray));

        String str_response = new Gson().toJson(jsonArray);

        try {
            Type type = new TypeToken<ArrayList<StateCityModel>>() {
            }.getType();

            stateCityModel = new Gson().fromJson(str_response, type);

            Timber.i("loadJSONFromAsset: stateCityModel: %s", new Gson().toJson(stateCityModel));

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("loadJSONFromAsset: states_and_districts: %s", e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackAddProject:
                hideSoftKeyBoard(context, view);
                fragmentManager.popBackStackImmediate();
//                fragmentManager.beginTransaction().remove(this).commit();
                break;
            case R.id.etState:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (stateCityModel != null && !stateCityModel.isEmpty()) {
                    bottomSheetDialogFragment = new StateListBottomSheetFragment(stateCityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "state_list_sheet");
                } else {
//                    commonFunction.showLongToast("State List is Empty " + getResources().getString(R.string.something_wrong));

                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "State List is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etCountryName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (countryListResultModel != null && !countryListResultModel.isEmpty()) {
                    bottomSheetDialogFragment = new CountryListBottomSheetFragment(countryListResultModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "country_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.etDistrict:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (cityModel != null && !cityModel.isEmpty()) {
                    bottomSheetDialogFragment = new CityListBottomSheetFragment(cityModel, this);
                    bottomSheetDialogFragment.show(fragmentManager, "city_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select the State");
                }
                break;
           case R.id.btAddProject:
                validation();
                break;
        }
    }

    private void validation() {

        if (!commonFunction.NullPointerValidator(binding.etProject.getText().toString().trim())) {
            binding.etProject.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Project Name");
        } else if (!commonFunction.NullPointerValidator(binding.etState.getText().toString().trim())) {
            binding.etState.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select State");
        } else if (!commonFunction.NullPointerValidator(binding.etDistrict.getText().toString().trim())) {
            binding.etDistrict.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Select District");
        } else if (!commonFunction.NullPointerValidator(binding.etVillage.getText().toString().trim())) {
            binding.etVillage.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Village Name");
        } else if (!commonFunction.NullPointerValidator(binding.etLocationName.getText().toString().trim())) {
            binding.etLocationName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter Location Name");
        } else {

            if (action.equalsIgnoreCase("edit_project")) {
                addProjectsHashMap.put("id", listProjectsResultModel.getId());
            } else {
                addProjectsHashMap.remove("id");
            }

            addProjectsHashMap.put("action", "add_projects");
            addProjectsHashMap.put("country", binding.etCountryName.getText().toString().trim());
            addProjectsHashMap.put("state", binding.etState.getText().toString().trim());
            addProjectsHashMap.put("district", binding.etDistrict.getText().toString().trim());
            addProjectsHashMap.put("village", binding.etVillage.getText().toString().trim());
            addProjectsHashMap.put("location", binding.etLocationName.getText().toString().trim());
            addProjectsHashMap.put("project_name", binding.etProject.getText().toString().trim());
            addProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                addProjectsPresenter.getAddEditProjects(addProjectsHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }

    @Override
    public void onStateClick(int position) {
        cityModel = new ArrayList<>();
        binding.etState.setText(stateCityModel.get(position).getState());

        cityModel.addAll(stateCityModel.get(position).getDistricts());
        Timber.i("onStateClick %s", new Gson().toJson(cityModel));
    }

    @Override
    public void onCityClick(int position) {
        binding.etDistrict.setText(cityModel.get(position));
        Timber.i("onCityClick %s", new Gson().toJson(cityModel.get(position)));
    }

    @Override
    public void onSuccessAddProjects(AddProjectsModel addProjectsModel) {
        commonFunction.dismissLoader();
        if (action.equalsIgnoreCase("edit_project")) {
            commonFunction.SuccessAlert(activity, "Success...!", "Project Updated Successfully"/*"addProjectsModel.getMsg()"*/);
            fragmentManager.popBackStackImmediate();
        } else {
            commonFunction.SuccessAlert(activity, "Success...!", "Project Added Successfully"/*"addProjectsModel.getMsg()"*/);
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                fragmentManager.popBackStack();
                moveToFragment(new AllProjectsFragment(), android.R.id.content, false);
            }, 100);
        }
    }

    @Override
    public void onErrorAddProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.displayErrorSnackbar(binding.getRoot(), context, error);
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();
        Timber.i("onSuccessCompanyDetails: companyDetailsModel - %s", new Gson().toJson(companyDetailsModel));
        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }

        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }
        if (GPSSelectedBluetooth != null && !GPSSelectedBluetooth.isEmpty()) {
            attempt();
        }
    }

    @Override
    public void onErrorCompanyDetails(String error) {

    }

    @Override
    public void onSuccessCountryList(CountryListModel countryListModel) {
        commonFunction.dismissLoader();
        if (countryListModel != null && countryListModel.getResult() != null && !countryListModel.getResult().isEmpty()) {
            this.countryListResultModel = countryListModel.getResult();;
        }
    }

    @Override
    public void onErrorCountryList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        addProjectsPresenter.onDispose();
    }

    @Override
    public void onCountryClick(int position) {
        binding.etCountryName.setText(countryListResultModel.get(position).getName());
        str_CountryType = countryListResultModel.get(position).getName();
    }
}