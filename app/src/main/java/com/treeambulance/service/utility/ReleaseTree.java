package com.treeambulance.service.utility;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.splunk.mint.Mint;

import timber.log.Timber;

public class ReleaseTree extends Timber.Tree {
    @Override
    protected void log(int priority, @Nullable String tag, @NonNull String message, @Nullable Throwable throwable) {
        if (priority == Log.ERROR) {
            if (throwable != null) {
                Mint.logException((Exception) throwable);
            }
        }
    }
}
