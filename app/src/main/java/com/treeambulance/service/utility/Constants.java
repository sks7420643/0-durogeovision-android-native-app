package com.treeambulance.service.utility;

public class Constants {
    public static final String PARAMETER_SEP = "&";
    public static final String PARAMETER_EQUALS = "=";
    public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";
    public static final String SHARED_USER_DATA = "user_data";
    public static final String SHARED_DEVICE_DATA = "device_data";
    public static final String SHARED_APP_DATA = "app_data";
    public static final String MyPREFERENCES = "myProject";
    public static final int REQUEST_CODE_AUTOCOMPLETE = 10;
    public static Boolean BackPresses = true;
    public static Boolean RESTorCat = false;
    public static String ERROR = "Something went wrong...!";

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static String DEVICE_NAME = "device_name";
    public static String TOAST = "toast";

    //    Operation to be performed.
    public static String RETRIEVE = "RETRIEVE";
    public static String CREATE = "CREATE";
    public static String UPDATE = "UPDATE";
    public static String DELETE = "DELETE";
    public static String CREATE_TABLE = "CREATE_TABLE";
    public static String INITIALISE = "INITIALISE";

    //    Entity upon which the operation should be made.
    public static String AREA = "AREA";
    public static String PROJECT = "PROJECT";
    public static String MARKER = "MARKER";
    public static String OPERATOR = "OPERATOR";
    public static String DUROTEK = "DUROTEK";
    public static String COMPANY = "COMPANY";
    public static String APPLICATION = "APPLICATION";
    public static String DATALOGS = "DATALOGS";
    public static String GPS = "GPS";


    public static String SUPERVISOR_PASSWORD = "SUPERVISOR_PASSWORD";
    public static String LOG = "LOGGING===========";
    public static String CREATE_NEW_COMPANY = "CREATE_NEW_COMPANY";
    public static String RESET_SUPERVISOR_PASSWORD = "RESET_SUPERVISOR_PASSWORD";

    //    User Mode.
    public static String SUPERVISOR_MODE = "SUPERVISOR_MODE";
    public static String OPERATOR_MODE = "OPERATOR_MODE";
    public static String DUROTEK_MODE = "DUROTEK_MODE";


    public static String RFIDREADER = "RFIDREADER";
    public static String GPSREADER = "GPSREADER";

    public static String REMOTE_ID = "REMOTE_ID";

    //Map
    public static final long SET_INTERVAL = 5000; //5 Seconds
    public static final long SET_FASTESTINTERVAL = 3000;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 2; //2 Seconds

    // The minimum distance to change Updates in meters
    public static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = (float) 0.1; // 10 meters

    public static final double BOUNDARY_RADIUS = 1;
}