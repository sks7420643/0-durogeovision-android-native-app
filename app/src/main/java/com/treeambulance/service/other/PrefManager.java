package com.treeambulance.service.other;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    public static final String PREF_NAME = "myPref";
    public static final String TOKEN = "token";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    Context context;

    public PrefManager(Context context) {
        try {
            this._context = context;
            pref = context.getSharedPreferences(PREF_NAME, context.MODE_PRIVATE);
            editor = pref.edit();
        } catch (NullPointerException e) {
            e.getMessage();
        }
    }

    public String getToken() {
        return pref.getString(TOKEN, "");
    }

    public void setToken(String token) {
        editor = pref.edit();
        editor.putString(TOKEN, token);
        editor.apply();
        editor.commit();
    }

}
