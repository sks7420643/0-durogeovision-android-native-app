package com.treeambulance.service.other;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.tapadoo.alerter.Alerter;
import com.treeambulance.service.R;
import com.treeambulance.service.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import timber.log.Timber;

public class Utiles {

    private static ProgressDialog dialog;

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String NullPointer(String value) {
        if (value != null && !value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("") && !value.isEmpty()) {
            return value;
        } else {
            return "";
        }
    }

    public static Boolean NullPointerValidator(String string) {
        if (string != null && !string.equalsIgnoreCase("") && !string.isEmpty() && !string.equalsIgnoreCase("null")) {
            return true;
        } else {
            return false;
        }
    }

    public static String RupeeFormat(String string) {
        return "\u20B9 " + new DecimalFormat("##,##,##,###.##").format(Double.valueOf(string));
    }

    /*public static String getBeforeCurrentDate() {
        Date date = Calendar.getInstance().getTime();
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(myFormat, Locale.US);

        return simpleDateFormat.format(date);
    }*/

    public static String getCurrentDate() {
        Date date = Calendar.getInstance().getTime();
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(myFormat, Locale.US);

        return simpleDateFormat.format(date);
    }

    public static String getBeforeCurrentDate(String deliveryDate) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(myFormat, Locale.US);
        try {
            Date date = simpleDateFormat.parse(deliveryDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            c.add(Calendar.DATE, -1);
            deliveryDate = simpleDateFormat.format(c.getTime());

            Timber.i("getBeforeCurrentDate: %s", deliveryDate);

            System.out.println(date);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            deliveryDate = "";
        }
        return deliveryDate;
    }

    public static String getRequiredDate(int editable) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, editable);
        Date date = calendar.getTime();

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(myFormat, Locale.US);

        return simpleDateFormat.format(date);
    }

    public static String getDate(String dateString) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MMM-yyyy");
            return fmtOut.format(date);
        } catch (ParseException e) {
//            e.printStackTrace();
            Timber.e("getDate: " + e.getMessage());
        }
        return dateString;
    }

    public static String getTime(String dateString) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm a");
            return fmtOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            Timber.e("getTime" + e.getMessage());
        }
        return dateString;
    }

    public static String getCurrentDateTime() {
        Date currentTime = Calendar.getInstance().getTime();
        return String.valueOf(currentTime);
    }

    public static String getLiveDateTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(calendar.getTime());
    }

    public static String getReverseDateTime(String dateString) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
            return fmtOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static void displayErrorSnackbar(View view, Context context, String message/*, Typeface typeface*/) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir_regular.ttf");
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Action", null);
            View snackBarView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
            params.gravity = Gravity.TOP;
            TextView textView = (TextView) (snackBarView.findViewById(com.google.android.material.R.id.snackbar_text));
            textView.setTypeface(typeface);
            snackBarView.setLayoutParams(params);
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.err_red));
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("displayErrorSnackbar: %s", e.getMessage());
            Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void displaySuccessSnackbar(View view, Context context, String message/*, Typeface typeface*/) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir_regular.ttf");
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Action", null);
            View snackBarView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
            params.gravity = Gravity.TOP;
            TextView textView = (TextView) (snackBarView.findViewById(com.google.android.material.R.id.snackbar_text));
            textView.setTypeface(typeface);
            snackBarView.setLayoutParams(params);
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.green));
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void displayNetworkErrorSnackbar(View view, Context context) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir_regular.ttf");
            Snackbar snackbar = Snackbar.make(view, R.string.connection, Snackbar.LENGTH_LONG);
            snackbar.setAction("Action", null);
            View snackBarView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
            params.gravity = Gravity.TOP;
            TextView textView = (TextView) (snackBarView.findViewById(com.google.android.material.R.id.snackbar_text));
            textView.setTypeface(typeface);
            snackBarView.setLayoutParams(params);
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.light_red));
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.connection, Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isNetworkAvailable(Activity activity) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static void showNoNetwork(Activity activity/*, Typeface typeface*/) {
        if (activity != null && !activity.isDestroyed()) {

            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_regular.ttf");
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_no_wifi)
                    .setTitle(R.string.internet_connect)
                    .setText(R.string.connection)
                    .setBackgroundColorRes(R.color.light_red)
                    .enableSwipeToDismiss()
                    .setProgressColorRes(R.color.black)
                    .setTitleTypeface(typeface)
                    .setTextTypeface(typeface)
                    .show();
                    /*.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                            activity.startActivity(intent);
                        }
                    }).show();*/
        }
    }

    public static void SuccessAlert(Activity activity, String title, String message/*, Typeface typeface*/) {

        if (activity != null) {
            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_regular.ttf");
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_check)
                    .setTitle(title)
                    .setDuration(2000)
                    .setTitleAppearance(R.style.AlertTextAppearance_Title)
                    .setText(message)
                    .setTextAppearance(R.style.AlertTextAppearance_Text)
                    .setTitleTypeface(typeface)
                    .setTextTypeface(typeface)
                    .setBackgroundColorRes(R.color.green)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    public static void FailureAlert(Activity activity, String title, String message/*, Typeface typeface*/) {

        if (activity != null && !activity.isDestroyed()) {
            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_regular.ttf");
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_err_cross)
                    .setTitle(title)
                    .setTitleAppearance(R.style.AlertTextAppearance_Title)
                    .setText(message)
                    .setTextAppearance(R.style.AlertTextAppearance_Text)
                    .setTitleTypeface(typeface)
                    .setTextTypeface(typeface)
                    .setBackgroundColorRes(R.color.err_red)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    public static void WarningAlert(Activity activity, String title, String message/*, Typeface typeface*/) {

        if (activity != null) {
            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_regular.ttf");
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_warning)
                    .setTitle(title)
                    .setDuration(2000)
                    .setTitleAppearance(R.style.AlertTextAppearance_Title)
                    .setText(message)
                    .setTextAppearance(R.style.AlertTextAppearance_Text)
                    .setTitleTypeface(typeface)
                    .setTextTypeface(typeface)
                    .setBackgroundColorRes(R.color.light_red)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    public static String getBodyError(final Throwable throwable) {
        try {
            if (throwable instanceof HttpException) {
                HttpException error = (HttpException) throwable;
                Timber.e("getBodyError: %s", error.response().errorBody());
                String errorBody = Objects.requireNonNull(error.response().errorBody()).string();
                JSONObject jsonObject = new JSONObject(errorBody);
                /*if (jsonObject.has("message")) {
                    return jsonObject.optString("message");
                }*/
                if (jsonObject.has("msg")) {
                    return jsonObject.optString("msg");
                } else {
                    return Constants.ERROR;
                }
            } else {
                return Constants.ERROR;
            }

        } catch (JSONException | IOException error) {// IOException
            Timber.e("getBodyError %s", error);
            error.printStackTrace();
            return Constants.ERROR;
        }
    }

    public static String getUserCountry(Context context) {
        String locale = context.getResources().getConfiguration().locale.getCountry();
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null) {
                final String simCountry = tm.getSimCountryIso();
                if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                    return simCountry.toUpperCase();
                } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G
                    String networkCountry = tm.getNetworkCountryIso();
                    if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                        return networkCountry.toUpperCase();
                    }
                } else {
                    return locale;
                }
            }
        } catch (Exception e) {
            Timber.e("getUserCountry: exceptionUserCountry: %s", e.getMessage());
        }
        return locale;
    }

    public static void showLoading(Context context, boolean isCancelable/*, String message*/) {
        dialog = new ProgressDialog(context);
        dialog.setMessage("loading....");
        dialog.setCancelable(isCancelable);
        dialog.show();
    }

    public static void hideLoading() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }
}